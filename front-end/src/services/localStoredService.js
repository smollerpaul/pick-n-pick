import store from "store";
import Cookies from "universal-cookie";
import { history } from "AppRenderer";

const cookies = new Cookies();

/**
 * localStored service for browser
 * @param {*} name
 * @param {*} value
 */

/**
 * encrypt data for secure
 * do we need secret key here ?? #TODO
 */

export const save = (name, value) => {
  if (name === undefined) throw new Error("Cant store value with name undefined");
  return cookies.set(name, value, { path: "/" });
};

export const get = name => {
  return cookies.get(name);
};

export const remove = name => {
  cookies.remove(name);
};

export const clearAll = () => {
  cookies.remove("accessToken");
  cookies.remove("refreshToken");
  cookies.remove("userInfo");
  cookies.remove("storeid");
  cookies.remove("brandid");
};

export const saveStore = (name, value) => {
  if (name === undefined) throw new Error("Cant store value with name undefined");
  return store.set(name, value);
};

export const getStore = name => {
  return store.get(name);
};

export const removeStore = name => {
  store.remove(name);
};

export const clearAllStore = () => {
  store.clearAll();
};

export const clearEverything = () => {
  clearAll();
  clearAllStore();
  history.push("/user");
  window.location.reload();
  clearAll();
  clearAllStore();
};

export default {
  save,
  get,
  remove,
  clearAll,
  saveStore,
  getStore,
  removeStore,
  clearAllStore,
  clearEverything
};
