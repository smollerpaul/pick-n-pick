import axios from "axios";
import _ from "lodash";
import { history } from "AppRenderer";
import config from "../config";
import { save, get, clearEverything } from "services/localStoredService";
import { NotificationManager } from "theme/components/common/react-notifications";

export const refresh = (requestData, refreshToken) => {
  return axios({
    method: "POST",
    url: `${config.apiBaseURL}/auth/refresh`,
    headers: {
      authorization: refreshToken,
      "Content-Type": "application/json"
    }
  })
    .then(res => {
      const { accessToken, refreshToken, userInfo } = res.data.data.payload;
      save("accessToken", accessToken);
      save("refreshToken", refreshToken);
      save("userInfo", userInfo);
      const { endpoint, method, data, headerInput } = requestData;
      return request(endpoint, method, data, headerInput, accessToken);
    })
    .catch(err => {
      NotificationManager.error("Error!", err.stack);
      clearEverything();
      return err;
    });
};

export const handerError = (error, requestData) => {
  return new Promise(async (resolve, reject) => {
    const status = _.get(error, "response.status");
    const refreshToken = get("refreshToken");
    const message = _.get(error, "response.data.message");
    if (status < 500 && status >= 400) {
      // if (status === 404) {
      //   return history.push("/404");
      // }
      if (status === 401) {
        if (refreshToken) return resolve(await refresh(requestData, refreshToken));
        history.push("/user");
        window.location.reload();
        return;
      }
      //  else if (status === 403) {
      //   clearEverything();
      // }
      else {
        return reject(message);
      }
    } else {
      return reject(message);
    }
  });
};

export const request = (endpoint, method, data, headerInput, accessToken = null) => {
  return new Promise((resolve, reject) => {
    const getHeaders = input => {
      const token = accessToken ? accessToken : get("accessToken");
      const header = {
        authorization: token,
        "Content-Type": "application/json",
        ...input
      };
      return header;
    };

    const options = {
      method,
      url: endpoint,
      headers: getHeaders(headerInput),
      data: method !== "GET" ? data : null,
      params: method === "GET" ? data : null
    };
    return axios(options)
      .then(res => resolve(res.data))
      .catch(async error => {
        try {
          return resolve(await handerError(error, { endpoint, method, data, headerInput }));
        } catch (err) {
          return reject(err);
        }
      });
  });
};
