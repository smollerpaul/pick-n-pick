/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";
import moment from "moment"
export const name = "AdminSRM";

const initialState = freeze({
  data: {
    name: " ",
    email: " ",
    gender: " ",
    // birthDate: moment(new Date()).format("MM-DD-YYYY"),
    birthDate: "",
    // password:"",
    facebook: " ",
    countryId: " ",
    city: " ",
    // confirmPassword: "",
    avatar: ""
  },
  countries: [],
  modalAvatar: false,
  tabs: "pendding",
  message: "",
  isOpen: false,
  isLoading: false,
  SRMs: [],
  listProcessing: [],
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      usePagination: true,
      isExactly: false,
    },
  },
});

export default handleActions(
  {
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.fetchAllSRM]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        data: {
          ...state.data,
          [name]: value,
        },
      });
    },
    [actions.fetchAllSRMSuccess]: (state, action) => {
      return freeze({
        ...state,
        SRMs: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
     [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
      });
    },
    [actions.createNewSRMSuccess]: (state, action) => {
      return freeze({
        ...state,
        SRMs: [action.payload.data, ...state.SRMs],
        data: [],
        isOpen: false,
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value
          }
        }
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isOpen: true,
        isEdit: true,
        data: action.payload.data,
      });
    },
    [actions.editSRMSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        SRMs: [ ...state.SRMs.map(item => {
          if (item._id === action.payload.data._id) {
            return action.payload.data;
          }
          return item;
        })]
      });
    },
    [actions.deleteSRMSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        SRMs: [...state.SRMs.filter(o => o._id !== action.payload.data)]
      });
    },
    [actions.fetchAllCountriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        countries: action.payload.data,
      });
    },
    [actions.clear]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        data: initialState.data,
      });
    },
  },
  initialState
);
