import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/adminSRM";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllSRM(action) {
  try {
    const res = yield call(API.fetchAllSRM, action.payload.data);
    yield put(actions.fetchAllSRMSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload.data);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewSRM(action) {
  try {
    const res = yield call(API.createNewSRM, action.payload.data);
    yield put(actions.createNewSRMSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditSRM(action) {
  try {
    const res = yield call(API.editSRM, action.payload.data);
    yield put(actions.editSRMSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteSRM(action) {
  try {
    const res = yield call(API.deleteSRM, action.payload.data);
    yield put(actions.deleteSRMSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleupdateTask(action) {
  try {
    const {data} = action.payload;
    for (let item of data) yield put(actions.editSRM({data: item}))
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllCountries(action) {
  try {
    const res = yield call(API.fetchAllCountries, action.payload.data);
    yield put(actions.fetchAllCountriesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function* fetchAllSRM() {
  yield takeAction(actions.fetchAllSRM, handleFetchAllSRM);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewSRM() {
  yield takeAction(actions.createNewSRM, handlecreateNewSRM);
}

export function* editSRM() {
  yield takeEvery(actions.editSRM, handleEditSRM);
}

export function* deleteSRM() {
  yield takeAction(actions.deleteSRM, handleDeleteSRM);
}

export function* fetchAllCountries() {
  yield takeAction(actions.fetchAllCountries, handlefetchAllCountries);
}

export function* updateTask() {
  yield takeAction(actions.updateTask, handleupdateTask);
}

export default [
  fetchAllSRM, 
  createNewSRM,
  editSRM,
  deleteSRM,
  getInfo,
  updateTask,
  fetchAllCountries,
];
