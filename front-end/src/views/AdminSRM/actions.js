/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("ADMIN_SRM/HANDLE_INPUT_CHANGE");

export const handleToggleModal = createAction("ADMIN_SRM/HANDLE_TOGGLE_MODAL");

export const fetchAllSRM = createAction("ADMIN_SRM/FETCH_ALL_SRM");
export const fetchAllSRMSuccess = createAction("ADMIN_SRM/FETCH_ALL_SRM_SUCCESS");

export const getInfo = createAction("ADMIN_SRM/GET_INFO_SRM");
export const getInfoSuccess = createAction("ADMIN_SRM/ GET_INFO_SRM_SUCCESS");

export const createNewSRM = createAction("ADMIN_SRM/CREATE_NEW_SRM");
export const createNewSRMSuccess = createAction("ADMIN_SRM/CREATE_NEW_SRM_SUCCESS");

export const editSRM = createAction("ADMIN_SRM/EDIT_SRM");
export const editSRMSuccess = createAction("ADMIN_SRM/EDIT_SRM_SUCCESS");

export const updateTask = createAction("ADMIN_SRM/UPDATE_TASK_SRM");
export const updateTaskSuccess = createAction("ADMIN_SRM/UPDATE_TASK_SRM_SUCCESS");

export const deleteSRM = createAction("ADMIN_SRM/DELETE_SRM");
export const deleteSRMSuccess = createAction("ADMIN_SRM/DELETE_SRM_SUCCESS");

export const addStudentInclass = createAction("ADMIN_SRM/ADD_STUDENT_IN_CLASS");
export const addStudentInclassSuccess = createAction("ADMIN_SRM/ADD_STUDENT_IN_CLASS_SUCCESS");

export const removeStudentInclass = createAction("ADMIN_SRM/REMOVE_STUDENT_IN_CLASS");
export const removeStudentInclassSuccess = createAction("ADMIN_SRM/REMOVE_STUDENT_IN_CLASS_SUCCESS");

export const fetchAllCountries = createAction("ADMIN_SRM/FETCH_ALL_COUNTRY");
export const fetchAllCountriesSuccess = createAction("ADMIN_SRM/FETCH_ALL_COUNTRY_SUCCESS");

export const handleSearch = createAction("ADMIN_SRM/HANDLE_SEARCH");
export const changeTabs = createAction("ADMIN_SRM/CHANGE_TABS");

export const clear = createAction("ADMIN_SRM/CLEAR");
