/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";
export const name = "Speakers";

const initialState = freeze({
  data: {},
  countries: [],
  modalAvatar: false,
  tabs: "All",
  message: "",
  isOpen: false,
  isLoading: false,
  speakers: [],
  srms: [],
  dataModal: {},
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      usePagination: true,
      isExactly: false,
    },
  },
});

export default handleActions(
  {
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.fetchAllSpeakers]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        data: {
          ...state.data,
          [name]: value,
        },
      });
    },
    [actions.handleInputModalChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        dataModal: {
          ...state.dataModal,
          [name]: value,
        },
      });
    },
    [actions.fetchAllSpeakersSuccess]: (state, action) => {
      return freeze({
        ...state,
        speakers: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
      });
    },
    [actions.createNewSpeakersSuccess]: (state, action) => {
      return freeze({
        ...state,
        speakers: [action.payload.data, ...state.speakers],
        data: [],
        isOpen: false,
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value
          }
        }
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isOpen: true,
        isEdit: true,
        data: action.payload.data,
      });
    },
    [actions.editSpeakersSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen: false,
        data: initialState.data,
        speakers: [...state.speakers.map(item => {
          if (item._id === action.payload.data._id) {
            return action.payload.data;
          }
          return item;
        })]
      });
    },
    [actions.deleteSpeakersSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen: false,
        data: initialState.data,
        speakers: [...state.speakers.filter(o => o._id !== action.payload.data)]
      });
    },
    [actions.fetchAllCountriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        countries: action.payload.data,
      });
    },
    [actions.fetchAllSRMsSuccess]: (state, action) => {
      return freeze({
        ...state,
        srms: action.payload.data,
      });
    },
    [actions.createNewCountriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        countries: [...state.countries, action.payload.data],
        data: {
          ...state.data,
          countryId: action.payload.data._id
        }
      });
    },
    [actions.clear]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        data: initialState.data,
      });
    },
  },
  initialState
);
