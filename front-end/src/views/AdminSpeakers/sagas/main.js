import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/speakers";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllSpeakers(action) {
  try {
    const res = yield call(API.fetchAllSpeakers, action.payload.data);
    yield put(actions.fetchAllSpeakersSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload.data);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewSpeakers(action) {
  try {
    const res = yield call(API.createNewSpeakers, action.payload.data);
    yield put(actions.createNewSpeakersSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditSpeakers(action) {
  try {
    const res = yield call(API.editSpeakers, action.payload.data);
    yield put(actions.editSpeakersSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteSpeakers(action) {
  try {
    const res = yield call(API.deleteSpeakers, action.payload.data);
    yield put(actions.deleteSpeakersSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllCountries(action) {
  try {
    const res = yield call(API.fetchAllCountries, action.payload.data);
    yield put(actions.fetchAllCountriesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllSRMs(action) {
  try {
    const res = yield call(API.fetchAllSRMs, action.payload.data);
    yield put(actions.fetchAllSRMsSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewCountries(action) {
  try {
    const res = yield call(API.createNewCountries, action.payload.data);
    yield put(actions.createNewCountriesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function* fetchAllSpeakers() {
  yield takeAction(actions.fetchAllSpeakers, handleFetchAllSpeakers);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewSpeakers() {
  yield takeAction(actions.createNewSpeakers, handlecreateNewSpeakers);
}

export function* editSpeakers() {
  yield takeEvery(actions.editSpeakers, handleEditSpeakers);
}

export function* deleteSpeakers() {
  yield takeAction(actions.deleteSpeakers, handleDeleteSpeakers);
}

export function* fetchAllCountries() {
  yield takeAction(actions.fetchAllCountries, handlefetchAllCountries);
}

export function* fetchAllSRMs() {
  yield takeAction(actions.fetchAllSRMs, handlefetchAllSRMs);
}

export function* createNewCountries() {
  yield takeAction(actions.createNewCountries, handlecreateNewCountries);
}

export default [
  fetchAllSpeakers, 
  createNewSpeakers,
  editSpeakers,
  deleteSpeakers,
  getInfo,
  fetchAllCountries,
  fetchAllSRMs,
  createNewCountries,
];
