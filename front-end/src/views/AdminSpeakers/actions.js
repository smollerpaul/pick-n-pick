/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("ADMINS_SPEAKERS/HANDLE_INPUT_CHANGE");

export const handleInputModalChange = createAction("ADMINS_SPEAKERS/HANDLE_INPUT_MODAL_CHANGE");

export const handleToggleModal = createAction("ADMINS_SPEAKERS/HANDLE_TOGGLE_MODAL");

export const fetchAllSpeakers = createAction("ADMINS_SPEAKERS/FETCH_ALL_SPEAKERS");
export const fetchAllSpeakersSuccess = createAction("ADMINS_SPEAKERS/FETCH_ALL_Speakers_SUCCESS");

export const getInfo = createAction("ADMINS_SPEAKERS/GET_INFO_SPEAKERS");
export const getInfoSuccess = createAction("ADMINS_SPEAKERS/ GET_INFO_Speakers_SUCCESS");

export const createNewSpeakers = createAction("ADMINS_SPEAKERS/CREATE_NEW_SPEAKERS");
export const createNewSpeakersSuccess = createAction("ADMINS_SPEAKERS/CREATE_NEW_Speakers_SUCCESS");

export const editSpeakers = createAction("ADMINS_SPEAKERS/EDIT_SPEAKERS");
export const editSpeakersSuccess = createAction("ADMINS_SPEAKERS/EDIT_Speakers_SUCCESS");

export const deleteSpeakers = createAction("ADMINS_SPEAKERS/DELETE_SPEAKERS");
export const deleteSpeakersSuccess = createAction("ADMINS_SPEAKERS/DELETE_SPEAKERS_SUCCESS");

export const updateTask = createAction("ADMINS_SPEAKERS/UPDATE_TASK_SPEAKERS");
export const updateTaskSuccess = createAction("ADMINS_SPEAKERS/UPDATE_TASK_Speakers_SUCCESS");

export const fetchAllCountries = createAction("ADMINS_SPEAKERS/FETCH_ALL_COUNTRIES");
export const fetchAllCountriesSuccess = createAction("ADMINS_SPEAKERS/ FETCH_ALL_COUNTRIES_SUCCESS");

export const fetchAllSRMs = createAction("ADMINS_SPEAKERS/FETCH_ALL_SRMS");
export const fetchAllSRMsSuccess = createAction("ADMINS_SPEAKERS/ FETCH_ALL_SRMS_SUCCESS");

export const createNewCountries = createAction("ADMINS_SPEAKERS/CREATE_NEW_COUNTRIES");
export const createNewCountriesSuccess = createAction("ADMINS_SPEAKERS/CREATE_NEW_COUNTRIES_SUCCESS");

export const handleSearch = createAction("ADMINS_SPEAKERS/HANDLE_SEARCH");
export const changeTabs = createAction("ADMINS_SPEAKERS/CHANGE_TABS");

export const clear = createAction("ADMINS_SPEAKERS/CLEAR");
