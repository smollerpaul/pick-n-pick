import React, { Fragment, useState } from "react";
import { Row, Card, CardBody, CardImg} from "reactstrap";
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";

import { Colxx } from "theme/components/common/CustomBootstrap";
import Breadcrumb from "theme/containers/navs/Breadcrumb";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import SearchButton from "theme/components/SearchButton";
import classnames from "classnames";
import IntlMessages from "helpers/IntlMessages";

import ModalControl from "./Modal"
import { isEmpty, get } from "lodash"

function SRM(props) {

  const { speakers, match} = props;
  const { actions, isLoading, pagination, tabs } = props;
  const { all } = pagination.search;
  const { page, limit, totalPages } = pagination;
  const { messages } = props.intl;



  const onPageChange = (pageIndex) => {
    actions.fetchAllSpeakers({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllSpeakers({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
  };
  const dataTableColumns = [
    {
      Header: "Avatar",
      accessor: "avatar",
      className: "justify-center",
      width: 80,
      Cell: (props) => <CardImg top src={isEmpty(get(props, "value", ""))
        ? "/assets/img/avatar.png"
        : get(props, "value", "")}
        alt="Avatar" />
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {props.value}
      </p>
    },
    {
      Header: messages["user.relationshipManagerBy"],
      accessor: "relationshipManagerBy",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {get(props,"value.name","")}
      </p>
    },
    {
      Header: messages["user.frequency"],
      accessor: "frequency",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {props.value}
      </p>
    },
    {
      Header: messages["user.email"],
      accessor: "email",
      className: "justify-center",
      Cell: (props) => <div className="d-flex justify-content-center">
        <p className="text-muted">
          {props.value ? props.value : "Not infomation"}
        </p>
      </div>
    },
  ];

  return (
    <Fragment>
      <Row>
        <Colxx xxs="12" className="header-top">
          <SearchButton
            name="speakers.addNew"
            onChange={(e) => actions.handleSearch(e)}
            search={all}
            onSearch={() =>{
              isEmpty(all) ? actions.fetchAllSpeakers({
                data: { ...pagination,search: { usePagination: true, isExactly: false, }, },
              }): 
              actions.fetchAllSpeakers({
                data: { ...pagination, search: { ...pagination.search, isExactly: false } },
              })}
            }
            onToggle={() => actions.handleToggleModal()}
          />
          {/* <Breadcrumb heading="menu.speakers" match={match} /> */}
        </Colxx>

        <Colxx xxs="12">
          <Nav tabs className="separator-tabs">
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "All" })}
                onClick={() => {
                  actions.changeTabs("All");
                  actions.fetchAllSpeakers({
                    data: pagination,
                  })
                }}
              >
                <i className="simple-icon-layers" /> <IntlMessages id="speaking.all" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "name" })}
                onClick={() => {
                  actions.changeTabs("name");
                  actions.fetchAllSpeakers({
                    data: { ...pagination,sortBy: tabs },
                  })
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="user.name" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "srm" })}
                onClick={() => {
                  actions.changeTabs("srm");
                  actions.fetchAllSpeakers({
                    data: { ...pagination,sortBy: tabs },
                  })
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="speaking.srm" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "frequency" })}
                onClick={() => {
                  actions.changeTabs("frequency");
                  actions.fetchAllSpeakers({
                    data: { ...pagination,sortBy: tabs },
                  })
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="user.frequency" />
              </NavLink>
            </NavItem>
          </Nav>
          </Colxx>
        <Colxx xxs="12">
          <Card className="mb-4">
            <CardBody>
              <ReactTable
                data={speakers}
                columns={dataTableColumns}
                TbodyComponent={CustomTbodyComponent}
                defaultPageSize={speakers.length}
                sortable={false}
                filterable={false}
                showPageJump={true}
                PaginationComponent={DataTablePagination}
                showPageSizeOptions={true}
                pageSize={speakers.length +1}
                page={page - 1}
                loading={isLoading}
                pages={totalPages}
                manual
                onPageChange={(pageIndex) => onPageChange(pageIndex)}
                onPageSizeChange={(pageSize, pageIndex) => onPageSizeChange(pageSize, pageIndex)}
                getTrProps={(state, rowInfo, column) => {
                  return {
                    className: "cursor-pointer",
                    onClick: () => actions.getInfo({ data: rowInfo.original }),
                  };
                }}
                className="-striped -highlight h-full overflow-hidden"
              />
            </CardBody>
          </Card>
        </Colxx>
      </Row>
      <ModalControl {...props}/>
    </Fragment>
  );
}

export default React.memo(injectIntl(SRM));
