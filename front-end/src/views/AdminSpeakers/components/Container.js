import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Speakers from "./Speakers";
import { isEmpty } from "lodash";

class Container extends PureComponent {
  componentDidMount() {
    const { speakers, countries, pagination, actions, srms } = this.props
    if (isEmpty(speakers)) {
      actions.fetchAllSpeakers({ data: pagination });
    }
    if (isEmpty(srms)) {
      actions.fetchAllSRMs({
        data: {
          ...pagination,
          search: {
            usePagination: false,
            isExactly: true,
          }
        }
      });
    }
    if (isEmpty(countries)) {
      actions.fetchAllCountries({
        data: {
          ...pagination,
          search: {
            usePagination: false,
            isExactly: true,
          }
        }
      });
    }
  }

  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <div className="loading"></div>}
        <Speakers {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
