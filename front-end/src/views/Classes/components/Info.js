import React, { useState, Fragment } from "react";
import { CardImg, CardTitle, Modal, ModalBody, ModalFooter, Button, Row, Label, CardBody } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import { Colxx } from "theme/components/common/CustomBootstrap";
import ReactTable from "react-table";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import DataTablePagination from "theme/components/DatatablePagination";

import { injectIntl } from "react-intl";
import { get } from "lodash"
function ClassesDetail(props) {
    const { dataModal, isOpen, actions, pagination, classes, isLoading } = props;
    const { messages } = props.intl
    const { page, limit, totalPages } = pagination;

    const onPageChange = (pageIndex) => {
        actions.fetchAllClasses({ data: { ...pagination, page: pageIndex + 1 } });
    };
    const onPageSizeChange = (pageSize, pageIndex) => {
        actions.fetchAllClasses({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
    };

    const dataTableColumns = [
        {
            Header: messages["classes.name"],
            accessor: "name",
            className: "justify-center",
            Cell: (props) => <p className="text-capitalize text-muted">{props.value}</p>,
        },
        {
            Header: messages["classes.numStudents"],
            accessor: "numStudents",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex justify-content-center">
                    <p className="text-capitalize text-muted">{props.value ? props.value : 0}</p>
                </div>
            ),
        },
        {
            Header: messages["classes.leader"],
            accessor: "leader.name",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex justify-content-center">
                    <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
                </div>
            ),
        },
    ];
    return (
        <Fragment>
            <Modal isOpen={isOpen} size="md" >
                <ModalBody>
                    <div className="text-center" >
                        <CardImg src={get(dataModal, "avatar", "")} />
                    </div>
                    <div>
                        <Label className="h2 d-block my-2 px-2 " style={{ backgroundColor: "#BF9553", color: "white" }}>
                            <IntlMessages id="user.info" />
                        </Label>
                    </div>
                    <Row>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.name" /> :
                            <span className="ml-1">{get(dataModal, "name", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.gender" /> :
                            <span className="ml-1">{get(dataModal, "gender", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.email" /> :
                            <span className="ml-1">{get(dataModal, "email", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.faculty" /> :
                            <span className="ml-1">{get(dataModal, "faculty", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.isHighQualityStudentProgram" /> :
                            <span className="ml-1">{messages[`user.isHighQualityStudentProgram.${get(dataModal, "isHighQualityStudentProgram", "")}`]}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.knowUsFrom" /> :
                            <span className="ml-1">{get(dataModal, "knowUsFrom", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.reasonToJoin" /> :
                            <span className="ml-1">{get(dataModal, "reasonToJoin", "")}</span>
                            </CardTitle>
                        </Colxx>
                    </Row>
                    <CardBody>
                        <ReactTable
                            data={classes}
                            columns={dataTableColumns}
                            TbodyComponent={CustomTbodyComponent}
                            defaultPageSize={limit}
                            sortable={false}
                            filterable={false}
                            showPageJump={true}
                            PaginationComponent={DataTablePagination}
                            showPageSizeOptions={true}
                            pageSize={classes.length + 1}
                            page={page - 1}
                            loading={isLoading}
                            pages={totalPages}
                            manual
                            onPageChange={(pageIndex) => onPageChange(pageIndex)}
                            onPageSizeChange={(pageSize, pageIndex) =>
                                onPageSizeChange(pageSize, pageIndex)
                            }
                            className="-striped -highlight h-full overflow-hidden"
                        />
                    </CardBody>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-center" >
                    <Button color="info" outline onClick={() => actions.clearDataModal({ name: "dataModal" })}>
                        <IntlMessages id="button.cancel" />
                    </Button>
                </ModalFooter>
            </Modal>
        </Fragment>
    );
}

export default React.memo(injectIntl(ClassesDetail));
