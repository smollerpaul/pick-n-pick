import React, { Fragment, useState } from "react";
import { Row, Card, CardBody, Label, CardImg, Input, ModalHeader, FormGroup, FormFeedback } from "reactstrap";
import { Nav, NavItem, NavLink, TabContent, TabPane, Modal, CardTitle, ModalBody, ModalFooter, Button } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";

import { Colxx } from "theme/components/common/CustomBootstrap";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import SearchButton from "theme/components/SearchButton";
import classnames from "classnames";
import moment from "moment"
import { get, isEmpty } from "lodash";
import TableCLassSpeadking from "./TableCLassSpeadking"

function Classes(props) {
  const { classes, students, viewSpeaking, actions, isLoading, pagination, tabs, history, dataStudent, team, studentsInClass } = props;
  const { page, limit, totalPages } = pagination;
  const { all } = pagination.search;
  const { messages } = props.intl;
  const [isOpen, setModal] = useState(false)
  const [isChange, setModalChangeUniversity] = useState(false)

  const onPageChange = (pageIndex) => {
    actions.fetchAllClasses({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllClasses({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
  };
  const dataTableColumns = [
    {
      Header: messages["classes.name"],
      accessor: "name",
      className: "d-flex justify-content-center",
      Cell: (props) => <p className="text-capitalize text-muted">{props.value}</p>,
    },
    {
      Header: messages["classes.numStudents"],
      accessor: "numStudents",
      className: "d-flex justify-content-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value ? props.value : 0}</p>
        </div>
      ),
    },
    {
      Header: messages["classes.leader"],
      accessor: "leader.name",
      className: "d-flex justify-content-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
        </div>
      ),
    },
    {
      Header: "Speaking Detail",
      accessor: "leader.name",
      className: "d-flex justify-content-center",
      Cell: ({ original }) => (
        <Button color="primary" onClick={(e) => {
          e.stopPropagation();
          actions.fetchClasses({ data: original })
        }} >
          View Speaking
        </Button>
      ),
    },
  ];
  const dataTableModal = [
    {
      Header: messages["classes.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">{props.value}</p>,
    },
    {
      Header: messages["classes.numStudents"],
      accessor: "numStudents",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value ? props.value : 0}</p>
        </div>
      ),
    },
    {
      Header: messages["classes.leader"],
      accessor: "leader.name",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
        </div>
      ),
    },
    {
      Header: "",
      accessor: "",
      className: "justify-center",
      Cell: ({ original }) => {
        return get(dataStudent, "classId._id", "") === original._id ? null : (
            <Button className="default" onClick={(e) => {
              e.stopPropagation()
              setModal(!isOpen)
              actions.addStudentInclass({
                data: {
                  classId: original._id,
                  studentId: dataStudent._id
                }
              })
              actions.fetchAllStudents({ data: pagination })
            }}>
              <IntlMessages id="classes.addinClass" />
            </Button>
          )
      },
    },
  ];

  const dataTableStudents = [
    {
      Header: "Status",
      accessor: "classId.name",
      className: "d-flex justify-content-center",
      Cell: ({ original, value }) => original.status === "3" ? <p><IntlMessages id="classes.waitInterview" /></p> : (
        <p>{value ? value : original.lastInterviewResult ? original.lastInterviewResult.rejectReason : "Waiting"} </p>),
    },
    {
      Header: "Avatar",
      accessor: "avatar",
      className: "justify-center",
      width: 100,
      Cell: (props) => {
        return <CardImg top src={props.value} alt={props.values} />;
      },
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      width: 250,
      Cell: (props) => (
        <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
      ),
    },
    {
      Header: messages["user.role"],
      accessor: "role",
      className: "d-flex justify-content-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{messages[`user.role.${props.value}`]}</p>
        </div>
      ),
    },
    {
      Header: messages["user.birthDate"],
      accessor: "birthDate",
      className: "d-flex justify-content-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{moment(props.value).format("MM/DD/YYYY")}</p>
        </div>
      ),
    },
    {
      Header: messages["user.englishLevel"],
      accessor: "englishLevel",
      className: "d-flex justify-content-center",
      width: 100,
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">
            {messages[`user.englishLevel.${props.value}`]}
          </p>
        </div>
      ),
    },
    {
      Header: "Intro",
      accessor: "videoIntroduce",
      className: "d-flex justify-content-center",
      width: 100,
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <a className="text-reset" onClick={(e) => e.stopPropagation()} style={{ textDecoration: "underline" }} href={`${props.value}`} target="_blank">View</a>
        </div>
      ),
    },
  ]

  return (
    <Fragment>
      <Row>
        <Colxx xxs="12" className="header-top">
          <SearchButton
            name="classes.addNew"
            onChange={(e) => actions.handleSearch(e)}
            search={all}
            onSearch={() => {
              isEmpty(all)
                ? actions.fetchAllClasses({
                  data: { ...pagination, search: { usePagination: true, isExactly: false } },
                })
                : actions.fetchAllClasses({
                  data: { ...pagination, search: { ...pagination.search, isExactly: false } },
                });
            }}
            onToggle={() => {
              actions.clear();
              history.push(`/app/classes/create`)
            }}
          />
          {/* <Breadcrumb heading="menu.classes" match={match} /> */}
        </Colxx>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs">
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "table" })}
                onClick={() => {
                  actions.changeTabs("table");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="classes.kanban" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "students" })}
                onClick={() => {
                  actions.changeTabs("students");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="classes.students" />
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={tabs} className="chat-app-tab-content mt-3">
            <TabPane tabId="table" className="chat-app-tab-pane">
              <Card className="mb-4">
                <CardBody>
                  <ReactTable
                    data={classes}
                    columns={dataTableColumns}
                    TbodyComponent={CustomTbodyComponent}
                    defaultPageSize={limit}
                    sortable={false}
                    filterable={false}
                    showPageJump={true}
                    PaginationComponent={DataTablePagination}
                    showPageSizeOptions={true}
                    pageSize={classes.length + 1}
                    page={page - 1}
                    loading={isLoading}
                    pages={totalPages}
                    manual
                    onPageChange={(pageIndex) => onPageChange(pageIndex)}
                    onPageSizeChange={(pageSize, pageIndex) =>
                      onPageSizeChange(pageSize, pageIndex)
                    }
                    getTrProps={(state, rowInfo, column) => {
                      return {
                        className: "cursor-pointer",
                        onClick: () => {
                          history.push(`/app/classes/${rowInfo.original._id}`);
                          actions.clear()
                        },
                      };
                    }}
                    className="-striped -highlight h-full overflow-hidden"
                  />
                </CardBody>
                {viewSpeaking === true && (
                  <CardBody>
                    <CardTitle>{`Team ${team.name}`}</CardTitle>
                    <TableCLassSpeadking {...props} />
                  </CardBody>
                )}
              </Card>
            </TabPane>
            <TabPane tabId="students" className="chat-app-tab-pane">
              <Card className="mb-4">
                <CardBody>
                  <ReactTable
                    data={students}
                    columns={dataTableStudents}
                    TbodyComponent={CustomTbodyComponent}
                    defaultPageSize={limit}
                    sortable={false}
                    filterable={false}
                    showPageJump={true}
                    PaginationComponent={DataTablePagination}
                    showPageSizeOptions={true}
                    pageSize={students.length + 1}
                    page={page - 1}
                    loading={isLoading}
                    pages={totalPages}
                    manual
                    onPageChange={(pageIndex) => actions.fetchAllStudents({ data: { ...pagination, page: pageIndex + 1 } })}
                    onPageSizeChange={(pageSize, pageIndex) =>
                      actions.fetchAllStudents({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } })
                    }
                    getTrProps={(state, rowInfo, column) => {
                      return {
                        className: "cursor-pointer",
                        onClick: () => {
                          actions.handleStudent({ data: rowInfo.original });
                          setModal(!isOpen)
                        },
                      };
                    }}
                    className="-striped -highlight h-full overflow-hidden"
                  />
                </CardBody>
              </Card>
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
      <Modal isOpen={isOpen} size="lg" >
        <ModalBody>
          <div className="text-center" >
            <CardImg top style={{maxWidth: "35em"}} src={get(dataStudent, "avatar", "")} />
          </div>
          <div>
            <Label className="h2 d-block my-2 px-2 " style={{ backgroundColor: "#BF9553", color: "white" }}>
              <IntlMessages id="user.info" />
            </Label>
          </div>
          <Row>
            <Colxx xxs="12" className="ml-2">
              <CardTitle className="my-2 h5" >
                <IntlMessages id="user.name" /> :
                <span className="ml-1">{get(dataStudent, "name", "")}</span>
              </CardTitle>
            </Colxx>
            <Colxx xxs="12" className="ml-2">
              <CardTitle className="my-2 h5" >
                <IntlMessages id="user.gender" /> :
                <span className="ml-1">{get(dataStudent, "gender", "")}</span>
              </CardTitle>
            </Colxx>
            <Colxx xxs="12" className="ml-2">
              <CardTitle className="my-2 h5" >
                <IntlMessages id="user.email" /> :
                <span className="ml-1">{get(dataStudent, "email", "")}</span>
              </CardTitle>
            </Colxx>
            <Colxx xxs="12" className="ml-2">
              <CardTitle className="my-2 h5" >
                <IntlMessages id="user.birthDay" /> :
                <span className="ml-1">{moment(get(dataStudent, "birthDate", "")).format("DD/MM/YYYY")}</span>
              </CardTitle>
            </Colxx>
            <Colxx xxs="12" className="ml-2">
              <CardTitle className="my-2 h5" >
                <IntlMessages id="user.countries" /> :
                <span className="ml-1">{get(dataStudent, "countryId.name", "")}</span>
              </CardTitle>
            </Colxx>
            <Colxx xxs="12" className="ml-2">
              <CardTitle className="my-2 h5" >
                <IntlMessages id="user.universityId" /> :
                <span className="ml-1">{get(dataStudent, "universityId.name", "")}</span>
              </CardTitle>
            </Colxx>
            <Colxx xxs="12" className="ml-2">
              <CardTitle className="my-2 h5" >
                <IntlMessages id="user.introduceMySelf" /> :
                <span className="ml-1"> <a className="text-reset" style={{ textDecoration: "underline" }} href={`${get(dataStudent, "facebook", "")}`} target="_blank">{get(dataStudent, "facebook", "")}</a></span>
              </CardTitle>
            </Colxx>
          </Row>
          {(isEmpty(dataStudent.classId) && dataStudent.status !== "3") && (
            <CardBody>
              <ReactTable
                data={classes}
                columns={dataTableModal}
                TbodyComponent={CustomTbodyComponent}
                defaultPageSize={limit}
                sortable={false}
                filterable={false}
                showPageJump={true}
                PaginationComponent={DataTablePagination}
                showPageSizeOptions={true}
                pageSize={classes.length + 1}
                page={page - 1}
                loading={isLoading}
                pages={totalPages}
                manual
                className="-striped -highlight h-full overflow-hidden"
              />
            </CardBody>
          )
          }
        </ModalBody>
        {isEmpty(dataStudent.classId) === false ? (
          <ModalFooter className="d-flex justify-content-around">
            <Button color="info" outline onClick={() => setModal(!isOpen)}>
              Ok
            </Button>
          </ModalFooter>
        ) : (
            <ModalFooter className="d-flex justify-content-around">
              <Button color="info" outline onClick={() => setModal(!isOpen)}>
                <IntlMessages id="button.cancel" />
              </Button>
              <Button color="warning" outline onClick={() => setModalChangeUniversity(!isChange)}>
                <IntlMessages id="classes.changeUniversity" />
              </Button>
            </ModalFooter>)}
      </Modal>

      <Modal isOpen={isChange} size="md">
        <ModalBody>
          <ModalHeader className="text-center">
            <Label>
              <IntlMessages id="modalDelete.sure" />
            </Label>
          </ModalHeader>
          <FormGroup>
            <Label>
              Note
            </Label>
            <Input
              type="textarea"
              rows={5}
              name="note"
              value={get(dataStudent, "note", "")}
              onChange={(e) => actions.handleInputChange(e)}
              invalid={isEmpty(get(dataStudent, "note", "")) ? true : false}
            />
            <FormFeedback>{messages[`students.note`]}</FormFeedback>
          </FormGroup>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-around">
          <Button
            color="info"
            outline
            onClick={() => {
              setModalChangeUniversity(!isChange);
            }}
          >
            <IntlMessages id="button.cancel" />
          </Button>
            <Button
              color="danger"
              disabled={dataStudent.note ? false : true}
              outline
              onClick={() => {
                actions.changeUniversity({
                  data: {
                    ...dataStudent,
                    needAdminAction: true,
                    needAdminActionType: "changeUniversity"
                  }
                })
                setModalChangeUniversity(!isChange)
              }}
            >
              <IntlMessages id="classes.changeUniversity" />
            </Button>
        </ModalFooter>
      </Modal>
    </Fragment>
  );
}

export default React.memo(injectIntl(Classes));
