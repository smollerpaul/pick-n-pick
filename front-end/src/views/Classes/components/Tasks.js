import React, { Fragment } from "react";
import { Card, CardBody, Label, CardImg } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import moment from "moment"
// import TaskManager from "./Tasks"

function Classes(props) {
  const { students, actions, isLoading, pagination} = props;
  const { page, limit, totalPages } = pagination;
  const { messages } = props.intl;

  const onPageChange = (pageIndex) => {
    actions.fetchAllClasses({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllClasses({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
  };

  const dataTableStudents = [
    {
      Header: "",
      accessor: "avatar",
      className: "justify-center",
      width: 100,
      Cell: ({original}) => {
        const {lastInterviewResult} = original
        if(lastInterviewResult) return <Label className="text-muted"><IntlMessages id="button.reject" /></Label>
        else return <Label className="text-muted" ><IntlMessages id="button.waiting" /></Label>;
      },
    },{
      Header: "",
      accessor: "avatar",
      className: "justify-center",
      width: 100,
      Cell: (props) => {
        return <CardImg top src={props.value} alt={props.values} />;
      },
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => (
        <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
      ),
    },
    {
      Header: messages["user.role"],
      accessor: "role",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value}</p>
        </div>
      ),
    },
    {
      Header: messages["user.birthDate"],
      accessor: "birthDate",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{moment(props.value).format("MM/DD/YYYY")}</p>
        </div>
      ),
    },
    {
      Header: messages["user.englishLevel"],
      accessor: "englishLevel",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">
            {messages[`user.englishLevel.${props.value}`]}
          </p>
        </div>
      ),
    },
    {
      Header: messages["user.faculty"],
      accessor: "faculty",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value}</p>
        </div>
      ),
    },
    {
      Header: "Intro",
      accessor: "videoIntroduce",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <a className="text-reset" style={{textDecoration: "underline"}} href={`${props.value}`}  target="_blank">{props.value}</a>
        </div>
      ),
    },
  ]

  return (
    <Fragment>
              <Card className="mb-4">
                <CardBody>
                  <ReactTable
                    data={students}
                    columns={dataTableStudents}
                    TbodyComponent={CustomTbodyComponent}
                    defaultPageSize={limit}
                    sortable={false}
                    filterable={false}
                    showPageJump={true}
                    PaginationComponent={DataTablePagination}
                    showPageSizeOptions={true}
                    pageSize={limit}
                    page={page - 1}
                    loading={isLoading}
                    pages={totalPages}
                    manual
                    onPageChange={(pageIndex) => onPageChange(pageIndex)}
                    onPageSizeChange={(pageSize, pageIndex) =>
                      onPageSizeChange(pageSize, pageIndex)
                    }
                    // getTrProps={(state, rowInfo, column) => {
                    //   return {
                    //     className: "cursor-pointer",
                    //     onClick: () => {
                    //       history.push(`/app/classes/${rowInfo.original._id}`);
                    //       actions.clear()
                    //     },
                    //   };
                    // }}
                    className="-striped -highlight h-full overflow-hidden"
                  />
                </CardBody>
              </Card>
    </Fragment>
  );
}

export default React.memo(injectIntl(Classes));
