import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Classes from "./Classes";
import { isEmpty } from "lodash";

class Container extends PureComponent {
  componentDidMount() {
    const {classes,students, pagination, actions} = this.props
    if (isEmpty(classes)) {
      actions.fetchAllClasses({ data: pagination });
    }
    if (isEmpty(students)) {
      actions.fetchAllStudents({ data: pagination });
    }
  }

  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <div className="loading"></div>}
        <Classes {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
