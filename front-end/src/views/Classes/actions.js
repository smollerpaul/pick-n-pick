/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("CLASSES/HANDLE_INPUT_CHANGE");

export const handleToggleModal = createAction("CLASSES/HANDLE_TOGGLE_MODAL");

export const fetchAllClasses = createAction("CLASSES/FETCH_ALL_CLASSES");
export const fetchAllClassesSuccess = createAction("CLASSES/FETCH_ALL_Classes_SUCCESS");

export const fetchClasses = createAction("CLASSES/FETCH_CLASSES");
export const fetchClassesSuccess = createAction("CLASSES/FETCH_Classes_SUCCESS");

export const getInfo = createAction("CLASSES/GET_INFO_CLASSES");
export const getInfoSuccess = createAction("CLASSES/ GET_INFO_Classes_SUCCESS");

export const createNewClasses = createAction("CLASSES/CREATE_NEW_CLASSES");
export const createNewClassesSuccess = createAction("CLASSES/CREATE_NEW_Classes_SUCCESS");

export const editClasses = createAction("CLASSES/EDIT_CLASSES");
export const editClassesSuccess = createAction("CLASSES/EDIT_Classes_SUCCESS");

export const updateTask = createAction("CLASSES/UPDATE_TASK_CLASSES");
export const updateTaskSuccess = createAction("CLASSES/UPDATE_TASK_Classes_SUCCESS");

export const deleteClasses = createAction("CLASSES/DELETE_CLASSES");
export const deleteClassesSuccess = createAction("CLASSES/DELETE_Classes_SUCCESS");

export const fetchAllStudents = createAction("CLASSES/FETCH_ALL_STUDENTS");
export const fetchAllStudentsSuccess = createAction("CLASSES/FETCH_ALL_STUDENTS_SUCCESS");

export const addStudentInclass = createAction("CLASSES/ADD_STUDENT_IN_CLASS");
export const addStudentInclassSuccess = createAction("CLASSES/ADD_STUDENT_IN_CLASS_SUCCESS");

export const undoStudentInclass = createAction("CLASSES/UNDO_STUDENT_IN_CLASS");
export const undoStudentInclassSuccess = createAction("CLASSES/UNDO_STUDENT_IN_CLASS_SUCCESS");

export const removeStudentInclass = createAction("CLASSES/REMOVE_STUDENT_IN_CLASS");
export const removeStudentInclassSuccess = createAction("CLASSES/REMOVE_STUDENT_IN_CLASS_SUCCESS");

export const handleStudent = createAction("CLASSES/HANDLE_STUDENT");
export const changeUniversity = createAction("CLASSES/CHANGE_UNIVERSITY_CLASSES");
export const changeUniversitySuccess = createAction("CLASSES/ CHANGE_UNIVERSITY_Classes_SUCCESS");

export const handleSearch = createAction("CLASSES/HANDLE_SEARCH");
export const changeTabs = createAction("CLASSES/CHANGE_TABS");
export const clear = createAction("CLASSES/CLEAR");
