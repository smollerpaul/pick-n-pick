import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/classes";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllClasses(action) {
  try {
    const res = yield call(API.fetchAllClasses, action.payload.data);
    yield put(actions.fetchAllClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload.data);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewClasses(action) {
  try {
    const res = yield call(API.createNewClasses, action.payload.data);
    yield put(actions.createNewClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditClasses(action) {
  try {
    const res = yield call(API.editClasses, action.payload.data);
    yield put(actions.editClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteClasses(action) {
  try {
    const res = yield call(API.deleteClasses, action.payload.data);
    yield put(actions.deleteClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllStudents(action) {
  try {
    const res = yield call(API.fetchAllStudents, action.payload.data);
    yield put(actions.fetchAllStudentsSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleaddStudentInclass(action) {
  try {
    const res = yield call(API.addStudentInclass, action.payload.data);
    yield put(actions.addStudentInclassSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleundoStudentInclass(action) {
  try {
    // const res = yield call(API.undoStudentInclass, action.payload);
    yield put(actions.undoStudentInclassSuccess());
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlechangeUniversity(action) {
  try {
    const res = yield call(API.changeUniversity, action.payload.data);
    yield put(actions.changeUniversitySuccess());
    NotificationManager.success("Please waiting response from admin");
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchClasses(action) {
  try {
    const res = yield call(API.fetchClasses, action.payload.data);
    yield put(actions.fetchClassesSuccess({data: res.data, team: action.payload.data}));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleupdateTask(action) {
  try {
    const {data} = action.payload;
    for (let item of data) yield put(actions.editClasses({data: item}))
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleremoveStudentInclass(action) {
  try {
    const res = yield call(API.removeStudentInclass, action.payload.data);
    yield put(actions.getInfo(action.payload.classId));
    yield put(actions.removeStudentInclassSuccess(res));
    NotificationManager.success("Undo Student Success")
  } catch (err) {
    NotificationManager.error(err);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function* fetchAllClasses() {
  yield takeAction(actions.fetchAllClasses, handleFetchAllClasses);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewClasses() {
  yield takeAction(actions.createNewClasses, handlecreateNewClasses);
}

export function* editClasses() {
  yield takeEvery(actions.editClasses, handleEditClasses);
}

export function* deleteClasses() {
  yield takeAction(actions.deleteClasses, handleDeleteClasses);
}

export function* fetchAllStudents() {
  yield takeAction(actions.fetchAllStudents, handlefetchAllStudents);
}

export function* addStudentInclass() {
  yield takeAction(actions.addStudentInclass, handleaddStudentInclass);
}

export function* undoStudentInclass() {
  yield takeAction(actions.undoStudentInclass, handleundoStudentInclass);
}

export function* changeUniversity() {
  yield takeAction(actions.changeUniversity, handlechangeUniversity);
}

export function* fetchClasses() {
  yield takeAction(actions.fetchClasses, handlefetchClasses);
}

export function* removeStudentInclass() {
  yield takeAction(actions.removeStudentInclass, handleremoveStudentInclass);
}

export function* updateTask() {
  yield takeAction(actions.updateTask, handleupdateTask);
}

export default [
  fetchAllClasses, 
  createNewClasses,
  editClasses,
  deleteClasses,
  fetchAllStudents,
  addStudentInclass,
  getInfo,
  updateTask,
  fetchAllStudents,
  undoStudentInclass,
  changeUniversity,
  fetchClasses,
  removeStudentInclass
];
