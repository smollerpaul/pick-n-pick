/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";
import {filter} from "lodash"
export const name = "Classes";

const initialState = freeze({
  data: {
    name: " ",
  },
  modalAvatar: false,
  tabs: "table",
  message: "",
  team: {},
  classes: [],
  students: [],
  studentsInClass: [],
  dataStudent: {},
  isOpen: false,
  isLoading: false,
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      usePagination: true,
      isExactly: false,
    },
  },
});

export default handleActions(
  {
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.fetchAllClasses]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        dataStudent: {
          ...state.dataStudent,
          [name]: value,
        },
      });
    },
    [actions.fetchAllClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        classes: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.fetchAllStudentsSuccess]: (state, action) => {
      return freeze({
        ...state,
        students: action.payload.data,
      });
    },
     [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
      });
    },
    [actions.createNewClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        classes: [action.payload.data, ...state.classes],
        data: [],
        isOpen: false,
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value
          }
        }
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isOpen: true,
        isEdit: true,
        dataStudent: action.payload.data,
      });
    },
    [actions.editClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        classes: [ ...state.classes.map(item => {
          if (item._id === action.payload.data._id) {
            return action.payload.data;
          }
          return item;
        })]
      });
    },
    [actions.deleteClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        classes: [...state.classes.filter(o => o._id !== action.payload.data)]
      });
    },
    [actions.addStudentInclassSuccess]: (state, action) => {
      return freeze({
        ...state,
        studentsInClass: [...state.studentsInClass, action.payload.data.classId]
      });
    },
    [actions.removeStudentInclassSuccess]: (state, action) => {
      return freeze({
        ...state,
        studentsInClass: filter(state.studentsInClass, o => o !== action.payload.data.classId)
      });
    },
    [actions.undoStudentInclassSuccess]: (state, action) => {
      return freeze({
        ...state,
        studentsInClass: filter(state.studentsInClass, o => o._ !== action.payload.data._id)
      });
    },
    [actions.handleStudent]: (state, action) => {
      return freeze({
        ...state,
        dataStudent: action.payload.data
      });
    },
    [actions.fetchClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        viewSpeaking: true,
        classSpeakings: action.payload.data,
        team: action.payload.team,
      });
    },
    [actions.clear]: (state, action) => {
      return freeze({
        ...state,
        classes: []
      });
    },
  },
  initialState
);
