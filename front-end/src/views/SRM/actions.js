/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

//SPEAKER
export const handleInputChange = createAction("SRM/HANDLE_INPUT_CHANGE");
export const handleToggleModal = createAction("SRM/HANDLE_TOGGLE_MODAL");


export const fetchAllSpeaker = createAction("SRM/FETCH_ALL_SPEAKERS");
export const fetchAllSpeakerSuccess = createAction("SRM/FETCH_ALL_SPEAKERS_SUCCESS");

export const getInfo = createAction("SRM/GET_INFO_SPEAKERS");
export const getInfoSuccess = createAction("SRM/ GET_INFO_SPEAKERS_SUCCESS");

export const createNewSpeaker = createAction("SRM/CREATE_NEW_SPEAKERS");
export const createNewSpeakerSuccess = createAction("SRM/CREATE_NEW_SPEAKERS_SUCCESS");

export const editSpeaker = createAction("SRM/EDIT_SPEAKERS");
export const editSpeakerSuccess = createAction("SRM/EDIT_Speaker_SUCCESS");

export const deleteSpeaker = createAction("SRM/DELETE_SPEAKERS");
export const deleteSpeakerSuccess = createAction("SRM/DELETE_SPEAKERS_SUCCESS");

// speaking

export const handleDataSpeakingChange = createAction("SRM/HANDLE_DATA_SPEAKING_CHANGE");
export const handleToggleModalSpeaking = createAction("SRM/HANDLE_TOGGLE_MODAL_SPEAKING");

export const fetchAllSpeaking = createAction("SRM/FETCH_ALL_SPEAKING");
export const fetchAllSpeakingSuccess = createAction("SRM/FETCH_ALL_SPEAKING_SUCCESS");

export const createNewSpeaking = createAction("SRM/CREATE_NEW_SPEAKING");
export const createNewSpeakingSuccess = createAction("SRM/CREATE_NEW_SPEAKING_SUCCESS");

export const editSpeaking = createAction("SRM/EDIT_SPEAKING");
export const editSpeakingSuccess = createAction("SRM/EDIT_Speaking_SUCCESS");

export const deleteSpeaking = createAction("SRM/DELETE_SPEAKING");
export const deleteSpeakingSuccess = createAction("SRM/DELETE_SPEAKING_SUCCESS");

export const getInfoSpeaking = createAction("SRM/GET_INFO_SPEAKING");
export const getInfoSpeakingSuccess = createAction("SRM/ GET_INFO_SPEAKING_SUCCESS");

//other
export const updateTask = createAction("SRM/UPDATE_TASK_SPEAKERS");
export const updateTaskSuccess = createAction("SRM/UPDATE_TASK_SPEAKERS_SUCCESS");

export const addStudentInclass = createAction("SRM/ADD_STUDENT_IN_CLASS");
export const addStudentInclassSuccess = createAction("SRM/ADD_STUDENT_IN_CLASS_SUCCESS");

export const removeStudentInclass = createAction("SRM/REMOVE_STUDENT_IN_CLASS");
export const removeStudentInclassSuccess = createAction("SRM/REMOVE_STUDENT_IN_CLASS_SUCCESS");

export const fetchAllCountries = createAction("SRM/FETCH_ALL_COUNTRY");
export const fetchAllCountriesSuccess = createAction("SRM/FETCH_ALL_COUNTRY_SUCCESS");

export const handleSearch = createAction("SRM/HANDLE_SEARCH");
export const changeTabs = createAction("SRM/CHANGE_TABS");

export const clear = createAction("SRM/CLEAR");
