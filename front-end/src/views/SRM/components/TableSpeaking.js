import React from "react";
import { Card, CardBody } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import moment from "moment";

import {filter, orderBy} from "lodash"

function TableSpeaking(props) {

  const { actions, isLoading, pagination,speaking } = props;
  const { page, limit, totalPages } = pagination;
  const { messages } = props.intl;

  const speakingOption = filter(speaking, o =>{
    const monthNow = moment().format("MM")
    const monthFilter = moment(o.happenDate).format("MM")

    const dayNow = moment().format("DD")
    const dayFilter = moment(o.happenDate).format("DD")

    if(monthNow === monthFilter){
      if(dayNow <= dayFilter){
        return o
      }
    }
  })


  const onPageChange = (pageIndex) => {
    console.log("page",{ ...pagination, page: pageIndex + 1 })
    actions.fetchAllSpeaking({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllSpeaking({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
  };

  const dataListSpeaking = [
    {
      Header: messages["speaking.name"],
      accessor: "speakerId.name",
      className: "d-flex justify-content-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {props.value}
      </p>
    },
    {
      Header: messages["user.topic"],
      accessor: "topic",
      className: "d-flex justify-content-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {props.value}
      </p>
    },
    {
      Header: messages["user.description"],
      accessor: "description",
      className: "d-flex justify-content-center truncate",
      Cell: (props) => <p className="text-capitalize text-muted">
        {props.value}
      </p>
    },
    {
      Header: messages["speaking.happenDate"],
      accessor: "happenDate",
      className: "d-flex justify-content-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {moment(props.value).format('llll')}
      </p>
    },
    {
      Header: messages["user.isCofirmed"],
      accessor: "isConfirmed",
      className: "d-flex justify-content-center",
      width: 120,
      Cell: (props) => <p className="text-capitalize text-muted">
        {messages[`user.isConfirmed.${props.value}`]}
      </p>
    },
  ];

  return (
    <Card className="mb-4">
      <CardBody>
        <ReactTable
          data={orderBy(speakingOption, "happenDate", "asc")}
          columns={dataListSpeaking}
          TbodyComponent={CustomTbodyComponent}
          defaultPageSize={limit}
          sortable={false}
          filterable={false}
          showPageJump={true}
          PaginationComponent={DataTablePagination}
          showPageSizeOptions={true}
          pageSize={speakingOption.length + 1}
          page={page - 1}
          loading={isLoading}
          pages={totalPages}
          manual
          onPageChange={(pageIndex) => onPageChange(pageIndex)}
          onPageSizeChange={(pageSize, pageIndex) => onPageSizeChange(pageSize, pageIndex)}
          getTrProps={(state, rowInfo, column) => {
            return {
              className: "cursor-pointer",
              onClick: () => actions.getInfoSpeaking({ data: rowInfo.original }),
            };
          }}
          className="-striped -highlight h-full overflow-hidden"
        />
      </CardBody>
    </Card>
  );
}

export default React.memo(injectIntl(TableSpeaking));
