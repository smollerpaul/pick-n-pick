import React, { Fragment, useState } from "react";
import { Row, FormGroup, Input, FormFeedback, CardImg } from "reactstrap";
import { Button, Modal, ModalBody, ModalHeader, ModalFooter, Label } from "reactstrap";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

import { Colxx } from "theme/components/common/CustomBootstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import CustomSelectInput from "theme/components/common/CustomSelectInput";
import "react-datepicker/dist/react-datepicker.css";
import { NotificationManager } from "theme/components/common/react-notifications";

import { isEmpty, get, find, isObject } from "lodash"

function ModalControl(props) {

    const { actions, dataSpeaking, isModalSpeaking, isEdit } = props;
    const { messages } = props.intl;
    const { topic, description, happenDate, speakerId, avatar } = dataSpeaking;

    const [modaDelete, setModalDelete] = useState(false)

    const speakersOption = get(props, "speakers", []).map(o => ({
        label: o.name,
        value: o._id,
        avatar: o.avatar
    }))

    const onSave = () => {
        for (const item in dataSpeaking) {
            if (isEmpty(dataSpeaking[item].toString()) || dataSpeaking[item] === " ") return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.${item}`]}`);
        }
        return isEdit ? actions.editSpeaking({ data: dataSpeaking }) : actions.createNewSpeaking({ data: dataSpeaking })
    };

    const date = new Date();
    return (
        <Fragment>
            <Modal isOpen={isModalSpeaking} size="lg">
                <ModalBody>
                    <ModalHeader>
                        <Label>
                            {isEdit ? topic : <IntlMessages id="srms.modalCreateSpeaking" />}
                        </Label>
                    </ModalHeader>
                    <Row>
                        <Colxx xxs="12" className="text-center mb-4">
                            <CardImg
                                src={isObject(speakerId) ? speakerId.avatar : isEmpty(avatar) ? "/assets/img/avatar.png" : avatar}
                                alt="Avatar"
                            />
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Input
                                        type="topic"
                                        name="topic"
                                        value={topic}
                                        onChange={(e) => actions.handleDataSpeakingChange(e)}
                                        invalid={isEmpty(topic) ? true : false}
                                    />
                                    <FormFeedback>{messages[`user.topic`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.topic" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <DatePicker
                                        value={moment(happenDate).format('llll')}
                                        selected={Date.parse(happenDate)}
                                        minDate={date.setDate(date.getDate() + 7)}
                                        onChange={(e) => {
                                            actions.handleDataSpeakingChange({
                                                target: { name: "happenDate", value: e },
                                            });
                                        }}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        timeIntervals={15}
                                        dateFormat="MMMM d, yyyy h:mm aa"
                                    />
                                    <FormFeedback>{messages[`speaking.happenDate`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="speaking.happenDate" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="12" xl="12">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Select
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        components={{ Input: CustomSelectInput }}
                                        options={speakersOption}
                                        name="speakerId"
                                        value={find(speakersOption, (o) => o.value === (isObject(speakerId) ? speakerId._id : speakerId))}
                                        onChange={(e) => {
                                            actions.handleDataSpeakingChange({
                                                target: { name: "speakerId", value: e.value },
                                            });
                                            actions.handleDataSpeakingChange({
                                                target: { name: "avatar", value: e.avatar },
                                            });
                                        }}
                                    />
                                    <FormFeedback>{messages[`user.speakerId`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.speakerId" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Input
                                        type="textarea"
                                        rows={5}
                                        name="description"
                                        value={description}
                                        onChange={(e) => actions.handleDataSpeakingChange(e)}
                                        invalid={isEmpty(description) ? true : false}
                                    />
                                    <FormFeedback>{messages[`user.description`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.description" />
                            </Label>
                        </Colxx>
                    </Row>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-end" >
                    <Button color="info" outline
                        onClick={() => {
                            actions.handleToggleModalSpeaking();
                            actions.clear({ name: "dataSpeaking" });
                        }}
                    >
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button color="primary"
                        disabled={get(dataSpeaking, "isConfirmed", false)}
                        onClick={() => onSave()}
                    >
                        <IntlMessages id="button.save" />
                    </Button>
                    {/* {isEdit &&
                        <Button color="danger"
                            onClick={() => setModalDelete(!modaDelete)}
                        >
                            <IntlMessages id="speaking.delete" />
                        </Button>
                    } */}
                </ModalFooter>
            </Modal>
            <Modal isOpen={modaDelete}>
                <ModalBody className="text-center">
                    <Label className="h5" >
                        <IntlMessages id="speaking.deleteTitle" />
                    </Label>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-end" >
                    <Button
                        outline
                        onClick={() => setModalDelete(!modaDelete)}
                    >
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button
                        color="danger"
                        onClick={() => {
                            actions.deleteSpeaking({ data: dataSpeaking })
                            setModalDelete(!modaDelete)
                        }}
                    >
                        <span>
                        Confirm to delete
                        </span>
                    </Button>
                </ModalFooter>
            </Modal>
        </Fragment>
    );
}

export default React.memo(injectIntl(ModalControl));
