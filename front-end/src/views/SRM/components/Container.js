import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import SRM from "./SRMs";
import { isEmpty } from "lodash";

class Container extends PureComponent {
  componentDidMount() {
    const {speakes,speaking, countries, pagination, actions} = this.props
    if (isEmpty(speakes)) {
      actions.fetchAllSpeaker({ data: pagination });
    }
    if (isEmpty(speaking)) {
      actions.fetchAllSpeaking({ data: pagination });
    }
    if (isEmpty(countries)) {
      actions.fetchAllCountries({ data: {
        ...pagination,
        search: {
          usePagination: false,
          isExactly: true,
        }
      }});
    }
  }

  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <div className="loading"></div>}
        <SRM {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
