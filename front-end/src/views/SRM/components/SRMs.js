import React, { Fragment } from "react";
import { Row, Button, ButtonGroup, UncontrolledButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import moment from 'moment'

import { Colxx } from "theme/components/common/CustomBootstrap";
import Breadcrumb from "theme/containers/navs/Breadcrumb";
import classnames from "classnames";

import TableSpeakers from "./TableSpeakes";
import TableSpeaking from "./TableSpeaking";
import ModalControl from "./ModalSpeaker"
import ModalSpeaking from "./ModalSpeaking"

function SRM(props) {

  const { actions, tabs, match } = props;
  const {messages} = props.intl;

  const year =moment().format("YYYY")
  return (
    <Fragment>
      <Row>
        <Colxx xxs="12" className="d-flex justify-content-between">
          <div>
            {/* <Breadcrumb heading="menu.srms" match={match} /> */}
          </div>
          {tabs === "speakers" && <Button
            className="mx-1"
            color="primary"
            onClick={() => actions.handleToggleModal()}
          >
            <IntlMessages id="srms.button.listSpeakers" />
          </Button>}
          {tabs === "speaking" && (
            <ButtonGroup>
              <UncontrolledButtonDropdown setActiveFromChild>
                <DropdownToggle color="primary">
                  <i className="iconsminds-filter-2" />
                </DropdownToggle>
                <DropdownMenu>
                  {["1", "2", "3", "4", "5", "6", "7", "8", "9", "10","11","12"].map(o => (
                    <DropdownItem className="d-flex"
                    onClick={() => actions.fetchAllSpeaking({ data: {month: o, year} })}
                    >
                      <div className="mx-2">{messages["speaking.moth"]}  {o}</div>
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </UncontrolledButtonDropdown>
              <Button
                className="mx-1"
                color="primary"
                onClick={() => actions.handleToggleModalSpeaking()}
              >
                <IntlMessages id="srms.button.listSpeaking" />
              </Button>
            </ButtonGroup>)}
        </Colxx>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs">
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "speaking" })}
                onClick={() => {
                  actions.changeTabs("speaking");
                }}
              >
                <i className="simple-icon-layers" /> <IntlMessages id="srms.listSpeaking" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "speakers" })}
                onClick={() => {
                  actions.changeTabs("speakers");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="srms.listSpeakers" />
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={tabs} className="chat-app-tab-content mt-3">
            <TabPane tabId="speakers" className="chat-app-tab-pane">
              <TableSpeakers {...props} />
            </TabPane>
            <TabPane tabId="speaking" className="chat-app-tab-pane mt-3">
              <TableSpeaking  {...props} />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
      <ModalControl {...props} />
      <ModalSpeaking {...props} />
    </Fragment>
  );
}

export default React.memo(injectIntl(SRM));
