import React from "react";
import { Card, CardBody, CardImg } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import { isEmpty, get } from "lodash"

function SRM(props) {

  const { speakers } = props;
  const { actions, isLoading, pagination } = props;
  // const { all } = pagination.search;
  const { page, limit, totalPages } = pagination;
  const { messages } = props.intl;


  const onPageChange = (pageIndex) => {
    actions.fetchAllSpeaker({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllSpeaker({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
  };
  const dataListPeakers = [
    {
      Header: "",
      accessor: "avatar",
      className: "justify-center",
      width: 100,
      Cell: (props) => <CardImg top src={isEmpty(get(props, "value", ""))
        ? "/assets/img/avatar.png"
        : get(props, "value", "")}
        alt="Avatar" />
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {props.value}
      </p>
    },
    {
      Header: messages["user.gender"],
      accessor: "gender",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {props.value}
      </p>
    },
    {
      Header: messages["user.frequency"],
      accessor: "frequency",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">
        {messages[`user.frequency.${props.value}`]}
      </p>
    },
    {
      Header: messages["user.email"],
      accessor: "email",
      className: "justify-center",
      Cell: (props) => <div className="d-flex justify-content-center">
        <p className="text-muted">
          {props.value ? props.value : ""}
        </p>
      </div>
    },
  ];
  return (
    <Card className="mb-4">
      <CardBody>
        <ReactTable
          data={speakers}
          columns={dataListPeakers}
          TbodyComponent={CustomTbodyComponent}
          defaultPageSize={limit}
          sortable={false}
          filterable={false}
          showPageJump={true}
          PaginationComponent={DataTablePagination}
          showPageSizeOptions={true}
          pageSize={speakers.length + 1}
          page={page - 1}
          loading={isLoading}
          pages={totalPages}
          manual
          onPageChange={(pageIndex) => onPageChange(pageIndex)}
          onPageSizeChange={(pageSize, pageIndex) => onPageSizeChange(pageSize, pageIndex)}
          getTrProps={(state, rowInfo, column) => {
            return {
              className: "cursor-pointer",
              onClick: () => actions.getInfo({ data: rowInfo.original }),
            };
          }}
          className="-striped -highlight h-full overflow-hidden"
        />
      </CardBody>
    </Card>
  );
}

export default React.memo(injectIntl(SRM));
