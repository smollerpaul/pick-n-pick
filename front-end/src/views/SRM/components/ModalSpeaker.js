import React, { Fragment, useState } from "react";
import { Row, CardImg, FormGroup, Input, FormFeedback } from "reactstrap";
import { Button, Modal, ModalBody, ModalHeader, ModalFooter, Label } from "reactstrap";
import Select from "react-select";

import { Colxx } from "theme/components/common/CustomBootstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import CustomSelectInput from "theme/components/common/CustomSelectInput";
import "react-datepicker/dist/react-datepicker.css";
import { NotificationManager } from "theme/components/common/react-notifications";
import UploadImage from "theme/components/UploadImage";

import { isEmpty, get, find, isObject} from "lodash"

function ModalControl(props) {

    const { isEdit, isOpen } = props;
    const { actions, data } = props;
    const { messages } = props.intl;
    const { name, email, gender, introduceMySelf, frequency, countryId, facebook } = data;

    const [modalAvatar, setModal] = useState(false)
    ///option
    const genderOption = ["male", "female", "other"].map(o => ({
        label: messages[`user.gender.${o}`],
        value: o
    }))
    const frequencyOption = ["daily", "weekly", "every2weeks", "monthly", "quaterly"].map(o => ({
        label: messages[`user.frequency.${o}`],
        value: o
    }))

    const countriesOption = get(props, "countries", []).map(o => ({
        label: o.name,
        value: o._id
    }))

    const onSave = () => {
        // for (const item in data) {
        //     if (isEmpty(data[item].toString()) || data[item] === " ") return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.${item}`]}`);
        // }
        if(isEmpty(data.name)) return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.name`]}`);
        else if(isEmpty(data.gender)) return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.gender`]}`);
        else if(isEmpty(data.countryId)) return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.countryId`]}`);
        else if(isEmpty(data.introduceMySelf)) return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.introduceMySelf`]}`);
        else if(isEmpty(data.avatar)) return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.avatar`]}`);
        else return isEdit ? actions.editSpeaker({ data }) : actions.createNewSpeaker({ data });
    };

    return (
        <Fragment>
            <Modal isOpen={isOpen} size="lg">
                <ModalBody>
                    <ModalHeader className="d-flex justify-content-around" >
                        <CardImg className="top-right-button-container"
                            onClick={() => setModal(!modalAvatar)}
                            src={isEmpty(get(props, "data.avatar", "")) ? "/assets/img/avatar.png" : get(props, "data.avatar", "")}
                            alt="Avatar"
                        />
                    </ModalHeader>
                    <Row>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Input
                                        type="name"
                                        name="name"
                                        value={name}
                                        onChange={(e) => actions.handleInputChange(e)}
                                        invalid={isEmpty(name) ? true : false}
                                    />
                                    <FormFeedback>{messages[`user.name`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.name" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Select
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        components={{ Input: CustomSelectInput }}
                                        options={genderOption}
                                        name="gender"
                                        value={find(genderOption, (o) => o.value === gender)}
                                        onChange={(e) => {
                                            actions.handleInputChange({
                                                target: { name: "gender", value: e.value },
                                            });
                                        }}
                                    />
                                    <FormFeedback>{messages[`user.gender`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.gender" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Input
                                        type="email"
                                        name="email"
                                        value={email}
                                        onChange={(e) => actions.handleInputChange(e)}
                                    />
                                    <FormFeedback>{messages[`user.email`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.email" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Select
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        components={{ Input: CustomSelectInput }}
                                        options={countriesOption}
                                        name="countryId"
                                        value={find(countriesOption, (o) => o.value === (isObject(countryId)? countryId._id : countryId))}
                                        onChange={(e) => {
                                            actions.handleInputChange({
                                                target: { name: "countryId", value: e.value },
                                            });
                                        }}
                                    />
                                    <FormFeedback>{messages[`user.countries`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.countries" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Input
                                        type="facebook"
                                        name="facebook"
                                        value={facebook}
                                        onChange={(e) => actions.handleInputChange(e)}
                                    />
                                    <FormFeedback>{messages[`user.facebook`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.facebook" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6" >
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Select
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        components={{ Input: CustomSelectInput }}
                                        options={frequencyOption}
                                        name="frequency"
                                        value={find(frequencyOption, (o) => o.value === frequency)}
                                        onChange={(e) => {
                                            actions.handleInputChange({
                                                target: { name: "frequency", value: e.value },
                                            });
                                        }}
                                    />
                                    <FormFeedback>{messages[`user.frequency`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.frequency" />
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Input
                                        type="textarea"
                                        rows={5}
                                        name="introduceMySelf"
                                        value={introduceMySelf}
                                        onChange={(e) => actions.handleInputChange(e)}
                                        invalid={isEmpty(introduceMySelf) ? true : false}
                                    />
                                    <FormFeedback>{messages[`user.introduceMySelf`]}</FormFeedback>
                                </FormGroup>
                                <IntlMessages id="user.introduceMySelf" />
                            </Label>
                        </Colxx>
                    </Row>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-end" >
                    <Button color="info" outline
                        onClick={() => {
                            actions.handleToggleModal();
                            actions.clear({name: "data"});
                        }}
                    >
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button color="primary"
                        onClick={() => onSave()}
                    >
                        <IntlMessages id="button.save" />
                    </Button>
                    {/* {isEdit &&
                        <Button color="danger"
                            onClick={() => actions.deleteSpeaker({ data })}
                        >
                            <IntlMessages id="button.delete" />
                        </Button>
                    } */}
                </ModalFooter>
            </Modal>
            <Modal
                isOpen={modalAvatar}
                size="lg"
                toggle={() => setModal(!modalAvatar)}
                centered
                style={{ boxShadow: "none" }}
            >
                <ModalHeader toggle={() => setModal(!modalAvatar)}>
                    <IntlMessages id="brands.uploadAvatar" />
                </ModalHeader>
                <ModalBody>
                    <UploadImage
                        title="Upload Image"
                        number={1}
                        onRemove={file => actions.handleInputChange({
                            target: {
                                name: "avatar",
                                value: ""
                            }
                        })}
                        onUpload={file => {
                            actions.handleInputChange({
                                target: {
                                    name: "avatar",
                                    value: file
                                }
                            })
                            setModal(!modalAvatar)
                        }}
                        onChange={() => setModal(!modalAvatar)}
                    />
                </ModalBody>
            </Modal>
        </Fragment>
    );
}

export default React.memo(injectIntl(ModalControl));
