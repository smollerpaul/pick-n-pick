import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/srm";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllSpeaker(action) {
  try {
    const res = yield call(API.fetchAllSpeaker, action.payload.data);
    yield put(actions.fetchAllSpeakerSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload.data);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewSpeaker(action) {
  try {
    const res = yield call(API.createNewSpeaker, action.payload.data);
    yield put(actions.createNewSpeakerSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditSpeaker(action) {
  try {
    const res = yield call(API.editSpeaker, action.payload.data);
    yield put(actions.editSpeakerSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteSpeaker(action) {
  try {
    const res = yield call(API.deleteSpeaker, action.payload.data);
    yield put(actions.deleteSpeakerSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllSpeaking(action) {
  try {
    const res = yield call(API.fetchAllSpeaking, action.payload.data);
    yield put(actions.fetchAllSpeakingSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlegetInfoSpeaking(action) {
  try {
    const res = yield call(API.getInfoSpeaking, action.payload.data);
    yield put(actions.getInfoSpeakingSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewSpeaking(action) {
  try {
    const res = yield call(API.createNewSpeaking, action.payload.data);
    yield put(actions.createNewSpeakingSuccess(res));
    NotificationManager.success("Succes");
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditSpeaking(action) {
  try {
    const res = yield call(API.editSpeaking, action.payload.data);
    yield put(actions.editSpeakingSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteSpeaking(action) {
  try {
    const res = yield call(API.deleteSpeaking, action.payload.data);
    yield put(actions.deleteSpeakingSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleupdateTask(action) {
  try {
    const {data} = action.payload;
    for (let item of data) yield put(actions.editSpeaker({data: item}))
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllCountries(action) {
  try {
    const res = yield call(API.fetchAllCountries, action.payload.data);
    yield put(actions.fetchAllCountriesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///speaker
export function* fetchAllSpeaker() {
  yield takeAction(actions.fetchAllSpeaker, handleFetchAllSpeaker);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewSpeaker() {
  yield takeAction(actions.createNewSpeaker, handlecreateNewSpeaker);
}

export function* editSpeaker() {
  yield takeEvery(actions.editSpeaker, handleEditSpeaker);
}

export function* deleteSpeaker() {
  yield takeAction(actions.deleteSpeaker, handleDeleteSpeaker);
}

//// speaking
export function* fetchAllSpeaking() {
  yield takeAction(actions.fetchAllSpeaking, handlefetchAllSpeaking);
}

export function* getInfoSpeaking() {
  yield takeAction(actions.getInfoSpeaking, handlegetInfoSpeaking);
}

export function* createNewSpeaking() {
  yield takeAction(actions.createNewSpeaking, handlecreateNewSpeaking);
}

export function* editSpeaking() {
  yield takeEvery(actions.editSpeaking, handleEditSpeaking);
}

export function* deleteSpeaking() {
  yield takeAction(actions.deleteSpeaking, handleDeleteSpeaking);
}

export function* fetchAllCountries() {
  yield takeAction(actions.fetchAllCountries, handlefetchAllCountries);
}

export function* updateTask() {
  yield takeAction(actions.updateTask, handleupdateTask);
}

export default [
  fetchAllSpeaker, 
  createNewSpeaker,
  editSpeaker,
  deleteSpeaker,
  fetchAllSpeaking,
  getInfoSpeaking,
  createNewSpeaking,
  editSpeaking,
  deleteSpeaking,
  getInfo,
  updateTask,
  fetchAllCountries,
];
