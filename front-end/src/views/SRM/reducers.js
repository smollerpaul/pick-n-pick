/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";

export const name = "SRM";

const initialState = freeze({
  data: {},
  dataSpeaking: {
    "topic": " ",
    "description": " ",
    "speakerId": " ",
  },
  countries: [],
  modalAvatar: false,
  tabs: "speaking",
  message: "",
  isOpen: false,
  isModalSpeaking: false,
  isLoading: false,
  speakers: [],
  speaking: [],
  listProcessing: [],
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      usePagination: true,
      isExactly: false,
    },
  },
});

export default handleActions(
  {
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.fetchAllSpeaker]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        data: {
          ...state.data,
          [name]: value,
        },
      });
    },
    [actions.fetchAllSpeakerSuccess]: (state, action) => {
      return freeze({
        ...state,
        speakers: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
     [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
      });
    },
    [actions.handleToggleModalSpeaking]: (state, action) => {
      return freeze({
        ...state,
        isModalSpeaking: !state.isModalSpeaking,
      });
    },
    [actions.createNewSpeakerSuccess]: (state, action) => {
      return freeze({
        ...state,
        speakers: [action.payload.data, ...state.speakers],
        data: [],
        isOpen: false,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isOpen: true,
        isEdit: true,
        data: action.payload.data,
      });
    },
    [actions.editSpeakerSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        speakers: [ ...state.speakers.map(item => {
          if (item._id === action.payload.data._id) {
            return action.payload.data;
          }
          return item;
        })]
      });
    },
    [actions.deleteSpeakerSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        speakers: [...state.speakers.filter(o => o._id !== action.payload.data)]
      });
    },
    ///Speaking
    [actions.fetchAllSpeaking]: (state, action) => {
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
        isLoading: false,
      });
    },
    [actions.fetchAllSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        speaking: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.getInfoSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        isModalSpeaking: true,
        isEdit: true,
        dataSpeaking: action.payload.data,
      });
    },
    [actions.handleDataSpeakingChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        dataSpeaking: {
          ...state.dataSpeaking,
          [name]: value,
        },
      });
    },
    [actions.createNewSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        dataSpeaking: [],
        speaking: [...state.speaking, action.payload.data],
        isModalSpeaking: false,
      });
    },
    [actions.editSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isModalSpeaking:false,
        data: initialState.data,
        speaking: [ ...state.speaking.map(item => {
          if (item._id === action.payload.data._id) {
            return action.payload.data;
          }
          return item;
        })]
      });
    },
    [actions.deleteSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isModalSpeaking:false,
        data: initialState.data,
        speaking: [...state.speaking.filter(o => o._id !== action.payload.data)]
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value
          }
        }
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.fetchAllCountriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        countries: action.payload.data,
      });
    },
    [actions.clear]: (state, action) => {
      const {name} = action.payload
      return freeze({
        ...state,
        isEdit: false,
        [name]: initialState[name],
      });
    },
  },
  initialState
);
