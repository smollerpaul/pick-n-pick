/**
 * @file constants
 */

export const CHECK_LOGIN = "WRAPPEDMENU/CHECK_LOGIN";
export const CHECK_LOGIN_SUCCESS = "WRAPPEDMENU/CHECK_LOGIN_SUCCESS";
export const CHECK_LOGIN_FAIL = "WRAPPEDMENU/CHECK_LOGIN_FAIL";

export const SAVE_TOKEN = "WRAPPER/SAVE_TOKEN"

export const CHECK_UPDATE = "WRAPPEDMENU/CHECK_UPDATE";

export const STORES_BRANDS = "WRAPPEDMENU/STORES_BRANDS"
export const CHANGE_TITLE = "WRAPPEDMENU/CHANGE_TITLE";