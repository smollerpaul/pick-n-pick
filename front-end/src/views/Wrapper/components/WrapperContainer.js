import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import { Helmet } from "react-helmet";
import { isEmpty } from "lodash";
import { defaultTitle } from "constants/defaultValues";
import { injectIntl } from "react-intl";

class WrapperContainer extends Component {
  componentDidMount() {
    if (isEmpty(this.props.userInfo)) {
      this.props.actions.checkLogin({ history: this.props.history });
    }
  }

  render() {
    return (
        <React.Fragment>
          <Helmet>
            <meta charSet="utf-8" />
            <meta name="revisit-after" content="1 days" />
            <meta name="robots" content="index, follow" />
            <meta name="description" content={defaultTitle[this.props.locale]} />
            <meta http-equiv="content-language" content={this.props.locale} />
            <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
            <meta property="og:url" content={window.location.href} />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={"LSE Management System"} />
            <meta property="og:description" content={defaultTitle[this.props.locale]} />
            <meta
              property="og:image"
              content="https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/80313959_111074243726774_3590473590077652992_n.jpg"
            />
            <title>LSE Management System</title>
          </Helmet>
          {this.props.children}
        </React.Fragment>
      )
  }
}

function mapStateToProps(state) {
  return {
    ...state[name],
    locale: state.settings.locale
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(injectIntl(WrapperContainer))
);
