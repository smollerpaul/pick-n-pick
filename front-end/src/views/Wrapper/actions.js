/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const checkLogin = createAction(CONST.CHECK_LOGIN);
export const checkLoginSuccess = createAction(CONST.CHECK_LOGIN_SUCCESS);
export const checkLoginFail = createAction(CONST.CHECK_LOGIN_FAIL);

export const saveToken = createAction(CONST.SAVE_TOKEN);

export const checkUpdate = createAction(CONST.CHECK_UPDATE);
export const storeBrands = createAction(CONST.STORES_BRANDS);
export const changeTitle = createAction(CONST.CHANGE_TITLE);
