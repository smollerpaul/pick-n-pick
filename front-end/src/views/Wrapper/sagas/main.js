import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";
import _ from "lodash";
import { get } from "services/localStoredService";
import * as API from "apis/wrapper";

export function* handleCheckLogin(action) {
  try {
    let accessToken = get("accessToken");
    if (_.isEmpty(accessToken)) {
      yield put(actions.checkLoginFail());
      action.payload.history.push("/user/login");
    } else {
      const res = yield call(API.getUserInfo);
      yield put(actions.checkLoginSuccess(res.data));
    }
  } catch (err) {
    yield put(actions.checkLoginFail(err));
  }
}

export function* checkLogin() {
  yield takeAction(actions.checkLogin, handleCheckLogin);
}

export default [checkLogin];
