/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Wrapper";

const initialState = freeze({
  notifications: [],
  token: "",
  brands: [],
  userInfo: {},
  currentTitle: ""
});

export default handleActions(
  {
    [actions.changeTitle]: (state, action) => {
      return freeze({
        ...state,
        currentTitle: action.payload
      })
    },
    [actions.storeBrands]: (state, action) => {
      return freeze({
        ...state,
        brands: action.payload
      })
    },
    [actions.saveToken]: (state, action) => {
      return freeze({
        ...state,
        token: action.payload
      });
    },
    [actions.checkLoginSuccess]: (state, action) => {
      return freeze({
        ...state,
        userInfo: action.payload
      })
    },
    [actions.checkUpdate]: (state, action) => {
      return freeze({
        ...state,
        userInfo: action.payload
      })
    },
  },
  initialState
);
