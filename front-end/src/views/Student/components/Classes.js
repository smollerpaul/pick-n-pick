import React, { Fragment, useState } from "react";
import { Row,Label, Button, Modal } from "reactstrap";
import { Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap";
import { FormGroup, Input, FormFeedback, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";

import classnames from "classnames";
import ClassList from "./ClassList";
import { Colxx } from "theme/components/common/CustomBootstrap";
import ModalControl from "./InfoSpeaker"
import { get, isEmpty } from "lodash";
import TableCLassSpeadking from "./TableCLassSpeadking";
import Topic from "./Topic"

function Students(props) {
  const { actions, pagination, tabs, data } = props;

  const { messages } = props.intl;

  const [modalOpen, setModalOpen] = useState(false);

  return (
    <Fragment>
      <Row>
        <Colxx xxs="12" className="header-top">
          {/* <Breadcrumb heading="menu.students" match={match} /> */}
        </Colxx>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs">
            {/* <NavItem>
              <NavLink
                className={classnames({ active: tabs === "pendding" })}
                onClick={() => {
                  actions.changeTabs("pendding");
                }}
              >
                <i className="simple-icon-layers" /> <IntlMessages id="speaking.listSpeaking" />
              </NavLink>
            </NavItem> */}
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "table" })}
                onClick={() => {
                  actions.changeTabs("table");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="speaking.table" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "topic" })}
                onClick={() => {
                  actions.changeTabs("topic");
                  actions.fetchAllSpeakingConfirm({data: pagination})
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="user.topic" />
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={tabs} className="chat-app-tab-content mt-3">
            <TabPane tabId="table" className="chat-app-tab-pane">
              <TableCLassSpeadking {...props} />
            </TabPane>
            <TabPane tabId="pendding" className="chat-app-tab-pane mt-3">
              {tabs === "pendding" && <ClassList {...props} />}
            </TabPane>
            <TabPane tabId="topic" className="chat-app-tab-pane mt-3">
              {tabs === "topic" && <Topic {...props} />}
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
      <Modal isOpen={modalOpen} size="md">
        <ModalBody>
          <ModalHeader>
            <Label>
              <IntlMessages id="modalDelete.sure" />
            </Label>
          </ModalHeader>
          <FormGroup>
            <Label>
              <IntlMessages id="user.name" />
            </Label>
            <Input
              type="textarea"
              rows={5}
              name="rejectReason"
              value={get(data, "rejectReason", "")}
              onChange={(e) => actions.handleInputChange(e)}
              invalid={isEmpty(get(data, "rejectReason", "")) ? true : false}
            />
            <FormFeedback>{messages[`students.rejectReason`]}</FormFeedback>
          </FormGroup>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-around">
          <Button
            color="info"
            className="default"
            outline
            onClick={() => {
              setModalOpen(!modalOpen);
              actions.clear();
            }}
          >
            <IntlMessages id="button.cancel" />
          </Button>
          <Button
            color="danger"
            className="default"
            outline
            onClick={() => {
              actions.handleStudentsPendding({
                data: {
                  interviewId: get(data, "interviewId", ""),
                  accept: false,
                  rejectReason: get(data, "rejectReason", ""),
                },
              });
              setModalOpen(!modalOpen);
            }}
          >
            <IntlMessages id="button.reject" />
          </Button>
        </ModalFooter>
      </Modal>
      <ModalControl {...props} />
    </Fragment>
  );
}

export default React.memo(injectIntl(Students));
