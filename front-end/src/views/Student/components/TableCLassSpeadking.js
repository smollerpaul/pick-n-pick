import React, { Fragment, useState } from "react";
import { Card, CardBody, Label, CardImg, Button, Modal } from "reactstrap";
import { ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import moment from "moment";
import { get, isEmpty, find } from "lodash";
function getWeekDays(curr, firstDay = 1 /* 0=Sun, 1=Mon, ... */) {
  // var cd = curr.getDate() - curr.getDay();
  // var from = new Date(curr.setDate(cd + firstDay));
  // var to = new Date(curr.setDate(cd + 6 + firstDay));
  let start  = new Date(moment(curr).startOf('week'))
  let end  = new Date(moment(curr).endOf('week'))
  return {
    from: moment(start).add(1, 'days'),
    to: moment(end).add(1, 'days'),
  };
}


function Students(props) {
  const { actions, isLoading, pagination, classes, userInfo } = props;
  const { page, limit, totalPages } = pagination;
  const { messages } = props.intl;

  const [modalCofirm, setModalCofirm] = useState(false);
  const [vote, setVote] = useState({ votings: [] });
  const [isConfirm, setIsConfirm] = useState(false);


  const onPageChange = (pageIndex) => {
    actions.fetchAllStudentClassSpeaking({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllStudentClassSpeaking({
      data: { ...pagination, page: pageIndex + 1, limit: pageSize },
    });
  };
  const dataTableColumns = [
    {
      Header: messages["user.avatar"],
      accessor: "speakerId.avatar",
      className: "justify-center",
      width: 80,
      Cell: (props) => (
        <CardImg
          top
          src={
            isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
          }
          alt="Avatar"
        />
      ),
    },
    {
      Header: messages["speaking.name"],
      accessor: "speakerId.name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: 'Nationality',
      accessor: "speakerId.countryId.name",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p>{props.value}</p>
        </div>
      ),
    },
    {
      Header: messages["speaking.topic"],
      accessor: "topic",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: "Votes",
      accessor: "votings",
      className: "d-flex justify-content-center",
      width: 80,
      Cell: ({ value }) => <p>{value.length}</p>,
    },
    {
      Header: "Date & Time",
      accessor: "happenDate",
      className: "justify-center",
      width: 250,
      Cell: (props) => <p className="text-capitalize ">{moment(props.value).format("LLLL")}</p>,
    },
    {
      Header: "Register",
      accessor: "_id",
      className: "d-flex justify-content-center",
      Cell: ({ value, original }) => {
        if (original.isConfirmed) return null;
        const unRegister = find(original.votings, o => o.studentId === userInfo._id);
        return  unRegister ? (<Button color="warning" outline 
        onClick={(e) => {
          e.stopPropagation()
          actions.unJoinClass({data: original})
        }}
        >
        <IntlMessages id="button.unregister" />
      </Button>) : (
          <Button>
            <IntlMessages id="button.registed" />
          </Button>
        );
      },
    },
    {
      Header: messages["user.isCofirmed"],
      accessor: "isConfirmed",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p>{messages[`user.isConfirmed.${props.value}`]}</p>
        </div>
      ),
    },
  ];

  const dataTableModalColumns = [
    {
      Header: "",
      accessor: "studentId.avatar",
      className: "justify-center",
      width: 80,
      Cell: (props) => (
        <CardImg
          top
          src={
            isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
          }
          alt="Avatar"
        />
      ),
    },
    {
      Header: messages["speaking.student"],
      accessor: "studentId.name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: messages["speaking.topic"],
      accessor: "studentId.email",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: messages["speaking.phone"],
      accessor: "studentId.englishLevel",
      className: "justify-center",
      Cell: (props) => (
        <p className="text-capitalize ">{messages[`user.englishLevel.${props.value}`]}</p>
      ),
    },
  ];

  // .setHours(0, 0, 0, 0)

  const today = new Date(new Date().setHours(0, 0, 0, 0));
  const weeks = [
    getWeekDays(new Date(today)),
    getWeekDays(new Date(today.setDate(today.getDate() + 7))),
    getWeekDays(new Date(today.setDate(today.getDate() + 7))),
    getWeekDays(new Date(today.setDate(today.getDate() + 7))),
  ];

  // const dataModalOption = get(vote, "votings", []).filter(o => o.studentId !== null)
  return (
    <Fragment>
      {weeks.map((o) => {
        const option = classes.filter((p) => {
          let date = new Date(p.happenDate);
          return date >= new Date(o.from) && date <= new Date(o.to);
        })
        // const classOptions = option.filter(o => o.isConfirmed === false)
        const listConfirm = option.filter(o => {
          if(o.isConfirmed === true) return o
        })
        return(
          <Fragment>
            <Card className="mb-4">
              <CardBody>
                <h5>
                  <IntlMessages id="speaking.from" /> {moment(o.from).format("DD/MM")}{" "}
                  <IntlMessages id="speaking.to" /> {moment(o.to).format("DD/MM")}
                </h5>
                <ReactTable
                  data={isEmpty(listConfirm) ? option : listConfirm}
                  columns={dataTableColumns}
                  TbodyComponent={CustomTbodyComponent}
                  defaultPageSize={limit}
                  sortable={false}
                  filterable={false}
                  showPageJump={true}
                  PaginationComponent={DataTablePagination}
                  showPageSizeOptions={true}
                  pageSize={isEmpty(listConfirm) ? option.length + 1 : listConfirm.length + 1 }
                  page={page - 1}
                  loading={isLoading}
                  pages={totalPages}
                  manual
                  onPageChange={(pageIndex) => onPageChange(pageIndex)}
                  onPageSizeChange={(pageSize, pageIndex) => onPageSizeChange(pageSize, pageIndex)}
                  getTrProps={(state, rowInfo, column) => {
                    return {
                      className: "cursor-pointer",
                      onClick: () => actions.handleToggleModal({ data: rowInfo.original }),
                    };
                  }}
                  className="-striped -highlight h-full overflow-hidden"
                />
              </CardBody>
            </Card>
          </Fragment>
        )
      })}
    </Fragment>
  );
}

export default React.memo(injectIntl(Students));
