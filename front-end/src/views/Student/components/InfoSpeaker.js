import React from "react";
import { CardImg, CardTitle, Modal, ModalBody, ModalFooter, Button, Row, Label, Card } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import { Colxx } from "theme/components/common/CustomBootstrap";

import { injectIntl } from "react-intl";
import { get, find } from "lodash"
function ClassesDetail(props) {
    const { dataModal, isOpen, actions, userInfo } = props;
    const { messages } = props.intl
    const unRegister = find(dataModal.votings, o => o.studentId === userInfo._id);
    console.log("data", dataModal)
    return (
        <Modal isOpen={isOpen} style={{ backgroundColor: '#6639D' }} size="md" >
            <ModalBody >
                <div className="text-center" >
                    <CardImg top  src={get(dataModal, "speakerId.avatar", "")} />
                </div>
                <div>
                    <Label className="h2 d-block my-2 px-2 " style={{ backgroundColor: "#BF9553", color: "white" }}>
                        <IntlMessages id="user.info" />
                    </Label>
                </div>
                <Row>
                    <Colxx xxs="12" className="ml-2">
                        <CardTitle className="my-2 h5" >
                            <IntlMessages id="user.name" /> :
                            <span className="ml-1">{get(dataModal, "speakerId.name", "")}</span>
                        </CardTitle>
                    </Colxx>
                    <Colxx xxs="12" className="ml-2">
                        <CardTitle className="my-2 h5" >
                            <IntlMessages id="user.gender" /> :
                            <span className="ml-1">{get(dataModal, "speakerId.gender", "")}</span>
                        </CardTitle>
                    </Colxx>
                    <Colxx xxs="12" className="ml-2">
                        <CardTitle className="my-2 h5" >
                            <IntlMessages id="user.frequency" /> :
                            <span className="ml-1">{get(dataModal, "speakerId.frequency", "")}</span>
                        </CardTitle>
                    </Colxx>
                    <Colxx xxs="12" className="ml-2">
                        <CardTitle className="my-2 h5" >
                            <IntlMessages id="user.introduceMySelf" /> :
                            <span className="ml-1">{get(dataModal, "speakerId.introduceMySelf", "")}</span>
                        </CardTitle>
                    </Colxx>
                    <Colxx xxs="12" className="ml-2">
                        <CardTitle className="my-2 h5" >
                            <IntlMessages id="user.countries" /> :
                            <span className="ml-1">{get(dataModal, "speakerId.countryId.name", "")}</span>
                        </CardTitle>
                    </Colxx>
                </Row>
            </ModalBody>
            <ModalFooter className="d-flex justify-content-around" >
                <Button color="info" outline onClick={() => actions.clearDataModal({ name: "dataModal" })}>
                    <IntlMessages id="button.close" />
                </Button>
                <Button color="primary"
                    disabled={dataModal.isConfirmed}
                    onClick={() => {
                        actions.joinClass({ data: dataModal._id })
                        actions.clearDataModal({ name: "dataModal" })
                    }
                    }
                >
                    <IntlMessages id="button.registed" />
                </Button>
            </ModalFooter>
        </Modal>
    );
}

export default React.memo(injectIntl(ClassesDetail));
