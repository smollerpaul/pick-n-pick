import React, { Fragment } from "react";
import Board from "react-trello";
import CustomCard from "./CustomCard";
import { find, get } from "lodash";

function ClassList(props) {
  const { messages } = props.intl;
  const { lanes, actions, classes } = props;
  const newLands = JSON.parse(
    JSON.stringify({
      lanes: lanes.map((p) => ({
        ...p,
        id: `${p.title}-${Date.now()}`,
        label: "",
        title: messages[`student.classSpeaking.${p.title}`],
      })),
    })
  );
  return (
    <Fragment>
      <Board
        laneDraggable={false}
        cardDraggable={false}
        data={newLands}
        components={{ Card: CustomCard }}
        onCardClick={(cardId) => {
          actions.getSpeaking({data: cardId})
          // let mySpeaker = find(classes, (p) => p._id === cardId);
          // if (!mySpeaker.hasRegisted) {
          //   actions.joinClass({ data: cardId });
          // }
        }}
        classNames="card cardbody"
      />
    </Fragment>
  );
}

export default ClassList;
