import React, { Fragment, useState } from "react";
import { Card, CardBody, Button, Modal, CardTitle, ModalFooter } from "reactstrap";
import {  Input, ModalBody, ModalHeader, CardImg } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import moment from "moment";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import { get, isEmpty } from "lodash";

function Students(props) {
    const { actions, isLoading, pagination, dataTopic, speakingConfirm } = props;

    const { limit } = pagination;
    const { messages } = props.intl;

    const [isModaleClip, setModal] = useState(false);
    const [isModaleTopic, setModalTopic] = useState(false);
    const [dataUpload, setData] = useState(Object);

    const speakingConfirmOption = speakingConfirm.map((o, index) => ({ index: index + 1, ...o }))
    const dataTopicOption = dataTopic.map((o, index) => ({ index: index + 1, ...o }))

    const dataTableColumns = [
        {
            Header: "NO",
            accessor: "index",
            className: "justify-center",
            width: 60,
            Cell: (props) => <p className="text-muted text-small mb-0">{props.value}</p>,
        },
        {
            Header: messages["user.topic"],
            accessor: "topic",
            className: "justify-center",
            Cell: (props) => <p className="text-muted text-small mb-0">{props.value}</p>,
        },
        {
            Header: 'Speaker',
            accessor: "speakerId.name",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex text-muted text-small mb-0">
                    <p>{props.value}</p>
                </div>
            ),
        },
        {
            Header: 'Nationality',
            accessor: "speakerId.countryId.name",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex text-muted text-small mb-0">
                    <p>{props.value}</p>
                </div>
            ),
        },
        {
            Header: "Quantity Clip",
            accessor: "speakingVideoQuantity",
            className: "justify-center",
            width: 60,
            Cell: (props) => <p className="text-muted text-small mb-0" >{props.value}</p>,
        },
        {
            Header: "Date & Time",
            accessor: "happenDate",
            className: "justify-center",
            Cell: (props) => (
                <p className="text-muted text-small mb-0">
                    {moment(props.value).format('DD/MM/YYYY')}
                </p>
            ),
        },
        {
            Header: "Video",
            accessor: "myVideo",
            className: "justify-center",
            Cell: ({ original, value }) =>
                <Button
                disabled={original.isCompleted}
                outline
                    onClick={(e) => {
                        e.stopPropagation();
                        setModal(!isModaleClip)
                        setData(original)
                    }}
                >{value ? "Re-upload" : "Upload"}
                </Button>,
        },
        {
            Header: messages["user.isCompleted"],
            accessor: "isCompleted",
            className: "justify-center",
            width: 80,
            Cell: (props) => (
              <div className="d-flex justify-content-center">
                <p>{messages[`user.isCompleted.${props.value}`]}</p>
              </div>
            ),
          },
    ];

    const dataTableStudents = [
        {
            Header: "NO",
            accessor: "index",
            className: "justify-center",
            width: 60,
            Cell: (props) => <p className="text-muted text-small mb-0">{props.value}</p>,
        },
        {
            Header: messages["user.avatar"],
            accessor: "uploadBy.avatar",
            className: "justify-center",
            width: 100,
            Cell: (props) => (
                <CardImg
                    top
                    src={
                        isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
                    }
                    alt="Avatar"
                />
            ),
        },
        {
            Header: 'Name',
            accessor: "uploadBy.name",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex text-muted text-small mb-0">
                    <p>{props.value}</p>
                </div>
            ),
        },
        {
            Header: 'Birth Day',
            accessor: "uploadBy.birthDate",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex text-muted text-small mb-0">
                    <p>{moment(props.value).format("DD-MM-YYYY")}</p>
                </div>
            ),
        },
        {
            Header: "Phone",
            accessor: "uploadBy.phone",
            className: "justify-center",
            width: 60,
            Cell: (props) => <p className="text-muted text-small mb-0" >{props.value}</p>,
        },
        {
            Header: "Email",
            accessor: "uploadBy.email",
            className: "justify-center",
            Cell: (props) => (
                <p className="text-muted text-small mb-0">
                    {props.value}
                </p>
            ),
        },
        {
            Header: "Faculty",
            accessor: "uploadBy.faculty",
            className: "justify-center",
            Cell: (props) =>
                <p className="text-muted text-small mb-0">{props.value}</p>,
        },
        {
            Header: "Intro",
            accessor: "uploadBy.videoIntroduce",
            className: "justify-center",
            Cell: (props) => <a className="text-reset" href={`${props.value}`} target="_blank" >View</a>
        },
        {
            Header: "Video",
            accessor: "linkVideo",
            className: "justify-center",
            Cell: (props) => <a className="text-reset" href={`${props.value}`} target="_blank" >View</a>
        },
    ];

    return (
        <Fragment>
            <Card className="mb-4">
                <CardBody>
                    <ReactTable
                        data={speakingConfirmOption}
                        columns={dataTableColumns}
                        TbodyComponent={CustomTbodyComponent}
                        defaultPageSize={limit}
                        pageSize={speakingConfirmOption.length + 1}
                        sortable={false}
                        filterable={false}
                        showPageJump={true}
                        PaginationComponent={DataTablePagination}
                        showPageSizeOptions={true}
                        loading={isLoading}
                        getTrProps={(state, rowInfo, column) => {
                            return {
                                onClick: () => {
                                    setData(rowInfo.original);
                                    setModalTopic(!isModaleTopic);
                                    actions.fetchTopic({ data: rowInfo.original })
                                },
                            };
                        }}
                        className="-striped -highlight h-full overflow-hidden"
                    />
                </CardBody>
            </Card>
            <Modal
                isOpen={isModaleClip}
                size="lg"
                toggle={() => setModal(!isModaleClip)}
                centered
                style={{ boxShadow: "none" }}
            >
                <ModalHeader toggle={() => setModal(!isModaleClip)}>
                    <span>
                        UPLOAD VIDEO CLIP
                    </span>
                </ModalHeader>
                <ModalBody>
                    <Input
                        onChange={(e) => {
                            actions.uploadClip({ data: { speakingId: dataUpload._id, linkVideo: e.target.value } });
                        }}
                    />
                </ModalBody>
                <ModalFooter className="d-flex justify-content-around">
                    <Button outline onClick={() => setModal(!isModaleClip)} >Cancel</Button>
                    <Button outline onClick={() => setModal(!isModaleClip)} >Ok</Button>
                </ModalFooter>
            </Modal>
            <Modal
                isOpen={isModaleTopic}
                size="xl"
                toggle={() => setModalTopic(!isModaleTopic)}
                centered
                style={{ boxShadow: "none" }}
            >
                <ModalBody >
                    <ModalHeader>
                        Detail
                    </ModalHeader>
                    <div>
                        <CardTitle>
                            Topic
                        </CardTitle>
                        <ReactTable
                        data={[dataUpload]}
                        columns={dataTableColumns}
                        TbodyComponent={CustomTbodyComponent}
                        pageSize={[dataUpload].length + 1}
                        loading={isLoading}
                        PaginationComponent={DataTablePagination}
                        getTrProps={(state, rowInfo, column) => {
                            return {
                                onClick: () => {
                                    setData(rowInfo.original);
                                    setModalTopic(!isModaleTopic);
                                    actions.fetchTopic({ data: rowInfo.original })
                                },
                            };
                        }}
                        className="-striped -highlight h-full overflow-hidden"
                    />
                    </div>
                    <div>
                        <CardTitle>
                            List student
                        </CardTitle>
                        <ReactTable
                        data={dataTopicOption}
                        columns={dataTableStudents}
                        TbodyComponent={CustomTbodyComponent}
                        defaultPageSize={limit}
                        pageSize={dataTopicOption.length + 1}
                        sortable={false}
                        filterable={false}
                        showPageJump={true}
                        PaginationComponent={DataTablePagination}
                        loading={isLoading}
                        className="-striped -highlight h-full overflow-hidden"
                    />
                    </div>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-center">
                    <Button outline onClick={() => setModalTopic(!isModaleTopic)} >OK</Button>
                </ModalFooter>
            </Modal>
        </Fragment>
    );
}

export default React.memo(injectIntl(Students));
