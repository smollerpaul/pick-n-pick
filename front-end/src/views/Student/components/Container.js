import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Classes from "./Classes";
import { isEmpty } from "lodash";
import { CardTitle, Card, CardBody, CardText, Input, Button, CardFooter } from "reactstrap";
class Container extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { code: "", waiting: false };
  }
  componentDidMount() {
    const { pagination, actions, userInfo, classes } = this.props;
    if (isEmpty(classes)) actions.fetchAllClasses({ data: pagination, userInfo });
    // if (isEmpty(checkInfo)) actions.checkValidate();
  }

  render() {
    const { isLoading, userInfo, actions, history } = this.props;
    const { code, waiting } = this.state;
    if (userInfo.isVerify === false) {
      return (
        <div className="d-flex justify-content-center">
          <Card style={{ width: "30vw" }} className="text-center">
            <CardBody>
              <CardTitle className="text-uppercase">
                Please Enter
            </CardTitle>
              <CardText>
                {`The validation code that get from email ${userInfo.email}`}
              </CardText>
              <Input value={code} onChange={(e) => {
                const { value } = e.target
                this.setState({ code: value })
              }} />
            </CardBody>
            <CardFooter>
              <Button className="mr-2" disabled={waiting} onClick={() => {
                actions.resendEmail()
                this.setState({ waiting: true })
                setTimeout(() => {this.setState({ waiting: false })}, 60000)
              }}
              >Resend code</Button>
              <Button color="success" onClick={() => actions.checkValidate({ data: { code }, history })}>Send code</Button>
            </CardFooter>
          </Card>
        </div>
      )
    }
    return (
      <React.Fragment>
        {isLoading && <div className="loading"></div>}
        <Classes {...this.props} />
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    ...state[name],
    userInfo: state["Wrapper"].userInfo,
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action,
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
