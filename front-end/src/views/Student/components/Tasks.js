import React, { Fragment, PureComponent } from "react";
import Board from "react-trello";
import { isEmpty, flattenDeep } from "lodash";
import { injectIntl } from "react-intl";
import CustomCard from "./CustomCard";

class Tasks extends PureComponent {
  render() {
    const {
      monday,
      tuesday,
      wednesday,
      thursday,
      friday,
      saturday,
      sunday,
      messages,
      actions } = this.props;
    const dataTable = JSON.parse(
      JSON.stringify({
        lanes: [{
          id: 1,
          title: messages["student.classSpeaking.monday"],
          cards: isEmpty(monday) ? [] : flattenDeep(flattenDeep(monday).map(e => ({
            id: e._id,
            title: e.topic,
            // description: e.description,
            happenDate: e.happenDate,
          })))
        },
        {
          id: 2,
          title: messages["student.classSpeaking.tuesday"],
          cards: isEmpty(tuesday) ? [] : flattenDeep(flattenDeep(tuesday).map(e => ({
            id: e._id,
            title: e.topic,
            // description: e.description,
            happenDate: e.happenDate,
          })))
        },
        {
          id: 3,
          title: messages["student.classSpeaking.wednesday"],
          cards: isEmpty(wednesday) ? [] : flattenDeep(flattenDeep(wednesday).map(e => ({
            id: e._id,
            title: e.topic,
            // description: e.description,
            happenDate: e.happenDate,
          })))
        },
        {
          id: 4,
          title: messages["student.classSpeaking.thursday"],
          cards: isEmpty(thursday) ? [] : flattenDeep(flattenDeep(thursday).map(e => ({
            id: e._id,
            title: e.topic,
            // description: e.description,
            happenDate: e.happenDate,
          })))
        },
        {
          id: 5,
          title: messages["student.classSpeaking.friday"],
          cards: isEmpty(friday) ? [] : flattenDeep(flattenDeep(friday).map(e => ({
            id: e._id,
            title: e.topic,
            // description: e.description,
            happenDate: e.happenDate,
          })))
        },
        {
          id: 6,
          title: messages["student.classSpeaking.saturday"],
          cards: isEmpty(saturday) ? [] : flattenDeep(flattenDeep(saturday).map(e => ({
            id: e._id,
            title: e.topic,
            // description: e.description,
            happenDate: e.happenDate,
          })))
        },
        {
          id: 7,
          title: messages["student.classSpeaking.sunday"],
          cards: isEmpty(sunday) ? [] : flattenDeep(flattenDeep(sunday).map(e => ({
            id: e._id,
            title: e.topic,
            // description: e.description,
            happenDate: e.happenDate,
          })))
        },
        ]
      }
      )
    )
    return (
      <Fragment>
        <Board
          data={dataTable}
          collapsibleLanes
          onCardClick={(e) => actions.getInfo({ data: { _id: e } })}
          classNames="card cardbody"
          components={{ Card: CustomCard }}
        />
      </Fragment>
    );
  }
}

export default React.memo(injectIntl(Tasks));
