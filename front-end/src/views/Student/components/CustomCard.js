import React, { Fragment } from "react";
import { MovableCardWrapper } from "react-trello/dist/styles/Base";
import moment from "moment";
import { Button } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as action from "../actions";
import { get } from "lodash";
import Truncate from "react-truncate";
const CustomCard = (props) => {
  const { onClick, className, title, happenDate, description, actions, speakerId } = props;
  const { hasRegisted } = props;
  return (
    <Fragment>
      <MovableCardWrapper onClick={onClick} className={className}>
        <header
          style={{
            ...(description ? { borderBottom: "1px solid #eee" } : null),
            paddingBottom: 6,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <div className="text-capitalize" style={{ fontSize: 14, fontWeight: "bold" }}>
            {title}
            <br />
          </div>
        </header>
        <Truncate lines={1} ellipsis={<span>...</span>}>
            {" "}
            <p className="pt-1">{description}</p>
          </Truncate>
        {happenDate && (
          <p style={{ fontSize: 11 }} className="pb-0 mb-0">
            <i className="iconsminds-timer" /> {moment(happenDate).format('llll')}
          </p>
        )}
        <p style={{ fontSize: 11 }} className="pb-0 mb-0">
          <i className="iconsminds-microphone-4" /> {get(speakerId, "name", "")}
        </p>
        {!hasRegisted && (
          <div className="text-center my-2">
            <Button
              color="primary"
              outline
              size="sm"
              onClick={() => actions.joinClass({ data: speakerId })}
            >
              <IntlMessages id="button.join" />
            </Button>
          </div>
        )}
      </MovableCardWrapper>
    </Fragment>
  );
};

function mapDispatchToProps(dispatch) {
  const actions = { joinClass: action.joinClass };
  return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(null, mapDispatchToProps)(CustomCard);
