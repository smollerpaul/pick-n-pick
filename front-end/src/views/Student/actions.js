/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("STUDENTS/HANDLE_INPUT_CHANGE");

export const handleToggleModal = createAction("STUDENTS/HANDLE_TOGGLE_MODAL");

export const fetchAllClasses = createAction("STUDENTS/FETCH_ALL_CLASSES");
export const fetchAllClassesSuccess = createAction("STUDENTS/FETCH_ALL_CLASSES_SUCCESS");

export const getInfo = createAction("STUDENTS/GET_INFO_CLASSES");
export const getInfoSuccess = createAction("STUDENTS/ GET_INFO_CLASSES_SUCCESS");

export const createNewClasses = createAction("STUDENTS/CREATE_NEW_CLASSES");
export const createNewClassesSuccess = createAction("STUDENTS/CREATE_NEW_CLASSES_SUCCESS");

export const editClasses = createAction("STUDENTS/EDIT_CLASSES");
export const editClassesSuccess = createAction("STUDENTS/EDIT_CLASSES_SUCCESS");

export const updateTask = createAction("STUDENTS/UPDATE_TASK_CLASSES");
export const updateTaskSuccess = createAction("STUDENTS/UPDATE_TASK_CLASSES_SUCCESS");

export const deleteClasses = createAction("STUDENTS/DELETE_CLASSES");
export const deleteClassesSuccess = createAction("STUDENTS/DELETE_CLASSES_SUCCESS");

export const addStudentInclass = createAction("STUDENTS/ADD_STUDENT_IN_CLASS");
export const addStudentInclassSuccess = createAction("STUDENTS/ADD_STUDENT_IN_CLASS_SUCCESS");

export const removeStudentInclass = createAction("STUDENTS/REMOVE_STUDENT_IN_CLASS");
export const removeStudentInclassSuccess = createAction("STUDENTS/REMOVE_STUDENT_IN_CLASS_SUCCESS");

export const handleSearch = createAction("STUDENTS/HANDLE_SEARCH");
export const changeTabs = createAction("STUDENTS/CHANGE_TABS");

export const joinClass = createAction("STUDENTS/JOIN_CLASS");
export const joinClassSuccess = createAction("STUDENTS/JOIN_CLASS_SUCCESS");

export const unJoinClass = createAction("STUDENTS/UN_JOIN_CLASS");
export const unJoinClassSuccess = createAction("STUDENTS/UN_JOIN_CLASS_SUCCESS");

export const fetchAllClassesPendding = createAction("STUDENTS/Classes_PEDDING");
export const fetchAllClassesPenddingSuccess = createAction("STUDENTS/Classes_PEDDING_SUCCESS");

export const handleClassesPendding = createAction("STUDENTS/HANDLE_CLASSES_PEDDING");
export const handleClassesPenddingSuccess = createAction("STUDENTS/HANDLE_CLASSES_PEDDING_SUCCESS");

export const getSpeaking = createAction("STUDENTS/GET_SPEAKING");
export const getSpeakingSuccess = createAction("STUDENTS/GET_SPEAKING_SUCCESS");

export const clear = createAction("STUDENTS/CLEAR");

export const clearDataModal = createAction("STUDENTS/clearDataModal");

export const confirmClass = createAction("STUDENTS/CONFIRM_CLASS")
export const confirmClassSuccess = createAction("STUDENTS/CONFIRM_CLASS_SUCCESS")

export const fetchTopic = createAction("STUDENTS/FETCH_ALL_TOPIC");
export const fetchTopicSuccess = createAction("STUDENTS/FETCH_ALL_TOPIC_SUCCESS");

export const fetchAllSpeakingConfirm = createAction("STUDENTS/FETCH_ALL_SPEAKING_CONFIRM");
export const fetchAllSpeakingConfirmSuccess = createAction("STUDENTS/FETCH_ALL_SPEAKING_CONFIRM_SUCCESS");

export const uploadClip = createAction("STUDENTS/UPLOAD_CLIP");
export const uploadClipSuccess = createAction("STUDENTS/UPLOAD_CLIP_SUCCESS");

export const checkValidate = createAction("STUDENTS/CHECK_VALIDATE");
export const checkValidateSuccess = createAction("STUDENTS/CHECK_VALIDATE_SUCCESS");

export const resendEmail = createAction("STUDENTS/PRESENT_EMAIL");
export const resendEmailSuccess = createAction("STUDENTS/PRESENT_EMAIL_SUCCESS");