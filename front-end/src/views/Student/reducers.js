/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";
import moment from "moment";
import { orderBy, find, get } from "lodash"
export const name = "Student";

const initialState = freeze({
  data: {
    name: " ",
    rejectReason: " ",
  },
  dataTopic: [],
  modalAvatar: false,
  tabs: "table",
  message: "",
  classes: [],
  ClassesPendding: [],
  ClassesInClass: [],
  dataModal: [],
  isOpen: false,
  isLoading: false,
  listProcessing: [],
  lanes: [],
  speakingConfirm: [],
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      usePagination: false,
      isExactly: true
    },
  },
});

export default handleActions(
  {
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.fetchAllClasses]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        data: {
          ...state.data,
          [name]: value,
        },
      });
    },
    [actions.fetchAllClassesSuccess]: (state, action) => {
      let classes = orderBy(action.payload.data, function(o) { return new moment(o.happenDate).format('YYYYMMDD'); }, ['asc']);
      const weeks = [
        { title: "monday", label: "Monday", value: 1 },
        { title: "tuesday", label: "Tuesday", value: 2 },
        { title: "wednesday", label: "Wednesday", value: 3 },
        { title: "thursday", label: "Thursday", value: 4 },
        { title: "friday", label: "Friday", value: 5 },
        { title: "saturday", label: "Saturday", value: 6 },
        { title: "sunday", label: "Sunday", value: 7 },
      ];
      let myTemp = moment().startOf("week");
      let monday = [];
      let tuesday = [];
      let wednesday = [];
      let thursday = [];
      let friday = [];
      let saturday = [];
      let sunday = [];
      weeks.map((myWeek) => {
        let startOfDay = moment(myTemp).add(myWeek.value, "day").format("YYYY-MM-DD");
        switch (myWeek.value) {
          case 1:
            monday = classes.filter(
              (e) => moment(e.happenDate).format("YYYY-MM-DD") === startOfDay
            );
            break;
          case 2:
            tuesday = classes.filter(
              (e) => moment(e.happenDate).format("YYYY-MM-DD") === startOfDay
            );
            break;
          case 3:
            wednesday = classes.filter(
              (e) => moment(e.happenDate).format("YYYY-MM-DD") === startOfDay
            );
            break;
          case 4:
            thursday = classes.filter(
              (e) => moment(e.happenDate).format("YYYY-MM-DD") === startOfDay
            );
            break;
          case 5:
            friday = classes.filter(
              (e) => moment(e.happenDate).format("YYYY-MM-DD") === startOfDay
            );
            break;
          case 6:
            saturday = classes.filter(
              (e) => moment(e.happenDate).format("YYYY-MM-DD") === startOfDay
            );
            break;
          default:
            sunday = classes.filter(
              (e) => moment(e.happenDate).format("YYYY-MM-DD") === startOfDay
            );
            break;
        }
      });
      return freeze({
        ...state,
        classes,
        lanes: [
          {
            cards: monday.map((o) => ({
              ...o,
              title: o.topic,
              description: o.description,
              label: moment(o.happenDate).format("LT"),
            })),
            title: "monday",
          },
          {
            cards: tuesday.map((o) => ({
              ...o,
              title: o.topic,
              description: o.description,
              label: moment(o.happenDate).format("LT"),
            })),
            title: "tuesday",
          },
          {
            cards: wednesday.map((o) => ({
              ...o,
              title: o.topic,
              description: o.description,
              label: moment(o.happenDate).format("LT"),
            })),
            title: "wednesday",
          },
          {
            cards: thursday.map((o) => ({
              ...o,
              title: o.topic,
              description: o.description,
              label: moment(o.happenDate).format("LT"),
            })),
            title: "thurday",
          },
          {
            cards: friday.map((o) => ({
              ...o,
              title: o.topic,
              description: o.description,
              label: moment(o.happenDate).format("LT"),
            })),
            title: "friday",
          },
          {
            cards: saturday.map((o) => ({
              ...o,
              title: o.topic,
              description: o.description,
              label: moment(o.happenDate).format("LT"),
            })),
            title: "saturday",
          },
          {
            cards: sunday.map((o) => ({
              ...o,
              title: o.topic,
              description: o.description,
              label: moment(o.happenDate).format("LT"),
            })),
            title: "sunday",
          },
        ],
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
        dataModal: action.payload.data,
      });
    },
    [actions.createNewClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        classes: [action.payload.data, ...state.Classes],
        data: [],
        isOpen: false,
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isOpen: true,
        isEdit: true,
        data: action.payload.data,
      });
    },
    [actions.editClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen: false,
        data: initialState.data,
        classes: [
          ...state.Classes.map((item) => {
            if (item._id === action.payload.data._id) {
              return action.payload.data;
            }
            return item;
          }),
        ],
      });
    },
    [actions.deleteClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen: false,
        data: initialState.data,
        classes: [...state.Classes.filter((o) => o._id !== action.payload.data)],
      });
    },
    [actions.addStudentInclassSuccess]: (state, action) => {
      return freeze({
        ...state,
        ClassesInClass: [...state.ClassesInClass, action.payload.data._id],
      });
    },
    [actions.fetchAllClassesPenddingSuccess]: (state, action) => {
      return freeze({
        ...state,
        ClassesPendding: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.handleClassesPenddingSuccess]: (state, action) => {
      return freeze({
        ...state,
        listProcessing: [...state.listProcessing, action.payload.data.interviewId],
        data: [],
      });
    },
    [actions.getSpeaking]: (state, action) => {
      return freeze({
        ...state,
        isOpen: true,
        dataModal: find(state.classes, o => action.payload.data),
      });
    },
    [actions.fetchAllSpeakingConfirmSuccess]: (state, action) => {
      return freeze({
        ...state,
        speakingConfirm: action.payload.data,
      });
    },
    [actions.fetchTopicSuccess]: (state, action) => {
      return freeze({
        ...state,
        dataTopic: action.payload.data,
      });
    },
    [actions.uploadClipSuccess]: (state, action) => {
      return freeze({
        ...state,
        speakingConfirm: [...state.speakingConfirm.map(o => {
          if(o._id === get(action, "payload.data.speakingId._id", "")) return {
            ...o, 
            myVideo: action.payload.data
          }
          return o
        })],
      });
    },
    [actions.clear]: (state, action) => {
      return freeze({
        ...state,
        data: {},
      });
    },
    [actions.clearDataModal]: (state, action) => {
      const {name} = action.payload;
      return freeze({
        ...state,
        isOpen: false,
        [name]: [],
      });
    },
    [actions.checkValidateSuccess]: (state, action) => {
      return freeze({
        ...state,
        userInfo: action.payload.data
      });
    },
  },
  initialState
);
