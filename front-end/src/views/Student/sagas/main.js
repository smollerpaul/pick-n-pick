import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/classesStudent";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllClasses(action) {
  try {
    // if (get('role') === "leader") {
    //   res = yield call(API.fetchAllClassesLeader, action.payload.data);
    // } else {
    // }
    let res = yield call(API.fetchAllClasses, action.payload.data);
    yield put(actions.fetchAllClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload.data);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewClasses(action) {
  try {
    const res = yield call(API.createNewClasses, action.payload.data);
    yield put(actions.createNewClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditClasses(action) {
  try {
    const res = yield call(API.editClasses, action.payload.data);
    yield put(actions.editClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlejoinClass(action) {
  try {
    const res = yield call(API.joinClass, action.payload.data);
    yield put(actions.joinClassSuccess(res));
    yield put(actions.fetchAllClasses({}));
    NotificationManager.success("Register Class Success");
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleunJoinClass(action) {
  try {
    const res = yield call(API.unJoinClass, action.payload.data);
    yield put(actions.unJoinClassSuccess(res));
    yield put(actions.fetchAllClasses({}));
    NotificationManager.success("DELETE Class Success");
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteClasses(action) {
  try {
    const res = yield call(API.deleteClasses, action.payload.data);
    yield put(actions.deleteClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleconfirmClass(action) {
  try {
    const res = yield call(API.confirmClass, action.payload.data);
    yield put(actions.confirmClassSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleupdateTask(action) {
  try {
    const { data } = action.payload;
    for (let item of data) yield put(actions.editClasses({ data: item }));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchTopic(action) {
  try {
    const res = yield call(API.fetchTopic, action.payload.data);
    yield put(actions.fetchTopicSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllSpeakingConfirm(action) {
  try {
    const res = yield call(API.fetchAllSpeakingConfirm, action.payload.data);
    yield put(actions.fetchAllSpeakingConfirmSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleuploadClip(action) {
  try {
    const res = yield call(API.uploadClip, action.payload.data);
    yield put(actions.uploadClipSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecheckValidate(action) {
  try {
    const res = yield call(API.checkValidate,action.payload.data);
    window.location.reload(true)
    // yield put(actions.checkValidateSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleresendEmail(action) {
  try {
    const res = yield call(API.resendEmail);
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function* fetchAllClasses() {
  yield takeAction(actions.fetchAllClasses, handleFetchAllClasses);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewClasses() {
  yield takeAction(actions.createNewClasses, handlecreateNewClasses);
}

export function* editClasses() {
  yield takeEvery(actions.editClasses, handleEditClasses);
}

export function* deleteClasses() {
  yield takeAction(actions.deleteClasses, handleDeleteClasses);
}

export function* joinClass() {
  yield takeAction(actions.joinClass, handlejoinClass);
}

export function* unJoinClass() {
  yield takeAction(actions.unJoinClass, handleunJoinClass);
}

export function* updateTask() {
  yield takeAction(actions.updateTask, handleupdateTask);
}

export function* confirmClass() {
  yield takeAction(actions.confirmClass, handleconfirmClass);
}

export function* fetchTopic() {
  yield takeAction(actions.fetchTopic, handlefetchTopic);
}

export function* fetchAllSpeakingConfirm() {
  yield takeAction(actions.fetchAllSpeakingConfirm, handlefetchAllSpeakingConfirm);
}

export function* uploadClip() {
  yield takeAction(actions.uploadClip, handleuploadClip);
}

export function* checkValidate() {
  yield takeAction(actions.checkValidate, handlecheckValidate);
}

export function* resendEmail() {
  yield takeAction(actions.resendEmail, handleresendEmail);
}

export default [
  confirmClass,
  fetchAllClasses,
  createNewClasses,
  editClasses,
  deleteClasses,
  getInfo,
  updateTask,
  joinClass,
  unJoinClass,
  fetchTopic,
  fetchAllSpeakingConfirm,
  uploadClip,
  checkValidate,
  resendEmail,
];
