import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import University from "./University";
import { isEmpty } from "lodash";

class Container extends PureComponent {
  componentDidMount() {
    const { university, countries, pagination, actions } = this.props;
    if (isEmpty(university)) {
      actions.fetchAllUniversity({ data: pagination });
    }
    if (isEmpty(countries)) {
      actions.fetchAllCountries({
        data: {
          ...pagination,
          search: {
            usePagination: false,
            isExactly: true,
          },
        },
      });
    }
  }

  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <div className="loading"></div>}
        <University {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name],
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action,
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
