import React, { Fragment } from "react";
import { Button, Modal, ModalBody, ModalHeader, CardImg } from "reactstrap";
import { CardSubtitle, CardText } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import ReactTable from "react-table";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import { isEmpty, get, find } from "lodash";
function Leaders(props) {
  const { isModalStudents, actions, students, pagination, universitys, universityId } = props;
  const { messages } = props.intl;
  const dataTableColumns = [
    {
      Header: "",
      accessor: "avatar",
      className: "justify-center",
      width: 80,
      Cell: (props) => (
        <a to="route" rel="noopener noreferrer" target="_blank" href={`${props.original.facebook}`}>
          <CardImg
            top
            src={
              isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
            }
            alt="Avatar"
          />
        </a>
      ),
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => (
        <div className="min-width-zero">
          <CardSubtitle className="truncate mb-0">{get(props, "original.name")}</CardSubtitle>
          <CardText className="text-muted text-small mb-0">
            {get(props, "original.faculty")}
          </CardText>
        </div>
      ),
    },
    {
      Header: messages["user.contactInfo"],
      accessor: "faculty",
      className: "justify-center",
      Cell: (props) => (
        <div className="min-width-zero">
          <CardSubtitle className="truncate mb-0">{get(props, "original.phone")}</CardSubtitle>
          <CardText className="text-muted text-small mb-0">{get(props, "original.email")}</CardText>
        </div>
      ),
    },
    {
      Header: messages["university.reasonToJoin"],
      accessor: "reasonToJoin",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize">{props.value}</p>,
    },
    {
      Header: messages["university.reasonToLeader"],
      accessor: "reasonToLeader",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-muted">{props.value ? props.value : 0}</p>
        </div>
      ),
    },
    {
      Header: messages["university.assignLeader"],
      accessor: "_id",
      className: "justify-center",
      Cell: ({ value }) => {
        const isUniLeader = find(universitys, o => o._id === universityId);
        if (isUniLeader.uniLeader) {
          return value === isUniLeader.uniLeader._id ? (
            <Button
              color="warning"
              onClick={() =>
                actions.assignLeader({
                  data: {
                    studentId: value,
                    universityId: props.universityId,
                    role: "student",
                  },
                  pagination,
                })
              }
            >
              <IntlMessages id="university.removeUnileader" />
            </Button>
          ) : null
        } else {
          return (
            <Button
              color="primary"
              onClick={() =>
                actions.assignLeader({
                  data: {
                    studentId: value,
                    universityId: props.universityId,
                    role: "uniLeader",
                  },
                  pagination,
                })
              }
            >
              <IntlMessages id="university.assignLeader" />
            </Button>
          )
        }
      },
    },
  ];

  return (
    <Fragment>
      <Modal isOpen={isModalStudents} size="xl" toggle={() => actions.handleToggleModalStudent()}>
        <ModalHeader toggle={() => actions.handleToggleModalStudent()}>
          <IntlMessages id="university.assignLeader" />
        </ModalHeader>
        <ModalBody>
          <ReactTable
            data={students}
            columns={dataTableColumns}
            TbodyComponent={CustomTbodyComponent}
            defaultPageSize={10}
            sortable={false}
            filterable={false}
            showPageJump={true}
            PaginationComponent={DataTablePagination}
            showPageSizeOptions={true}
            className="-striped -highlight h-full overflow-hidden"
          />
        </ModalBody>
      </Modal>
    </Fragment>
  );
}

export default Leaders;
