import React, { Fragment, useState } from "react";
import { Row, FormGroup, Input, FormFeedback } from "reactstrap";
import { Button, Modal, ModalBody, ModalHeader, ModalFooter, Label } from "reactstrap";
import { Colxx } from "theme/components/common/CustomBootstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import "react-datepicker/dist/react-datepicker.css";
import { isEmpty } from "lodash";

function ModalControl(props) {
  const { isEdit, isOpen } = props;
  const { actions, data } = props;
  const { messages } = props.intl;
  const { name, shortName } = data;
  const [modalDelete, setModalDelete] = useState(false);

  const onSave = () => {
    return isEdit ? actions.editUniversity({ data }) : actions.createNewUniversity({ data });
  };

  return (
    <Fragment>
      <Modal isOpen={isOpen} size="lg" toggle={() => actions.handleToggleModal()}>
        <ModalHeader toggle={() => actions.handleToggleModal()}>
          <IntlMessages id="university.info" />
        </ModalHeader>
        <ModalBody>
          <Row>
            <Colxx xxs="12" md="12" lg="6" xl="6">
              <Label className="form-group has-float-label mb-4">
                <FormGroup>
                  <Input
                    type="name"
                    name="name"
                    value={name}
                    onChange={(e) => actions.handleInputChange(e)}
                    invalid={isEmpty(name) ? true : false}
                  />
                  <FormFeedback>{messages[`university.name`]}</FormFeedback>
                </FormGroup>
                <IntlMessages id="university.name" />
              </Label>
            </Colxx>
            <Colxx xxs="12" md="12" lg="6" xl="6">
              <Label className="form-group has-float-label mb-4">
                <FormGroup>
                  <Input
                    name="shortName"
                    value={shortName}
                    onChange={(e) => actions.handleInputChange(e)}
                    invalid={isEmpty(shortName) ? true : false}
                  />
                  <FormFeedback>{messages[`university.shortName`]}</FormFeedback>
                </FormGroup>
                <IntlMessages id="university.shortName" />
              </Label>
            </Colxx>
            <Colxx xxs="12"></Colxx>
          </Row>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-end">
          <Button
            color="info"
            outline
            onClick={() => {
              actions.handleToggleModal();
              actions.clear({ name: "data" });
            }}
          >
            <IntlMessages id="button.cancel" />
          </Button>
          <Button color="primary" onClick={() => onSave()}>
            <IntlMessages id="button.save" />
          </Button>
          {/* {isEdit && (
            <Button color="danger" onClick={() => setModalDelete(!modalDelete)}>
              <IntlMessages id="button.delete" />
            </Button>
          )} */}
        </ModalFooter>
      </Modal>
      <Modal isOpen={modalDelete}>
        <ModalBody className="text-center">
          <Label className="h5" >
            <IntlMessages id="modalDelete.sure" />
          </Label>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-end" >
          <Button
            outline
            onClick={() => setModalDelete(!modalDelete)}
          >
            <IntlMessages id="button.cancel" />
          </Button>
          <Button
            color="danger"
            onClick={() => {
              actions.deleteUniversity({ data })
              setModalDelete(!modalDelete)
            }}
          >
            <IntlMessages id="button.delete" />
          </Button>
        </ModalFooter>
      </Modal>
    </Fragment>
  );
}

export default React.memo(injectIntl(ModalControl));
