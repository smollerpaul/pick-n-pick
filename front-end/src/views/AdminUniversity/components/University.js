import React, { Fragment } from "react";
import { CardSubtitle, CardText, Row, Button, Nav, NavItem, NavLink, TabContent, TabPane, } from "reactstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";

import { Card, CardBody } from "reactstrap";
import ReactTable from "react-table";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import { isEmpty, get } from "lodash";
import { Colxx } from "theme/components/common/CustomBootstrap";
import Breadcrumb from "theme/containers/navs/Breadcrumb";
import SearchButton from "theme/components/SearchButton";
import ModalUniversity from "./ModalUniversity";
import Leaders from "./Leaders";
import classnames from "classnames";
import ChangeUniverSity from "./ChangeUniversity"
function SRM(props) {
  const { actions, universitys, match, pagination, tabs } = props;
  const { messages } = props.intl;
  const { page, limit, totalPages } = props.pagination;
  const { all } = pagination.search;
  const dataListPeakers = [
    {
      Header: messages["university.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => (
        <p className="text-capitalize ">
          {props.value} ({props.original.shortName})
        </p>
      ),
    },
    {
      Header: messages["university.leaderInfo"],
      accessor: "gender",
      className: "justify-center",
      width: 400,
      Cell: (props) => {
        if (isEmpty(props.original.uniLeader)) {
          return (
            <Button
              color="primary"
              onClick={(e) => {
                e.stopPropagation();
                actions.fetchAllStudent({
                  data: {
                    search: {
                      universityId: props.original._id,
                      usePagination: false,
                      isExactly: true,
                    },
                  },
                });
                actions.handleToggleModalStudent(props.original._id);
              }}
            >
              <IntlMessages id="university.assignLeader" />
            </Button>
          );
        }
        return (
          <div className="min-width-zero" onClick={(e) => {
            e.stopPropagation();
            actions.fetchAllStudent({
              data: {
                search: {
                  universityId: props.original._id,
                  usePagination: false,
                  isExactly: true,
                },
              },
            });
            actions.handleToggleModalStudent(props.original._id);
          }}>
            <CardSubtitle className="truncate mb-0">
              {get(props, "original.uniLeader.name")}
            </CardSubtitle>
            <CardText className="text-muted text-small mb-0">
              {get(props, "original.uniLeader.email")}
              <br />
              {get(props, "original.uniLeader.phone")}
            </CardText>
          </div>
        );
      },
    },
    {
      Header: messages["university.totalClass"],
      accessor: "countClasses",
      className: "justify-center",
      width: 100,
      Cell: (props) => <p className="text-capitalize ">{get(props, "value", 0)}</p>,
    },
  ];
  const onPageChange = (pageIndex) => {
    actions.fetchAllUniversity({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllUniversity({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
  };

  return (
    <Fragment>
      <Row>
        <Colxx xxs="12">
          <div>
            {/* <Breadcrumb heading="menu.university" match={match} /> */}
            <SearchButton
              name="university.addNew"
              onChange={(e) => actions.handleSearch(e)}
              search={all}
              onSearch={() => {
                isEmpty(all)
                  ? actions.fetchAllUniversity({
                    data: { ...pagination, search: { usePagination: true, isExactly: false } },
                  })
                  : actions.fetchAllUniversity({
                    data: { ...pagination, search: { ...pagination.search, isExactly: false } },
                  });
              }}
              onToggle={() => actions.handleToggleModal()}
            />
          </div>
        </Colxx>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs">
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "university" })}
                onClick={() => {
                  actions.changeTabs("university");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="menu.university" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "change" })}
                onClick={() => {
                  actions.fetchAllStudentChangeUniversity({
                    data: {
                      ...pagination,
                      search: {
                        usePagination: true,
                        isExactly: false,
                        needAdminAction: true
                      }
                    }
                  })
                  actions.changeTabs("change");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="classes.changeUniversity" />
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={tabs} className="chat-app-tab-content mt-3">
            <TabPane tabId="university" className="chat-app-tab-pane">
              <Card className="mb-4">
                <CardBody>
                  <ReactTable
                    data={universitys}
                    columns={dataListPeakers}
                    TbodyComponent={CustomTbodyComponent}
                    defaultPageSize={limit}
                    sortable={false}
                    filterable={false}
                    showPageJump={true}
                    PaginationComponent={DataTablePagination}
                    showPageSizeOptions={true}
                    pageSize={universitys.length +1}
                    page={page - 1}
                    pages={totalPages}
                    manual
                    onPageChange={(pageIndex) => onPageChange(pageIndex)}
                    onPageSizeChange={(pageSize, pageIndex) => onPageSizeChange(pageSize, pageIndex)}
                    getTrProps={(state, rowInfo, column) => {
                      return {
                        className: "cursor-pointer",
                        onClick: () => actions.getInfo({ data: rowInfo.original }),
                      };
                    }}
                    className="-striped -highlight h-full overflow-hidden"
                  />
                </CardBody>
              </Card>
            </TabPane>
            <TabPane tabId="change" className="chat-app-tab-pane">
              <ChangeUniverSity {...props} />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
      <ModalUniversity {...props} />
      <Leaders {...props} />
    </Fragment>
  );
}

export default React.memo(injectIntl(SRM));
