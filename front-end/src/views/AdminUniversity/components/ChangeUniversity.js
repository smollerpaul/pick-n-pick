import React, { Fragment, useState } from "react";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";

import { Card, CardBody, CardImg, Label, Modal, ModalBody, ModalFooter, Button, CardTitle, Row, ModalHeader, FormGroup, FormFeedback } from "reactstrap";
import ReactTable from "react-table";
import Select from "react-select";
import CustomSelectInput from "theme/components/common/CustomSelectInput";

import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import moment from "moment"
import { find, get } from "lodash";
import { Colxx } from "theme/components/common/CustomBootstrap";
import Truncate from "react-truncate";


function SRM(props) {
    const { actions, pagination, studentChange, isInfo, dataChange, universityChangeId } = props;
    const { messages } = props.intl;
    const { page, limit, totalPages } = props.pagination;
    const [isChange, setIsChange] = useState(false)

    const universityIdOption = get(props, "universitys", []).map(o => ({
        label: `${o.shortName} - ${o.name}`,
        value: o._id
    }))
    const dataTableStudents = [
        {
            Header: "Avatar",
            accessor: "avatar",
            className: "justify-center",
            width: 100,
            Cell: (props) => {
                return <CardImg top src={props.value} alt={props.values} />;
            },
        },
        {
            Header: messages["user.name"],
            accessor: "name",
            className: "justify-center",
            Cell: (props) => (
                <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
            ),
        },
        {
            Header: messages["user.role"],
            accessor: "role",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex justify-content-center">
                    <p className="text-capitalize text-muted">{props.value}</p>
                </div>
            ),
        },
        {
            Header: messages["user.englishLevel"],
            accessor: "englishLevel",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex justify-content-center">
                    <p className="text-capitalize text-muted">
                        {messages[`user.englishLevel.${props.value}`]}
                    </p>
                </div>
            ),
        },
        {
            Header: "University",
            accessor: "universityId.name",
            className: "justify-center",
            width: 250,
            Cell: (props) => (
                <Truncate lines={1} ellipsis={<span>...</span>}>
                    {" "}
                    <p className="pt-1">{props.value}</p>
                </Truncate>
            ),
        },
        {
            Header: "Reason Change University",
            accessor: "note",
            className: "justify-center",
            width: 250,
            Cell: (props) => (
                <Truncate lines={1} ellipsis={<span>...</span>}>
                    {" "}
                    <p className="pt-1">{props.value}</p>
                </Truncate>
            ),
        },
    ]
    const onPageChange = (pageIndex) => {
        actions.fetchAllStudentChangeUniversity({ data: { ...pagination, page: pageIndex + 1 } });
    };
    const onPageSizeChange = (pageSize, pageIndex) => {
        actions.fetchAllStudentChangeUniversity({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
    };

    return (
        <Fragment>
            <Card className="mb-4">
                <CardBody>
                    <ReactTable
                        data={studentChange}
                        columns={dataTableStudents}
                        TbodyComponent={CustomTbodyComponent}
                        defaultPageSize={limit}
                        sortable={false}
                        filterable={false}
                        showPageJump={true}
                        PaginationComponent={DataTablePagination}
                        showPageSizeOptions={true}
                        pageSize={studentChange.length +1}
                        page={page - 1}
                        pages={totalPages}
                        manual
                        onPageChange={(pageIndex) => onPageChange(pageIndex)}
                        onPageSizeChange={(pageSize, pageIndex) => onPageSizeChange(pageSize, pageIndex)}
                        getTrProps={(state, rowInfo, column) => {
                            return {
                                className: "cursor-pointer",
                                onClick: () => actions.handleToggleModalChangeUniversity({ data: rowInfo.original }),
                            };
                        }}
                        className="-striped -highlight h-full overflow-hidden"
                    />
                </CardBody>
            </Card>
            <Modal isOpen={isInfo} size="md">
                <ModalBody>
                    <div className="text-center" >
                        <CardImg top src={get(dataChange, "avatar", "")} />
                    </div>
                    <div>
                        <Label className="h2 d-block my-2 px-2 " style={{ backgroundColor: "#BF9553", color: "white" }}>
                            <IntlMessages id="user.info" />
                        </Label>
                    </div>
                    <Row>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.name" /> :
                            <span className="ml-1">{get(dataChange, "name", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.gender" /> :
                            <span className="ml-1">{get(dataChange, "gender", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.email" /> :
                            <span className="ml-1">{get(dataChange, "email", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.birthDay" /> :
                            <span className="ml-1">{moment(get(dataChange, "birthDate", "")).format("DD/MM/YYYY")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.countries" /> :
                            <span className="ml-1">{get(dataChange, "countryId.name", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.universityId" /> :
                            <span className="ml-1">{get(dataChange, "universityId.name", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.introduceMySelf" /> :
                            <span className="ml-1">{get(dataChange, "facebook", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <span>Note</span> :
                            <span className="ml-1">{get(dataChange, "note", "")}</span>
                            </CardTitle>
                        </Colxx>
                    </Row>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-around">
                    <Button
                        color="info"
                        onClick={() => actions.handleToggleModalChangeUniversity({ data: {} })}
                    >
                        <IntlMessages id="button.cancel" />
                    </Button>{" "}
                    <Button
                        color="primary"
                        onClick={() => setIsChange(!isChange)
                        }
                    >
                        <IntlMessages id="button.accept" />
                    </Button>{" "}
                </ModalFooter>
            </Modal>
            <Modal isOpen={isChange} size="lg" toggle={() => actions.handleToggleModal()}>
                <ModalHeader toggle={() => actions.handleToggleModal()}>
                    <IntlMessages id="university.info" />
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Select
                                        isDisabled={true}
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        components={{ Input: CustomSelectInput }}
                                        options={universityIdOption}
                                        name="universityId"
                                        value={find(universityIdOption, (o) => o.value === get(dataChange, "universityId._id", ""))}
                                    />
                                    <FormFeedback>Please input {messages[`user.universityId`]}</FormFeedback>
                                </FormGroup>
                                <span><IntlMessages id="user.universityId" /> old</span>
                            </Label>
                        </Colxx>
                        <Colxx xxs="12" md="12" lg="6" xl="6">
                            <Label className="form-group has-float-label mb-4">
                                <FormGroup>
                                    <Select
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        components={{ Input: CustomSelectInput }}
                                        options={universityIdOption}
                                        name="universityId"
                                        value={find(universityIdOption, (o) => o.value === universityChangeId)}
                                        onChange={(e) => {
                                            actions.handleDataUniversity({
                                                target: { name: "universityId", value: e.value },
                                            });
                                        }}
                                    />
                                    <FormFeedback>Please input {messages[`user.universityId`]}</FormFeedback>
                                </FormGroup>
                                <span><IntlMessages id="user.universityId" /> new</span>
                            </Label>
                        </Colxx>
                        <Colxx xxs="12"></Colxx>
                    </Row>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-end">
                    <Button
                        color="info"
                        outline
                        onClick={() => setIsChange(!isChange)}
                    >
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button color="primary"
                        onClick={() => {
                            actions.changeUniversity({
                                data: {
                                    ...dataChange,
                                    universityId: universityChangeId,
                                }
                            })
                            setIsChange(!isChange)
                        }}
                    >
                        <IntlMessages id="button.save" />
                    </Button>
                </ModalFooter>
            </Modal>
        </Fragment>
    );
}

export default React.memo(injectIntl(SRM));
