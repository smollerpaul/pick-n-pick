/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";

export const name = "University";

const initialState = freeze({
  data: {
    name: " ",
    shortName: " ",
  },
  dataStudents: {
    topic: " ",
    description: " ",
    speakerId: " ",
  },
  dataChange: {},
  countries: [],
  studentChange: [],
  modalAvatar: false,
  isInfo: false,
  tabs: "university",
  message: "",
  universityChangeId: "",
  isOpen: false,
  isModalStudents: false,
  isLoading: false,
  universitys: [],
  students: [],
  universityId: "",
  listProcessing: [],
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      all: "",
      usePagination: true,
      isExactly: false,
    },
  },
});

export default handleActions(
  {
    [actions.assignLeaderSuccess]: (state, action) => {
      return freeze({
        ...state,
        isModalStudents: false,
      })
    },
    [actions.handleToggleModalStudent]: (state, action) => {
      return freeze({
        ...state,
        isModalStudents: !state.isModalStudents,
        universityId: action.payload
      })
    },
    [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
      });
    },
    [actions.handleToggleModalChangeUniversity]: (state, action) => {
      const {data} = action.payload;
      if(data){
        return freeze({
          ...state,
          isInfo: !state.isInfo,
          dataChange: action.payload.data
        });
      } else {
        return freeze({
          ...state,
          isInfo: !state.isInfo,
        });
      }
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        data: {
          ...state.data,
          [name]: value,
        },
      });
    },
    [actions.handleDataUniversity]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        universityChangeId: value,
      });
    },
    [actions.fetchAllUniversity]: (state, action) => {
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
        isLoading: false,
      });
    },
    [actions.fetchAllUniversitySuccess]: (state, action) => {
      return freeze({
        ...state,
        universitys: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.createNewUniversitySuccess]: (state, action) => {
      return freeze({
        ...state,
        universitys: [action.payload.data, ...state.universitys],
        data: [],
        isOpen: false,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isOpen: true,
        isEdit: true,
        data: action.payload.data,
      });
    },
    [actions.editUniversitySuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen: false,
        data: initialState.data,
        universitys: [
          ...state.universitys.map((item) => {
            if (item._id === action.payload.data._id) {
              return action.payload.data;
            }
            return item;
          }),
        ],
      });
    },
    [actions.deleteUniversitySuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen: false,
        data: initialState.data,
        universitys: [...state.universitys.filter((o) => o._id !== action.payload.data)],
      });
    },
    ///Student
    [actions.fetchAllStudentSuccess]: (state, action) => {
      return freeze({
        ...state,
        students: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.getInfoStudentSuccess]: (state, action) => {
      return freeze({
        ...state,
        isModalStudents: true,
        isEdit: true,
        dataStudents: action.payload.data,
      });
    },
    [actions.handleDataStudentChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        dataStudents: {
          ...state.dataStudent,
          [name]: value,
        },
      });
    },
    [actions.createNewStudentSuccess]: (state, action) => {
      return freeze({
        ...state,
        dataStudents: [],
        students: [...state.students, action.payload.data],
        isModalStudents: false,
      });
    },
    [actions.editStudentSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isModalStudents: false,
        data: initialState.data,
        students: [
          ...state.students.map((item) => {
            if (item._id === action.payload.data._id) {
              return action.payload.data;
            }
            return item;
          }),
        ],
      });
    },
    [actions.deleteStudentSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isModalStudents: false,
        data: initialState.data,
        students: [...state.students.filter((o) => o._id !== action.payload.data)],
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.fetchAllCountriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        countries: action.payload.data,
      });
    },
    [actions.fetchAllStudentChangeUniversity]: (state, action) => {
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
        isLoading: false,
      });
    },
    [actions.fetchAllStudentChangeUniversitySuccess]: (state, action) => {
      return freeze({
        ...state,
        studentChange: action.payload.data,
      });
    },
    [actions.changeUniversitySuccess]: (state, action) => {
      return freeze({
        ...state,
        studentChange: state.studentChange.filter(o => o._id !== action.payload.data._id),
      });
    },
    [actions.clear]: (state, action) => {
      const { name } = action.payload;
      return freeze({
        ...state,
        isEdit: false,
        [name]: initialState[name],
      });
    },
  },
  initialState
);
