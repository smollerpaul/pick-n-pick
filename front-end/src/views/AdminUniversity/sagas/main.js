import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/adminStudent";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllUniversity(action) {
  try {
    const res = yield call(API.fetchAllUniversity, action.payload.data);
    yield put(actions.fetchAllUniversitySuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload.data);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewUniversity(action) {
  try {
    const res = yield call(API.createNewUniversity, action.payload.data);
    yield put(actions.createNewUniversitySuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditUniversity(action) {
  try {
    const res = yield call(API.editUniversity, action.payload.data);
    yield put(actions.editUniversitySuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteUniversity(action) {
  try {
    const res = yield call(API.deleteUniversity, action.payload.data);
    yield put(actions.deleteUniversitySuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllStudent(action) {
  try {
    const res = yield call(API.fetchAllStudent, action.payload.data);
    yield put(actions.fetchAllStudentSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlegetInfoStudent(action) {
  try {
    const res = yield call(API.getInfoStudent, action.payload.data);
    yield put(actions.getInfoStudentSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewStudent(action) {
  try {
    const res = yield call(API.createNewStudent, action.payload.data);
    yield put(actions.createNewStudentSuccess(res));
    NotificationManager.success("Succes");
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditStudent(action) {
  try {
    const res = yield call(API.editStudent, action.payload.data);
    yield put(actions.editStudentSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteStudent(action) {
  try {
    const res = yield call(API.deleteStudent, action.payload.data);
    yield put(actions.deleteStudentSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleupdateTask(action) {
  try {
    const { data } = action.payload;
    for (let item of data) yield put(actions.editUniversity({ data: item }));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllCountries(action) {
  try {
    const res = yield call(API.fetchAllCountries, action.payload.data);
    yield put(actions.fetchAllCountriesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleassignLeader(action) {
  try {
    const res = yield call(API.assignLeader, action.payload.data);
    yield put(actions.assignLeaderSuccess(res));
    yield put(actions.fetchAllUniversity({ data: action.payload.pagination }));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllStudentChangeUniversity(action) {
  try {
    const res = yield call(API.fetchAllStudentChangeUniversity, action.payload.data);
    yield put(actions.fetchAllStudentChangeUniversitySuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlechangeUniversity(action) {
  try {
    const res = yield call(API.changeUniversity, action.payload.data);
    yield put(actions.changeUniversitySuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///University
export function* fetchAllUniversity() {
  yield takeAction(actions.fetchAllUniversity, handleFetchAllUniversity);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewUniversity() {
  yield takeAction(actions.createNewUniversity, handlecreateNewUniversity);
}

export function* editUniversity() {
  yield takeEvery(actions.editUniversity, handleEditUniversity);
}

export function* deleteUniversity() {
  yield takeAction(actions.deleteUniversity, handleDeleteUniversity);
}

//// Student
export function* fetchAllStudent() {
  yield takeAction(actions.fetchAllStudent, handlefetchAllStudent);
}

export function* getInfoStudent() {
  yield takeAction(actions.getInfoStudent, handlegetInfoStudent);
}

export function* createNewStudent() {
  yield takeAction(actions.createNewStudent, handlecreateNewStudent);
}

export function* editStudent() {
  yield takeEvery(actions.editStudent, handleEditStudent);
}

export function* deleteStudent() {
  yield takeAction(actions.deleteStudent, handleDeleteStudent);
}

export function* fetchAllCountries() {
  yield takeAction(actions.fetchAllCountries, handlefetchAllCountries);
}

export function* updateTask() {
  yield takeAction(actions.updateTask, handleupdateTask);
}

export function* assignLeader() {
  yield takeAction(actions.assignLeader, handleassignLeader);
}

export function* fetchAllStudentChangeUniversity() {
  yield takeAction(actions.fetchAllStudentChangeUniversity, handlefetchAllStudentChangeUniversity);
}

export function* changeUniversity() {
  yield takeAction(actions.changeUniversity, handlechangeUniversity);
}

export default [
  assignLeader,
  fetchAllUniversity,
  createNewUniversity,
  editUniversity,
  deleteUniversity,
  fetchAllStudent,
  getInfoStudent,
  createNewStudent,
  editStudent,
  deleteStudent,
  getInfo,
  updateTask,
  fetchAllCountries,
  fetchAllStudentChangeUniversity,
  changeUniversity
];
