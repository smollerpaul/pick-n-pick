/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

//University
export const handleInputChange = createAction("ADMIN_UNIVERSITY/HANDLE_INPUT_CHANGE");
export const handleToggleModal = createAction("ADMIN_UNIVERSITY/HANDLE_TOGGLE_MODAL");


export const fetchAllUniversity = createAction("ADMIN_UNIVERSITY/FETCH_ALL_UNIVERSITY");
export const fetchAllUniversitySuccess = createAction("ADMIN_UNIVERSITY/FETCH_ALL_UNIVERSITY_SUCCESS");

export const getInfo = createAction("ADMIN_UNIVERSITY/GET_INFO_UNIVERSITY");
export const getInfoSuccess = createAction("ADMIN_UNIVERSITY/ GET_INFO_UNIVERSITY_SUCCESS");

export const createNewUniversity = createAction("ADMIN_UNIVERSITY/CREATE_NEW_UNIVERSITY");
export const createNewUniversitySuccess = createAction("ADMIN_UNIVERSITY/CREATE_NEW_UNIVERSITY_SUCCESS");

export const editUniversity = createAction("ADMIN_UNIVERSITY/EDIT_UNIVERSITY");
export const editUniversitySuccess = createAction("ADMIN_UNIVERSITY/EDIT_University_SUCCESS");

export const deleteUniversity = createAction("ADMIN_UNIVERSITY/DELETE_UNIVERSITY");
export const deleteUniversitySuccess = createAction("ADMIN_UNIVERSITY/DELETE_SPEAKERS_SUCCESS");

// Student

export const handleDataStudentChange = createAction("ADMIN_UNIVERSITY/HANDLE_DATA_STUDENT_CHANGE");
export const handleToggleModalStudent = createAction("ADMIN_UNIVERSITY/HANDLE_TOGGLE_MODAL_STUDENT");

export const fetchAllStudent = createAction("ADMIN_UNIVERSITY/FETCH_ALL_STUDENT");
export const fetchAllStudentSuccess = createAction("ADMIN_UNIVERSITY/FETCH_ALL_STUDENT_SUCCESS");

export const createNewStudent = createAction("ADMIN_UNIVERSITY/CREATE_NEW_STUDENT");
export const createNewStudentSuccess = createAction("ADMIN_UNIVERSITY/CREATE_NEW_STUDENT_SUCCESS");

export const editStudent = createAction("ADMIN_UNIVERSITY/EDIT_STUDENT");
export const editStudentSuccess = createAction("ADMIN_UNIVERSITY/EDIT_STUDENT_SUCCESS");

export const deleteStudent = createAction("ADMIN_UNIVERSITY/DELETE_STUDENT");
export const deleteStudentSuccess = createAction("ADMIN_UNIVERSITY/DELETE_STUDENT_SUCCESS");

export const getInfoStudent = createAction("ADMIN_UNIVERSITY/GET_INFO_STUDENT");
export const getInfoStudentSuccess = createAction("ADMIN_UNIVERSITY/ GET_INFO_STUDENT_SUCCESS");

//other
export const updateTask = createAction("ADMIN_UNIVERSITY/UPDATE_TASK_SPEAKERS");
export const updateTaskSuccess = createAction("ADMIN_UNIVERSITY/UPDATE_TASK_SPEAKERS_SUCCESS");

export const addStudentInclass = createAction("ADMIN_UNIVERSITY/ADD_STUDENT_IN_CLASS");
export const addStudentInclassSuccess = createAction("ADMIN_UNIVERSITY/ADD_STUDENT_IN_CLASS_SUCCESS");

export const removeStudentInclass = createAction("ADMIN_UNIVERSITY/REMOVE_STUDENT_IN_CLASS");
export const removeStudentInclassSuccess = createAction("ADMIN_UNIVERSITY/REMOVE_STUDENT_IN_CLASS_SUCCESS");

export const fetchAllCountries = createAction("ADMIN_UNIVERSITY/FETCH_ALL_COUNTRY");
export const fetchAllCountriesSuccess = createAction("ADMIN_UNIVERSITY/FETCH_ALL_COUNTRY_SUCCESS");

export const handleSearch = createAction("ADMIN_UNIVERSITY/HANDLE_SEARCH");
export const changeTabs = createAction("ADMIN_UNIVERSITY/CHANGE_TABS");

export const clear = createAction("ADMIN_UNIVERSITY/CLEAR");
export const handleToggleModalChangeUniversity = createAction("ADMIN_UNIVERSITY/HANDLE_TOGGLE_MODAL_UNIVERSITY_CHANGE");

export const handleDataUniversity = createAction("ADMIN_UNIVERSITY/HANDLE_DATA_UNIVERSITY");

export const assignLeader = createAction("ADMIN_UNIVERSITY/ASSIGN_LEADER");
export const assignLeaderSuccess = createAction("ADMIN_UNIVERSITY/ASSIGN_LEADER_SUCCESS");

export const fetchAllStudentChangeUniversity = createAction("ADMIN_UNIVERSITY/FETCH_ALL_STUDENT_CHANGE_UNIVERSITY");
export const fetchAllStudentChangeUniversitySuccess = createAction("ADMIN_UNIVERSITY/FETCH_ALL_STUDENT_CHANGE_UNIVERSITY_SUCCESS");

export const changeUniversity = createAction("ADMIN_UNIVERSITY/CHANGE_UNIVERSITY");
export const changeUniversitySuccess = createAction("ADMIN_UNIVERSITY/CHANGE_UNIVERSITY_SUCCESS");