/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const checkLogin = createAction("ROOM_LIVER/CHECK_LOGIN");
export const checkLoginSuccess = createAction("ROOM_LIVER/CHECK_LOGIN_SUCCESS");
export const checkLoginFail = createAction("ROOM_LIVER/CHECK_LOGIN_FAIL");

export const saveToken = createAction("ROOM_LIVER/SAVE_TOKEN");

export const speakerRoom = createAction("ROOM_LIVER/SPEAKER_ROOM");
export const speakerRoomSuccess = createAction("ROOM_LIVSPEAKER_ROOM_SUCCESS");
export const speakerRoomFail = createAction("ROOM_LIVER/SPEAKER_ROOM_FAIL");

export const studentRoom = createAction("ROOM_LIVER/STUDENT_ROOM");
export const studentRoomSuccess = createAction("ROOM_LIVSTUDENT_ROOM_SUCCESS");
export const studentRoomFail = createAction("ROOM_LIVER/STUDENT_ROOM_FAIL");