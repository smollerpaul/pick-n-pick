import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";
import _ from "lodash";
import { get } from "services/localStoredService";
import * as API from "apis/room";

export function* handleCheckLogin(action) {
  try {
    let accessToken = get("accessToken");
    if (_.isEmpty(accessToken)) {
      yield put(actions.checkLoginFail());
      action.payload.history.push("/user/login");
    } else {
      const res = yield call(API.getInfo);
      yield put(actions.checkLoginSuccess(res.data));
    }
  } catch (err) {
    yield put(actions.checkLoginFail(err));
  }
}

export function* handlespeakerRoom(action) {
  try {
   const res = yield call(API.speakerRoom, action.payload.data)
   yield put(actions.speakerRoomSuccess(res))
   /// push link
   window.location.replace(res.data)
  } catch (err) {
    yield put(actions.speakerRoomFail(err))
  }
}

export function* handlestudentRoom(action) {
  try {
   const res = yield call(API.studentRoom, action.payload.data)
   yield put(actions.studentRoomSuccess(res))
   /// push link
   window.location.replace(res.data)
  } catch (err) {
    yield put(actions.studentRoomFail(err))
  }
}

export function* checkLogin() {
  yield takeAction(actions.checkLogin, handleCheckLogin);
}

export function* speakerRoom() {
  yield takeAction(actions.speakerRoom, handlespeakerRoom);
}

export function* studentRoom() {
  yield takeAction(actions.studentRoom, handlestudentRoom);
}

export default [checkLogin, speakerRoom, studentRoom];
