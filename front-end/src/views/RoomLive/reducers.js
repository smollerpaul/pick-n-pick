/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "RoomLive";

const initialState = freeze({
  notifications: [],
  token: "",
  userInfo: {},
  currentTitle: "",
  error: false
});

export default handleActions(
  {
    [actions.saveToken]: (state, action) => {
      return freeze({
        ...state,
        token: action.payload
      });
    },
    [actions.checkLoginSuccess]: (state, action) => {
      return freeze({
        ...state,
        userInfo: action.payload
      })
    },
    [actions.speakerRoomSuccess]: (state, action) => {
      return freeze({
        ...state,
        userInfo: action.payload
      })
    },
    [actions.speakerRoomFail]: (state, action) => {
      return freeze({
        ...state,
        error: true,
      })
    },
    [actions.studentRoomSuccess]: (state, action) => {
      return freeze({
        ...state,
        userInfo: action.payload
      })
    },
    [actions.studentRoomFail]: (state, action) => {
      return freeze({
        ...state,
        error: true,
      })
    },
  },
  initialState
);
