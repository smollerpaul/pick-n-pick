import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import { injectIntl } from "react-intl";

function WrapperContainer (props) {
    const {location} = props;
    const isSpeaker  = location.pathname.match(/speaker/gi)
    const isSpeaking  = location.pathname.match(/speaking/gi)
    return (
      <React.Fragment>
        <h1> haha</h1>
      </React.Fragment>
    );
  }
function mapStateToProps(state) {
  return {
    ...state[name],
    locale: state.settings.locale
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(injectIntl(WrapperContainer))
);
