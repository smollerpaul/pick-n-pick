import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import { injectIntl } from "react-intl";
import ERROR from "./error"
function WrapperContainer(props) {
  const { location, actions, error, history } = props;
  const { id } = props.match.params
  const isSpeaker = location.pathname.match(/speaker/g)
  const isSpeaking = location.pathname.match(/speaking/g)
  if (id) {
    if (isSpeaker) actions.speakerRoom({data: id, history})
    if (isSpeaking) {
      action.checkLogin()
      actions.studentRoom({data: id, history})
    }
  }
  if (error) return <ERROR />
  return (
    <React.Fragment>
    </React.Fragment>
  );
}
function mapStateToProps(state) {
  return {
    ...state[name],
    locale: state.settings.locale
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(injectIntl(WrapperContainer))
);
