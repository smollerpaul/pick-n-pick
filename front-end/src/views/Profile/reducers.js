/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
export const name = "Profile";

const initialState = freeze({
  isLoading: false,
  isFinshed: false,
  countries: [],
  cities: [],
  universities: [],
  data: {
    name: " ",
    email: " ",
    gender: " ",
    birthDate: " ",
    faculty: " ",
    facebook: " ",
    englishLevel: " ",
    city: " ",
    avatar: "",
    confirmPassword: "",
    knowUsFrom: " ",
  }
});

export default handleActions(
  {
    [actions.fetchAllCountriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        countries: action.payload
      });
    },
    [actions.updateUserSuccess]: (state, action) => {
      return freeze({
        ...state,
        isFinshed: !state.isFinshed,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        data: action.payload.data,
      });
    },
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    [actions.clearInput]: (state, action) => {
      return freeze({
        ...state,
        data: initialState.data
      });
    }
  },
  initialState
);
