/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("PROFILE/HANDLE_INPUT_CHANGE");

export const getInfo = createAction("PROFILE/GETINFO");
export const getInfoSuccess = createAction("PROFILE/GETINFO_SUCCESS");

export const updateUser = createAction("PROFILE/UPDATE");
export const updateUserSuccess = createAction("PROFILE/UPDATE_SUCCESS");

export const clearInput = createAction("PROFILE/CLEAR_INPUT");

export const fetchAllCountries = createAction("PROFILE/FETCH_ALL_COUNTRIES");
export const fetchAllCountriesSuccess = createAction("PROFILE/FETCH_ALL_COUNTRIES_SUCCESS");