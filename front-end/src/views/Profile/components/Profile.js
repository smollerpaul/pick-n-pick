import React, { useState } from "react";
import { Row, Card, CardTitle, CardBody, CardImg } from "reactstrap";
import { NavLink } from "react-router-dom";
import { Colxx } from "theme/components/common/CustomBootstrap";
import IntlMessages from "helpers/IntlMessages";
import { injectIntl } from "react-intl";
import UserInfo from "./UserInfo";
import AnimateGroup from "theme/components/AnimateGroup";
import SelectLanguages from "theme/components/common/SelectLanguages";
import { isEmpty, get } from "lodash";

function Register(props) {
  const [isOpen, setModal] = useState(false);
  return (
    <AnimateGroup enter={{ animation: "transition.slideRightBigIn" }}>
      <Row className="h-100">
        <Colxx xxs="12" md="10" lg="10" xl="10" className="mx-auto my-auto pt-5">
          <Card>
            <CardBody>
              <CardImg
                className="top-right-button-container"
                onClick={() => setModal(!isOpen)}
                src={
                  isEmpty(get(props, "data.avatar", ""))
                    ? "/assets/img/avatar.png"
                    : get(props, "data.avatar", "")
                }
                alt="Avatar"
              />
              <NavLink to={`/`} className="white">
                <span className="logo-single" style={{ marginBottom: "30px" }} />
              </NavLink>
              <div className="d-flex justify-ali">
                <CardTitle className="mr-2">
                  <IntlMessages id="user.updateInfo" />
                </CardTitle>
              </div>
            </CardBody>
            <CardBody>
              <UserInfo {...props} isOpen={isOpen} setModal={() => setModal(!isOpen)} />
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </AnimateGroup>
  );
}

export default injectIntl(Register);
