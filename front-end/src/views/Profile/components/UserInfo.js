import React, { Fragment } from "react";
import { Form, Label } from "reactstrap";
import { Input, Button, FormGroup, FormFeedback, Row } from "reactstrap";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import DatePicker from "react-datepicker";
import Select from "react-select";
import moment from "moment";


import CustomSelectInput from "theme/components/common/CustomSelectInput";
import "react-datepicker/dist/react-datepicker.css";
import { Colxx } from "theme/components/common/CustomBootstrap";
import { NotificationManager } from "theme/components/common/react-notifications";
import UploadImage from "theme/components/UploadImage";

import { isEmpty, find, get } from "lodash";

function Register(props) {
  const { name, gender, phone, birthDate, videoIntroduce } = props.data;
  const { faculty, facebook, englishLevel, reasonToJoin, reasonToLeader, knowUsFrom, city } = props.data;
  const { actions, isOpen, data, history } = props;
  const { messages } = props.intl;

  const genderOption = ["male", "female", "other"].map(o => ({
    label: messages[`user.gender.${o}`],
    value: o
  }))

  const englishLevelOption = [1, 2, 3, 4, 5].map(o => ({
    label: messages[`user.englishLevel.${o}`],
    value: o
  }))

  const knowUsFromOption = ["socialMedia", "article", "friend", "others"].map(o => ({
    label: messages[`user.knowUsFrom.${o}`],
    value: o
  }))

  const onUserRegister = () => {
    for (const item in data) {
      if (isEmpty(data[item].toString()) || data[item] === " ") return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.${item}`]}`);
    }
    return actions.updateUser({ data, history });
  }
  return (
    <Fragment>
      <Form>
      <Row>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="name"
                  name="name"
                  value={name}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(name) ? true : false}
                />
                <FormFeedback>{messages[`user.name`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.name" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={genderOption}
                  name="gender"
                  value={find(genderOption, (o) => o.value === gender)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "gender", value: e.value },
                    });
                  }}
                />
                <FormFeedback>{messages[`user.gender`]}</FormFeedback>
              </FormGroup>
              <span> <IntlMessages id="user.gender" />(*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="date"
                  min="1960-01-01T00:00"
                  name="birthDate"
                  value={moment(birthDate).format("YYYY-MM-DD")}
                  onChange={(e) => {
                    actions.handleInputChange(e)
                  }}
                  invalid={isEmpty(birthDate) ? true : false}
                />
                <FormFeedback>{messages[`user.birthDay`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.birthDay" />(MM/DD/YYYY)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="number"
                  name="phone"
                  value={phone}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(phone) ? true : false}
                />
                <FormFeedback>{messages[`user.phone`]}</FormFeedback>
              </FormGroup>
              <IntlMessages id="user.phone" />
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={englishLevelOption}
                  name="englishLevel"
                  value={find(englishLevelOption, (o) => o.value == englishLevel)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "englishLevel", value: e.value },
                    });
                  }}
                />
                <FormFeedback>{messages[`user.englishLevel`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.englishLevel" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="city"
                  name="city"
                  value={city}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(city) ? true : false}
                />
                <FormFeedback>{messages[`user.city`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.city" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={knowUsFromOption}
                  name="knowUsFrom"
                  value={find(knowUsFromOption, (o) => o.value === knowUsFrom)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "knowUsFrom", value: e.value },
                    });
                  }}
                />
                <FormFeedback>{messages[`user.knowUsFrom`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.knowUsFrom" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="text"
                  name="faculty"
                  value={faculty}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(faculty) ? true : false}
                />
                <FormFeedback>{messages[`user.faculty`]}</FormFeedback>
              </FormGroup>
              <IntlMessages id="user.faculty" />
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="facebook"
                  name="facebook"
                  value={facebook}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(facebook) ? true : false}
                />
                <FormFeedback>{messages[`user.facebook`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.facebook" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="text"
                  name="videoIntroduce"
                  value={videoIntroduce}
                  onChange={(e) => actions.handleInputChange(e)}
                />
                <FormFeedback>{messages[`user.videoIntroduce`]}</FormFeedback>
              </FormGroup>
              <IntlMessages id="user.videoIntroduce" />
            </Label>
          </Colxx>
          <Colxx xxs="12" md="12" lg="12" xl="12" >
            <Label className="form-group has-float-label my-4">
              <IntlMessages id="user.reasonToJoin" />
            </Label>
            <FormGroup>
              <Input
                type="textarea"
                rows={5}
                name="reasonToJoin"
                value={reasonToJoin}
                onChange={(e) => actions.handleInputChange(e)}
              // invalid={isEmpty(reasonToJoin) ? true : false}
              />
              <FormFeedback>{messages[`user.reasonToJoin`]}</FormFeedback>
            </FormGroup>
          </Colxx>
          <Colxx xxs="12" md="12" lg="12" xl="12" >
            <Label className="form-group has-float-label my-4">
              <IntlMessages id="user.reasonToLeader" />
            </Label>
            <FormGroup>
              <Input
                type="textarea"
                rows={5}
                placeholder="If you would like to improve your leadership, management skills, you can nominate yourself as a Team Leader. "
                name="reasonToLeader"
                value={reasonToLeader}
                onChange={(e) => actions.handleInputChange(e)}
              // invalid={isEmpty(reasonToLeader) ? true : false}
              />
              <FormFeedback>{messages[`user.reasonToLeader`]}</FormFeedback>
            </FormGroup>
          </Colxx>
        </Row>
        <hr />
        <div className="d-flex justify-content-center">
          <Button
            color="primary"
            className="btn-shadow login default"
            size="lg"
            onClick={() => onUserRegister()}
          >
            <IntlMessages id="button.save" />
          </Button>
        </div>
      </Form>
      <Modal
        isOpen={isOpen}
        size="lg"
        toggle={() => props.setModal()}
        centered
        style={{ boxShadow: "none" }}
      >
        <ModalHeader toggle={() => props.setModal()}>
          <IntlMessages id="brands.uploadAvatar" />
        </ModalHeader>
        <ModalBody>
          <UploadImage
            title="Upload Image"
            number={1}
            onRemove={file => actions.removeImage(file)}
            onUpload={file => {
              actions.handleInputChange({
                target: {
                  name: "avatar",
                  value: file
                }
              })
              props.setModal()
            }}
            onChange={() => props.setModal()}
          />
        </ModalBody>
      </Modal>
    </Fragment>
  );
}

export default React.memo(injectIntl(Register));