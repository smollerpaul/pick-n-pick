import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/profile";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllCountries() {
  try {
    let res = yield call(API.fetchAllCountries);
    yield put(actions.fetchAllCountriesSuccess(res.data));
  } catch (err) {
    NotificationManager.error("Error!", err);
  }
}
export function* handleupdateUser(action) {
  try {
    let res = yield call(API.updateUser, action.payload.data);
    yield put(actions.updateUserSuccess(res.data));
    NotificationManager.success("Update Info Success");

  } catch (err) {
    NotificationManager.error("Error!", err);
  }
}

export function* handlegetInfo() {
  try {
    let res = yield call(API.getInfo);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Error!", err);
  }
}

////////////////////////////////////////////////////////////

export function* fetchAllCountries() {
  yield takeAction(actions.fetchAllCountries, handleFetchAllCountries);
}

export function* updateUser() {
  yield takeAction(actions.updateUser, handleupdateUser);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handlegetInfo);
}
////////////////////////////////////////////////////////////

export default [fetchAllCountries, updateUser, getInfo];
