/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("REGISTER/HANDLE_INPUT_CHANGE");

export const createUser = createAction("REGISTER/CREATE_USER");
export const createUsersuccess = createAction("REGISTER/CREATE_USER_SUCCESS");
export const createUserFail = createAction("REGISTER/CREATE_USER_FAIL");

export const clearInput = createAction("REGISTER/CLEAR_INPUT");

export const fetchAllCountries = createAction("REGISTER/FETCH_ALL_COUNTRIES");
export const fetchAllCountriesSuccess = createAction("REGISTER/FETCH_ALL_COUNTRIES_SUCCESS");

export const fetchAllCities = createAction("REGISTER/FETCH_ALL_CITIES");
export const fetchAllCitiesSuccess = createAction("REGISTER/FETCH_ALL_CITIES_SUCCESS");

export const fetchAllUniversities = createAction("REGISTER/FETCH_ALL_UNIVERSITIES");
export const fetchAllUniversitiesSuccess = createAction("REGISTER/FETCH_ALL_UNIVERSITIES_SUCCESS");