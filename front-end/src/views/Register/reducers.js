/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import moment from "moment";
export const name = "RegisterUser";

const initialState = freeze({
  isLoading: false,
  isFinshed: false,
  countries: [],
  cities: [],
  universities: [],
  data: {
    name: " ",
    email: " ",
    gender: "male",
    birthDate:" ",
    isHighQualityStudentProgram: true,
    password:"",
    universityId: " ",
    facebook: " ",
    englishLevel: 3,
    countryId: " ",
    city: " ",
    avatar: "",
    confirmPassword: "",
    knowUsFrom: " ",
  }
});

export default handleActions(
  {
    [actions.fetchAllCountries]: (state, action) => {
      return freeze({
        ...state,
        isLoading: !state.isLoading
      });
    },
    [actions.fetchAllCountriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        countries: action.payload,
        data: {
          ...state.data,
          countryId:action.payload[0]._id,
        }
      });
    },
    [actions.createUsersuccess]: (state, action) => {
      return freeze({
        ...state,
        isFinshed: !state.isFinshed,
      });
    },
    [actions.fetchAllUniversitiesSuccess]: (state, action) => {
      return freeze({
        ...state,
        universities: action.payload,
        isLoading: false,
      });
    },
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    [actions.clearInput]: (state, action) => {
      return freeze({
        ...state,
        data: initialState.data
      });
    }
  },
  initialState
);
