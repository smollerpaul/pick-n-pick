import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/auth";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";
import { save } from "services/localStoredService";


//Create User's account
export function* handleCreateUser(action) {
  try {
    let res = yield call(API.createUser, action.payload.data);
    const { accessToken, refreshToken, userInfo } = res.data;
    save("refreshToken", refreshToken);
    save("accessToken", accessToken);
    save("userInfo", userInfo);
    yield put(actions.createUsersuccess(res));
    action.payload.history.push(`/`);
    NotificationManager.success(action.payload.messages[`user.thankyou`])
  } catch (err) {
    NotificationManager.error(err);
  }
}

export function* handleFetchAllCountries() {
  try {
    let res = yield call(API.fetchAllCountries);
    yield put(actions.fetchAllCountriesSuccess(res.data));
  } catch (err) {
    NotificationManager.error("Error!", err);
  }
}
export function* handleFetchAllUniversities() {
  try {
    let res = yield call(API.fetchAllUniversities);
    yield put(actions.fetchAllUniversitiesSuccess(res.data));
  } catch (err) {
    NotificationManager.error("Error!", err);
  }
}

////////////////////////////////////////////////////////////
export function* createUser() {
  yield takeAction(actions.createUser, handleCreateUser);
}

export function* fetchAllCountries() {
  yield takeAction(actions.fetchAllCountries, handleFetchAllCountries);
}

export function* fetchAllUniversities() {
  yield takeAction(actions.fetchAllUniversities, handleFetchAllUniversities);
}
////////////////////////////////////////////////////////////

export default [
  createUser, 
  // fetchAllCities,
  fetchAllCountries,
  fetchAllUniversities
];
