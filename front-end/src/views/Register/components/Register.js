import React, { useState } from "react";
import { Row, Card, CardTitle, CardBody, CardImg, CardFooter, Label } from "reactstrap";
import { NavLink } from "react-router-dom";
import { Colxx } from "theme/components/common/CustomBootstrap";
import IntlMessages from "helpers/IntlMessages";
import { injectIntl } from "react-intl";
import UserInfo from "./UserInfo";
import AnimateGroup from "theme/components/AnimateGroup";
import { isEmpty, get } from "lodash";

function Register(props) {
  const [isOpen, setModal] = useState(false)
  return (
    <AnimateGroup enter={{ animation: "transition.slideRightBigIn" }}>
      <Row className="h-100">
        <Colxx xxs="12" md="10" lg="10" xl="10" className="mx-auto my-auto pt-5">
          <Card >
            <CardBody>
              <Card className="top-right-button-container shadow-none" onClick={() => setModal(!isOpen)}>
                <CardImg
                  src={isEmpty(get(props, "data.avatar", "")) ? "/assets/img/avatar.png" : get(props, "data.avatar", "")}
                  alt="Avatar"
                />
                <CardFooter className="text-center">
                  <Label>
                    <IntlMessages id="brands.uploadAvatar" />
                  </Label>
                </CardFooter>
              </Card>
              <NavLink to={`/`} className="white">
                <span className="logo-single" style={{ marginBottom: "30px" }} />
              </NavLink>
              <div className="d-flex justify-ali">
                <CardTitle className="mr-2">
                  <IntlMessages id="user.register" />
                </CardTitle>
              </div>
            </CardBody>
            <CardBody>
              <UserInfo {...props} isOpen={isOpen} setModal={() => setModal(!isOpen)} />
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </AnimateGroup>
  );
}

export default injectIntl(Register);
