import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Register from "./Register";
import { isEmpty } from "lodash";

class Container extends PureComponent {
  componentDidMount() {
    const { countries, cities, universities, actions } = this.props
    if (isEmpty(countries)) {
      actions.fetchAllCountries();
    }
    // if (isEmpty(cities)) {
    //   actions.fetchAllCities();
    // }
    if (isEmpty(universities)) {
      actions.fetchAllUniversities();
    }
  }
  render() {
    const { isLoading } = this.props;
    if (isLoading) return <div className="loading"></div>;
    else return (
      <React.Fragment>
        <Register {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
