import React, { Fragment, useState } from "react";
import { Form, Label, CustomInput, ModalFooter } from "reactstrap";
import { Input, Button, FormGroup, FormFeedback, Row } from "reactstrap";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
// import DatePicker from "react-datepicker";
import Select from "react-select";
import moment from "moment";
import CustomSelectInput from "theme/components/common/CustomSelectInput";
import "react-datepicker/dist/react-datepicker.css";
import { Colxx } from "theme/components/common/CustomBootstrap";
import { NotificationManager } from "theme/components/common/react-notifications";
import UploadImage from "theme/components/UploadImage";

import DataCity from "./cities.json"
import { isEmpty, find, get } from "lodash";

function Register(props) {
  const { name, email, gender, phone, birthDate, password, confirmPassword, videoIntroduce,universityRequest  } = props.data;
  const { universityId, faculty, facebook, englishLevel, reasonToJoin, reasonToLeader, knowUsFrom, countryId, city } = props.data;
  const { actions, isOpen, data, history, universities } = props;
  const { messages } = props.intl;

  const cityOption = DataCity.data.map(o => ({
    label: o.name,
    value: o.name
  }))

  const genderOption = ["male", "female", "other"].map(o => ({
    label: messages[`user.gender.${o}`],
    value: o
  }))

  const englishLevelOption = [1, 2, 3, 4, 5].map(o => ({
    label: messages[`user.englishLevel.${o}`],
    value: o
  }))

  const universityIdOption = get(props, "universities", []).map(o => ({
    label: `${o.shortName} - ${o.name}`,
    value: o._id,
  }))

  const countriesOption = get(props, "countries", []).map(o => ({
    label: o.name,
    value: o._id
  }))

  const knowUsFromOption = ["socialMedia", "article", "friend", "others"].map(o => ({
    label: messages[`user.knowUsFrom.${o}`],
    value: o
  }))
  
  const otherUniversity = find(universities, o => o._id === universityId)
  console.log("otherUniversity", otherUniversity)

  const [isKnow, setIsKown] = useState(false)
  const [modalAgree, setModalAgree] = useState(false)

  const onUserRegister = () => {
    const { password, confirmPassword } = data;
    if (password === confirmPassword) {
      for (const item in data) {
        if (isEmpty(data[item].toString()) || data[item] === " ") return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.${item}`]}`);
      }
      return actions.createUser({ data, messages, history });
    }
    else {
      return NotificationManager.error(`${messages[`user.match`]}`)
    }
  };
  return (
    <Fragment>
      <Form>
        <Row>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="name"
                  name="name"
                  value={name}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(name) ? true : false}
                />
                <FormFeedback>Please input {messages[`user.name`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.name" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={genderOption}
                  name="gender"
                  value={find(genderOption, (o) => o.value === gender)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "gender", value: e.value },
                    });
                  }}
                />
                <FormFeedback>Please input {messages[`user.gender`]}</FormFeedback>
              </FormGroup>
              <span> <IntlMessages id="user.gender" />(*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="date"
                  min="1960-01-01T00:00"
                  name="birthDate"
                  value={moment(birthDate).format("YYYY-MM-DD")}
                  onChange={(e) => {
                    actions.handleInputChange(e)
                  }}
                  invalid={isEmpty(birthDate) ? true : false}
                />
                <FormFeedback>Please input {messages[`user.birthDay`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.birthDay" />(MM/DD/YYYY)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="email"
                  name="email"
                  value={email}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(email) ? true : false}
                />
                <FormFeedback>Please input {messages[`user.email`]}</FormFeedback>
              </FormGroup>
              <span> <IntlMessages id="user.email" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="number"
                  name="phone"
                  value={phone}
                  onChange={(e) => actions.handleInputChange(e)}
                />
              </FormGroup>
              <IntlMessages id="user.phone" />
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={englishLevelOption}
                  name="englishLevel"
                  value={find(englishLevelOption, (o) => o.value === englishLevel)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "englishLevel", value: e.value },
                    });
                  }}
                />
                <FormFeedback>Please input {messages[`user.englishLevel`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.englishLevel" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={countriesOption}
                  name="countryId"
                  value={find(countriesOption, (o) => o.value === countryId)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "countryId", value: e.value },
                    });
                  }}
                />
                <FormFeedback>Please input {messages[`user.countries`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.countries" /> (*)</span>
            </Label>
          </Colxx>
          {countryId === "5ed88148b78c05933012c853" ? (
            <Colxx xxs="12" md="6" lg="4" xl="4" >
              <Label className="form-group has-float-label mb-4">
                <FormGroup>
                  <Select
                    className="react-select"
                    classNamePrefix="react-select"
                    components={{ Input: CustomSelectInput }}
                    options={cityOption}
                    name="city"
                    value={find(cityOption, (o) => o.value === city)}
                    onChange={(e) => {
                      actions.handleInputChange({
                        target: { name: "city", value: e.value },
                      });
                    }}
                  />
                  <FormFeedback>Please input {messages[`user.city`]}</FormFeedback>
                </FormGroup>
                <span><IntlMessages id="user.city" /> (*)</span>
              </Label>
            </Colxx>
          ) : (
              <Colxx xxs="12" md="6" lg="4" xl="4" >
                <Label className="form-group has-float-label mb-4">
                  <FormGroup>
                    <Input
                      type="city"
                      name="city"
                      value={city}
                      onChange={(e) => actions.handleInputChange(e)}
                      invalid={isEmpty(city) ? true : false}
                    />
                    <FormFeedback>Please input {messages[`user.city`]}</FormFeedback>
                  </FormGroup>
                  <span><IntlMessages id="user.city" /> (*)</span>
                </Label>
              </Colxx>
            )}
          <Colxx xxs="12" md="6" lg="4" xl="4" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={knowUsFromOption}
                  name="knowUsFrom"
                  value={find(knowUsFromOption, (o) => o.value === knowUsFrom)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "knowUsFrom", value: e.value },
                    });
                  }}
                />
                <FormFeedback>Please input {messages[`user.knowUsFrom`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.knowUsFrom" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Select
                  className="react-select"
                  classNamePrefix="react-select"
                  components={{ Input: CustomSelectInput }}
                  options={universityIdOption}
                  name="universityId"
                  value={find(universityIdOption, (o) => o.value === universityId)}
                  onChange={(e) => {
                    actions.handleInputChange({
                      target: { name: "universityId", value: e.value },
                    });
                  }}
                />
                <FormFeedback>Please input {messages[`user.universityId`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.universityId" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="text"
                  name="faculty"
                  value={faculty}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(faculty) ? true : false}
                />
              </FormGroup>
              <IntlMessages id="user.faculty" />
            </Label>
          </Colxx>
          {get(otherUniversity,"isOther", false) && <Colxx xxs="12" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="text"
                  name="universityRequest"
                  value={universityRequest}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(universityRequest) ? true : false}
                />
              </FormGroup>
              <IntlMessages id="user.universityId" />
            </Label>
          </Colxx>}
          <Colxx xxs="12" md="12" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="password"
                  name="password"
                  value={password}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(password) ? true : false}
                />
                <FormFeedback>Please input {messages[`user.password`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.password" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="12" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="password"
                  name="confirmPassword"
                  value={confirmPassword}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(confirmPassword) ? true : false}
                />
                <FormFeedback>Please input {messages[`user.confirmPassword`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.confirmPassword" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="facebook"
                  name="facebook"
                  value={facebook}
                  onChange={(e) => actions.handleInputChange(e)}
                  invalid={isEmpty(facebook) ? true : false}
                />
                <FormFeedback>Please input {messages[`user.facebook`]}</FormFeedback>
              </FormGroup>
              <span><IntlMessages id="user.facebook" /> (*)</span>
            </Label>
          </Colxx>
          <Colxx xxs="12" md="6" lg="6" xl="6" >
            <Label className="form-group has-float-label mb-4">
              <FormGroup>
                <Input
                  type="text"
                  name="videoIntroduce"
                  value={videoIntroduce}
                  onChange={(e) => actions.handleInputChange(e)}
                />
                <FormFeedback>Please input {messages[`user.videoIntroduce`]}</FormFeedback>
              </FormGroup>
              <IntlMessages id="user.videoIntroduce" />
            </Label>
          </Colxx>
          <Colxx xxs="12" md="12" lg="12" xl="12" >
            <Label className="form-group has-float-label my-4">
              <IntlMessages id="user.reasonToJoin" />
            </Label>
            <FormGroup>
              <Input
                type="textarea"
                rows={5}
                name="reasonToJoin"
                value={reasonToJoin}
                onChange={(e) => actions.handleInputChange(e)}
              // invalid={isEmpty(reasonToJoin) ? true : false}
              />
              <FormFeedback>Please input {messages[`user.reasonToJoin`]}</FormFeedback>
            </FormGroup>
          </Colxx>
          <Colxx xxs="12" md="12" lg="12" xl="12" >
            <Label className="form-group has-float-label my-4">
              <IntlMessages id="user.reasonToLeader" />
            </Label>
            <FormGroup>
              <Input
                type="textarea"
                rows={5}
                placeholder="If you would like to improve your leadership, management skills, you can nominate yourself as a Team Leader. "
                name="reasonToLeader"
                value={reasonToLeader}
                onChange={(e) => actions.handleInputChange(e)}
              // invalid={isEmpty(reasonToLeader) ? true : false}
              />
              <FormFeedback>Please input {messages[`user.reasonToLeader`]}</FormFeedback>
            </FormGroup>
          </Colxx>
        </Row>
        <hr />
        <div className="d-flex justify-content-between">
          <CustomInput
            type="checkbox"
            className="h5"
            id="exampleCustomCheckbox"
            label="I’ve agreed Terms & Conditions"
            onClick={() => setIsKown(!isKnow)}
          />
          <Button
            color="primary"
            className="btn-shadow login default"
            size="lg"
            onClick={() => isKnow ? onUserRegister() : setModalAgree(!modalAgree)}
          >
            <IntlMessages id="user.register-button" />
          </Button>
        </div>
      </Form>
      <Modal
        isOpen={isOpen}
        size="lg"
        toggle={() => props.setModal()}
        centered
        style={{ boxShadow: "none" }}
      >
        <ModalHeader toggle={() => props.setModal()}>
          <IntlMessages id="brands.uploadAvatar" />
        </ModalHeader>
        <ModalBody>
          <UploadImage
            title="Upload Image"
            number={1}
            onRemove={file => actions.removeImage(file)}
            onUpload={file => {
              actions.handleInputChange({
                target: {
                  name: "avatar",
                  value: file
                }
              })
              props.setModal()
            }}
            onChange={() => props.setModal()}
          />
        </ModalBody>
      </Modal>
      <Modal isOpen={modalAgree}>
        <ModalBody className="text-center">
          <Label className="h5" >
            <IntlMessages id="modalAgree.sure" />
          </Label>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-center" >
          <Button
            outline
            onClick={() => setModalAgree(!modalAgree)}
          >
            OK
          </Button>
        </ModalFooter>
      </Modal>
    </Fragment>
  );
}

export default React.memo(injectIntl(Register));