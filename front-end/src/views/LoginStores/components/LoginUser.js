import React from "react";
import { Row, Card, FormGroup, FormFeedback, Form, Label, Input, Button, Alert } from "reactstrap";
import { NavLink } from "react-router-dom";
import IntlMessages from "helpers/IntlMessages";
import Select from "react-select";

import { injectIntl } from "react-intl";
import AnimateGroup from "theme/components/AnimateGroup";
import CustomSelectInput from "theme/components/common/CustomSelectInput";
import { Colxx } from "theme/components/common/CustomBootstrap";
import { NotificationManager } from "theme/components/common/react-notifications";

import { isEmpty, find } from "lodash";

function Login(props) {
  const { messages } = props.intl;
  const { loginError, history, data, actions } = props;
  const { email, password, loginAs } = props.data;

  const login = () => {
    for (const item in data) {
      if (isEmpty(data[item].toString()) || data[item] === " ") return NotificationManager.error(`${messages[`user.reWrite`]} ${messages[`user.${item}`]}`);
    }
    return actions.emailLogin({ data, messages, history });
  };

  const loginAsOption = ["srm", "student", "admin"].map(o => ({
    label: messages[`user.loginAs.${o}`],
    value: o
  }))

  return (
    <AnimateGroup enter={{ animation: "transition.slideRightBigIn" }}>
      <Row className="h-100">
        <Colxx xxs="12" md="8" className="mx-auto my-auto pt-5">
          <Card className="auth-card">
            <div className="form-side">
              <NavLink to={`#`} className="white login-card">
                <span className="logo-single" />
              </NavLink>
              <hr />
              {loginError && (
                <Alert color="danger">
                  <IntlMessages id="general.loginFail" />
                </Alert>
              )}
              <Form>
                <FormGroup>
                  <Label for="Email">Email</Label>
                  <Input
                    type="email"
                    name="email"
                    value={email}
                    id="Email"
                    onChange={e => actions.handleInputChange(e)}
                    invalid={isEmpty(email) ? true : false}
                  />
                  <FormFeedback>Oh noes! Email is not valid!</FormFeedback>
                </FormGroup>
                <FormGroup>
                  <Label for="Password">Password</Label>
                  <Input
                    type="password"
                    name="password"
                    id="Password"
                    value={password}
                    onChange={e => actions.handleInputChange(e)}
                    invalid={isEmpty(email) ? true : false}
                  />
                  <FormFeedback>Oh noes! Password is not valid!</FormFeedback>
                </FormGroup>
                {/* <FormGroup>
                  <Label >Role</Label>
                  <Select
                    className="react-select"
                    classNamePrefix="react-select"
                    components={{ Input: CustomSelectInput }}
                    options={loginAsOption}
                    name="loginAs"
                    value={find(loginAsOption, (o) => o.value === loginAs)}
                    onChange={(e) => {
                      actions.handleInputChange({
                        target: { name: "loginAs", value: e.value },
                      });
                    }}
                  />
                  <FormFeedback>{messages[`user.loginAs`]}</FormFeedback>
                </FormGroup> */}
              </Form>
                <Row>
                  <Colxx xxs="12" md="12" lg="12" xl="12">
                    <Button
                      color="primary"
                      size="lg"
                      className="default login"
                      block
                      onClick={() => login()}
                    >
                      <IntlMessages id="user.login-title" />
                    </Button>
                  </Colxx>
                  <Colxx xxs="12" md="12" lg="12" xl="12">
                    <Button
                      color="none"
                      outline
                      size="lg"
                      className="default login"
                      block
                      onClick={() => history.push(`/user/register`)}
                    >
                      <IntlMessages id="user.register" />
                    </Button>
                  </Colxx>
                </Row>
              <hr />
              <div className="login-footer text-center">
                iGen.edu.vn - Power by iGen.edu.vn 2020
                </div>
            </div>
            <div className="image-side-login">
              <div className="float-right"></div>
            </div>
          </Card>
        </Colxx>
      </Row>
    </AnimateGroup>
  );
}

export default injectIntl(Login);
