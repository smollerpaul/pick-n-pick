/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const facebookLogin = createAction(CONST.HANDLE_LOGIN_FACEBOOK);
export const facebookLoginSuccess = createAction(CONST.HANDLE_LOGIN_FACEBOOK_SUCCESS);
export const facebookLoginFail = createAction(CONST.HANDLE_LOGIN_FACEBOOK_FAIL);

export const googleLogin = createAction(CONST.HANDLE_LOGIN_GOOGLE);
export const googleLoginSuccess = createAction(CONST.HANDLE_LOGIN_GOOGLE_SUCCESS);
export const googleLoginFail = createAction(CONST.HANDLE_LOGIN_GOOGLE_FAIL);

export const emailLogin = createAction(CONST.HANDLE_EMAIL_LOGIN);
export const emailLoginSuccess = createAction(CONST.HANDLE_EMAIL_LOGIN_SUCCESS);
export const emailLoginFail = createAction(CONST.HANDLE_EMAIL_LOGIN_FAIL);

export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);

export const validateInputLogin = createAction(CONST.VALIDATE_INPUT_LOGIN);

export const clearAll = createAction(CONST.CLEAR_ALL);

export const isLoggedIn = createAction(CONST.HANDLE_IS_LOGGED_IN);
