import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as wrapperActions from "views/Wrapper/actions";
import * as API from "apis/auth";
import { takeAction } from "services/forkActionSagas";
import { save, get } from "services/localStoredService";
import { isEmpty } from "lodash";
import lodash from "lodash";
//Facebook Login
function* handleFacebookLogin(action) {
  try {
    let res = yield call(API.facebookLogin, action.payload.data);
    const { accessToken, refreshToken, userInfo } = res.data.data;
    save("refreshToken", refreshToken);
    save("accessToken", accessToken);
    save("userInfo", userInfo);
    yield put(actions.facebookLoginSuccess(res.data));
    yield put(wrapperActions.saveToken(accessToken));
    action.payload.history.push(`/`);
  } catch (err) {
    yield put(actions.facebookLoginFail(err));
  }
}

//Google Login
export function* handleGoogleLogin(action) {
  try {
    let res = yield call(API.googleLogin, action.payload.data);
    const { accessToken, refreshToken, userInfo } = res.data.data;
    save("refreshToken", refreshToken);
    save("accessToken", accessToken);
    save("userInfo", userInfo);
    yield put(actions.googleLoginSuccess(res.data));
    yield put(wrapperActions.saveToken(accessToken));
    action.payload.history.push(`/`);
  } catch (err) {
    yield put(actions.googleLoginFail(err));
  }
}

//Email Login
export function* handleEmailLogin(action) {
  try {
    let res = yield call(API.userLogin, action.payload.data);
    const { accessToken, refreshToken, userInfo } = res.data;
    save("refreshToken", refreshToken);
    save("accessToken", accessToken);
    save("userInfo", userInfo);
    save('role', userInfo.role)
    yield put(actions.emailLoginSuccess(res.data));
    yield put(wrapperActions.saveToken(accessToken));
    if (res.data.userInfo.isSRM) {
      action.payload.history.push(`/app/srms`);
    } else {
      if (lodash.get(res.data, "userInfo.role", "") === "uniLeader")
        action.payload.history.push(`/app/classes`);
      else if (lodash.get(res.data, "userInfo.role", "") === "leader")
        action.payload.history.push(`/app/leaders`);
      else if (lodash.get(res.data, "userInfo.role", "") === "student")
        action.payload.history.push(`/app/students`);
      else action.payload.history.push(`/admin`);
    }
  } catch (err) {
    yield put(actions.emailLoginFail(err));
  }
}

//check is Logged in

export function* handleIsLoggedIn(action) {
  const token = yield get("accessToken");
  if (!isEmpty(token)) {
    action.payload.history.push(`/`);
  }
}

////////////////////////////////////////////////////////////
export function* googleLogin() {
  yield takeAction(actions.googleLogin, handleGoogleLogin);
}
export function* facebookLogin() {
  yield takeAction(actions.facebookLogin, handleFacebookLogin);
}
export function* emailLogin() {
  yield takeAction(actions.emailLogin, handleEmailLogin);
}
export function* isLoggedIn() {
  yield takeAction(actions.isLoggedIn, handleIsLoggedIn);
}
////////////////////////////////////////////////////////////

export default [facebookLogin, googleLogin, emailLogin, isLoggedIn];
