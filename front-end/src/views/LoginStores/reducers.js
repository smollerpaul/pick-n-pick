/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { clearAll } from "services/localStoredService";

export const name = "LoginStores";

const initialState = freeze({
  isLoading: false,
  data: {
    email: " ",
    password: "",
  }
});

export default handleActions(
  {
    [actions.googleLoginSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    [actions.facebookLoginSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    [actions.emailLoginSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    [actions.facebookLogin]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.googleLogin]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.emailLogin]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;

      return freeze({
        ...state,
        loginError: false,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    [actions.emailLoginFail]: (state, action) => {
      return freeze({
        ...state,
        loginError: true,
        isLoading: false
      });
    },
    [actions.googleLoginFail]: (state, action) => {
      return freeze({
        ...state,
        loginError: true,
        isLoading: false
      });
    },
    [actions.facebookLoginFail]: (state, action) => {
      return freeze({
        ...state,
        loginError: true,
        isLoading: false
      });
    },
    [actions.validateInputLogin]: (state, action) => {
      return freeze({
        ...state,
        emailError: true,
        passwordError: true
      });
    },
    [actions.clearAll]: (state, action) => {
      clearAll();
      return freeze({
        ...initialState
      });
    }
  },
  initialState
);
