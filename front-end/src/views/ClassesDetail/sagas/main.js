import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/classes";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllClasses(action) {
  try {
    const res = yield call(API.fetchAllClasses, action.payload.data);
    yield put(actions.fetchAllClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewClasses(action) {
  try {
    const res = yield call(API.createNewClasses, action.payload.data);
    yield put(actions.createNewClassesSuccess(res));
    action.payload.history.push(`/app/classes`)
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditClasses(action) {
  try {
    const res = yield call(API.editClasses, action.payload.data);
    yield put(actions.editClassesSuccess(res));
    action.payload.history.push(`/app/classes`)
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewClassesDraft(action) {
  try {
    const res = yield call(API.createNewClasses, action.payload.data);
    yield put(actions.createNewClassesDraftSuccess(res));
    // action.payload.history.push(`/app/classes/${res.data._id}`)
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleeditClassesDraft(action) {
  try {
    const res = yield call(API.editClasses, action.payload.data);
    yield put(actions.editClassesDraftSuccess(res));
    // action.payload.history.push(`/app/classes/${res.data._id}`)
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteClasses(action) {
  try {
    const res = yield call(API.deleteClasses, action.payload.data);
    yield put(actions.deleteClassesSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllStudents(action) {
  try {
    const res = yield call(API.fetchAllStudents, action.payload.data);
    yield put(actions.fetchAllStudentsSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleaddStudentInclass(action) {
  try {
    const res = yield call(API.addStudentInclass, action.payload.data);
    yield put(actions.addStudentInclassSuccess(res));
    actions.fetchAllStudents({
      data: {
        search: {
          usePagination: false,
          isExactly: true,
        }
      }
    })
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleupdateTask(action) {
  try {
    const {data} = action.payload;
    for (let item of data) yield put(actions.editClasses({data: item}))
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlepromveStudtents(action) {
  try {
    const res = yield call(API.promveStudtents, action.payload.data);
    yield put(actions.getInfo(action.payload.data.classId));
    yield put(actions.promveStudtentsSuccess(res));
    NotificationManager.success("Promte Student Success")
  } catch (err) {
    NotificationManager.error(err);
  }
}

export function* handleremoveStudentInclass(action) {
  try {
    const res = yield call(API.removeStudentInclass, action.payload.data);
    yield put(actions.getInfo(action.payload.classId));
    yield put(actions.removeStudentInclassSuccess(res));
    NotificationManager.success("Undo Student Success")
  } catch (err) {
    NotificationManager.error(err);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function* fetchAllClasses() {
  yield takeAction(actions.fetchAllClasses, handleFetchAllClasses);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewClasses() {
  yield takeAction(actions.createNewClasses, handlecreateNewClasses);
}

export function* editClasses() {
  yield takeEvery(actions.editClasses, handleEditClasses);
}

export function* createNewClassesDraft() {
  yield takeAction(actions.createNewClassesDraft, handlecreateNewClassesDraft);
}

export function* editClassesDraft() {
  yield takeEvery(actions.editClassesDraft, handleeditClassesDraft);
}

export function* deleteClasses() {
  yield takeAction(actions.deleteClasses, handleDeleteClasses);
}

export function* fetchAllStudents() {
  yield takeAction(actions.fetchAllStudents, handlefetchAllStudents);
}

export function* addStudentInclass() {
  yield takeAction(actions.addStudentInclass, handleaddStudentInclass);
}

export function* promveStudtents() {
  yield takeAction(actions.promveStudtents, handlepromveStudtents);
}

export function* removeStudentInclass() {
  yield takeAction(actions.removeStudentInclass, handleremoveStudentInclass);
}

export function* updateTask() {
  yield takeAction(actions.updateTask, handleupdateTask);
}

export default [
  fetchAllClasses, 
  createNewClasses,
  editClasses,
  deleteClasses,
  fetchAllStudents,
  addStudentInclass,
  getInfo,
  updateTask,
  promveStudtents,
  createNewClassesDraft,
  editClassesDraft,
  removeStudentInclass
];
