import React, { Fragment } from "react";
import Board from "react-trello";
import { get, find, flattenDeep } from "lodash";
import { injectIntl } from "react-intl";
import CustomCard from "./CustomCard";

function Tasks({ leadStatus, actions, leads }) {
  const taskList = JSON.parse(
    JSON.stringify({
      lanes: leadStatus.map(status => ({
        id: status._id,
        title: status.name,
        cards: flattenDeep(leads.map(lead => {
          return status._id === lead.status ? {
            id: lead._id,
            title: lead.title,
            updatedAt: lead.updatedAt,
            status: lead.status,
            source: lead.source
          } : []
        }))
      })),
    })
  );

  const checkDifferent = (dataChange) => {
    try {
      const newCard = find(dataChange, (data, index) =>
        data.cards.length > taskList.lanes[index].cards.length ? data : null
      )
      if(newCard){
        const data = newCard.cards.map(o =>({
            _id: o.id,
            source: o.source,
            status: newCard.id
          }))
        actions.updateTask({data}); 
      }
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Fragment>
      <Board
        data={taskList}
        collapsibleLanes
        onCardClick={(e) => actions.getInfo({ data: { _id: e } })}
        classNames="card cardbody"
        components={{ Card: CustomCard }}
        onDataChange={(e) => checkDifferent(get(e, "lanes", []))}
      />
    </Fragment>
  );
}

export default React.memo(injectIntl(Tasks));
