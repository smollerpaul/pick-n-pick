import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Classes from "./Classes";

function Container(props) {
  const { isLoading } = props;
  const { actions, pagination } = props
  const { id } = props.match.params;

  useEffect(() => {
    if (id !== undefined && id !== "create") {
      actions.getInfo(id);
    }
    actions.fetchAllStudents({
      data: {
        ...pagination,
        search: {
          usePagination: false,
          isExactly: true,
        }
      }
    })

    return () => {
      actions.clear();
    };
  }, [id, actions]);

  return (
    <React.Fragment>
      {isLoading && <div className="loading"></div>}
      <Classes {...props} />
    </React.Fragment>
  );
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
