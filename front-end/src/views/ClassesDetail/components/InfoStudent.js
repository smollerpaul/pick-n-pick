import React, { useState, Fragment } from "react";
import { CardImg, CardTitle, Modal, ModalBody, ModalFooter, Button, Row, Label } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import { Colxx } from "theme/components/common/CustomBootstrap";

import { injectIntl } from "react-intl";
import { get, isEmpty } from "lodash"
function ClassesDetail(props) {
    const { dataModal, isOpen, actions, data, pagination } = props;
    const { messages } = props.intl
    const [isDelete, setModalDelete] = useState(false)
    const [isDemote, setModalDemote] = useState(false)
    const [isPromote, setModalPromote] = useState(false)
    return (
        <Fragment>
            <Modal isOpen={isOpen} size="md" >
                <ModalBody>
                    <div className="text-center" >
                        <CardImg src={get(dataModal, "avatar", "")} />
                    </div>
                    <div>
                        <Label className="h2 d-block my-2 px-2 " style={{ backgroundColor: "#BF9553", color: "white" }}>
                            <IntlMessages id="user.info" />
                        </Label>
                    </div>
                    <Row>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.name" /> :
                            <span className="ml-1">{get(dataModal, "name", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.gender" /> :
                            <span className="ml-1">{get(dataModal, "gender", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.email" /> :
                            <span className="ml-1">{get(dataModal, "email", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.faculty" /> :
                            <span className="ml-1">{get(dataModal, "faculty", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.isHighQualityStudentProgram" /> :
                            <span className="ml-1">{messages[`user.isHighQualityStudentProgram.${get(dataModal, "isHighQualityStudentProgram", "")}`]}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.knowUsFrom" /> :
                            <span className="ml-1">{get(dataModal, "knowUsFrom", "")}</span>
                            </CardTitle>
                        </Colxx>
                        <Colxx xxs="12" className="ml-2">
                            <CardTitle className="my-2 h5" >
                                <IntlMessages id="user.reasonToJoin" /> :
                            <span className="ml-1">{get(dataModal, "reasonToJoin", "")}</span>
                            </CardTitle>
                        </Colxx>
                    </Row>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-around" >
                    <Button color="info" outline onClick={() => actions.clearDataModal({ name: "dataModal" })}>
                        <IntlMessages id="button.cancel" />
                    </Button>
                    {/* <Button color="danger" outline onClick={() => setModalDelete(!isDelete)}>
                        <IntlMessages id="button.delete" />
                    </Button> */}
                    {dataModal.role === "leader" ? (
                        <Button color="warning" outline onClick={() => setModalDemote(!isDemote)}>
                            <IntlMessages id="button.demote" />
                        </Button>
                    ) : isEmpty(data.leader) ? (
                        <Button color="warning" outline onClick={() => setModalPromote(!isPromote)}>
                            <IntlMessages id="classes.promote" />
                        </Button>
                    ) : null}
                </ModalFooter>
            </Modal>
            <Modal isOpen={isDelete} size="md" >
                <ModalBody>
                    <Label>
                        <IntlMessages id="modalDelete.sure" />
                    </Label>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-around" >
                    <Button color="info" outline onClick={() => setModalDelete(!isDelete)}>
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button color="danger" outline onClick={() => {
                        actions.removeStudentInclass({ data: dataModal._id })
                        setModalDelete(!isDelete)
                    }}>
                        <IntlMessages id="button.delete" />
                    </Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={isDemote} size="md" >
                <ModalBody>
                    <Label>
                        <IntlMessages id="modalDelete.sure" />
                    </Label>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-around" >
                    <Button color="info" outline onClick={() => setModalDemote(!isDemote)}>
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button color="danger" outline onClick={() => {
                        actions.promveStudtents({
                            data: {
                                classId: dataModal.classId, studentId: dataModal._id, role: "student"
                            },
                        })
                        setModalDemote(!isDemote)
                        actions.clearDataModal({ name: "students" })
                    }}>
                        <IntlMessages id="button.confirm" />
                    </Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={isPromote} size="md" >
                <ModalBody>
                    <Label>
                        <IntlMessages id="modalDelete.sure" />
                    </Label>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-around" >
                    <Button color="info" outline onClick={() => setModalPromote(!isPromote)}>
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button color="danger" outline onClick={() => {
                        actions.promveStudtents({
                            data: {
                                classId: dataModal.classId, studentId: dataModal._id, role: "leader"
                            },
                        })
                        setModalPromote(!isPromote);
                        actions.clearDataModal({ name: "students" })
                    }}>
                        <IntlMessages id="classes.promote" />
                    </Button>
                </ModalFooter>
            </Modal>
        </Fragment>
    );
}

export default React.memo(injectIntl(ClassesDetail));
