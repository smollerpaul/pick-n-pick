import React, { Fragment } from "react";
import { Card, CardBody, CardImg, CardTitle } from "reactstrap";
import { Button, Input, Label, FormGroup, FormFeedback } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import { injectIntl } from "react-intl";
import ReactTable from "react-table";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import DataTablePagination from "theme/components/DatatablePagination";
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import { isEmpty, filter } from "lodash";
import classnames from "classnames";

import ModalControl from "./InfoStudent"

function ClassesDetail(props) {
  const { actions, isEdit, data, students, studentsInClass, history, leaders, pagination } = props;
  const { name, classStudents } = data;
  const { tabs } = props;
  const { messages } = props.intl;

  const studentsOption = filter(students, (o) => (o.isWaitingForClass || o.status === "3"))

  const dataTableColumns = [
    {
      Header: "Avatar",
      accessor: "avatar",
      className: "justify-center",
      width: 100,
      Cell: (props) => {
        return <CardImg top src={props.value} alt={props.values} />;
      },
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => (
        <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
      ),
    },
    {
      Header: messages["user.gender"],
      accessor: "gender",
      className: "justify-center",
      width: 100,
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{messages[`user.gender.${props.value}`]}</p>
        </div>
      ),
    },
    {
      Header: messages["user.englishLevel"],
      accessor: "englishLevel",
      className: "justify-center",
      width: 100,
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">
            {messages[`user.englishLevel.${props.value}`]}
          </p>
        </div>
      ),
    },
    {
      Header: messages["user.faculty"],
      accessor: "faculty",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value}</p>
        </div>
      ),
    },
    {
      Header: "Move",
      accessor: "_id",
      className: "justify-center",
      width: 150,
      Cell: ({ original }) => {
        let isHave = studentsInClass.includes(original._id);
        let isleaders = leaders.includes(original._id);
        if (original.status === "3") return <Label className="text-muted" ><IntlMessages id="classes.waitInterview" /></Label>
        else {
          if (!data.leader) {
            return isHave ? (
              <Button
              size="sm"
                className="default mr-1"
                color="warning"
                onClick={() =>
                  actions.promveStudtents({
                    data: {
                      classId: data._id, studentId: original._id, role: "leader"
                    },
                  })
                }
              >
                {isleaders ? (
                  <IntlMessages id="classes.leader" />
                ) : (
                    <IntlMessages id="classes.promote" />
                  )}
              </Button>
            ) : original.role !== "leader" ? (
              <div className="text-center">
                  <Button
                  size="sm"
                    className="default"
                    color="primary"
                    onClick={() =>
                      actions.addStudentInclass({ data: { classId: data._id, studentId: original._id } })
                    }
                  >
                    <IntlMessages id="classes.addinClass" />
                  </Button>
                </div>
              ) : null;
          } else {
            return isHave ? (
              <Button
              size="sm"
                className="default mr-1"
                color="warning"
                onClick={() =>
                  actions.removeStudentInclass({data: original._id, classId: data._id })
                }
              >
                <IntlMessages id="classes.undo" />
              </Button>
            ) : (
                <div className="text-center">
                  <Button
                  size="sm"
                    className="default"
                    color="primary"
                    onClick={() =>
                      actions.addStudentInclass({ data: { classId: data._id, studentId: original._id } })
                    }
                  >
                    <IntlMessages id="classes.addinClass" />
                  </Button>
                </div>
              );
          }
        }
      },
    },
    {
      Header: messages["students.rejectReason"],
      accessor: "lastInterviewResult.rejectReason",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value}</p>
        </div>
      ),
    },
  ];
  const dataTableColumnsMember = [
    {
      Header: "Avatar",
      accessor: "avatar",
      className: "justify-center",
      width: 100,
      Cell: (props) => {
        return <CardImg top src={props.value} alt="avatar" />;
      },
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => (
        <p className="text-capitalize text-muted">{props.value ? props.value : ""}</p>
      ),
    },
    {
      Header: messages["user.faculty"],
      accessor: "faculty",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{props.value}</p>
        </div>
      ),
    },
    {
      Header: messages["user.gender"],
      accessor: "gender",
      className: "d-flex justify-content-center",
      width: 150,
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">{messages[`user.gender.${props.value}`]}</p>
        </div>
      ),
    },
    {
      Header: messages["user.englishLevel"],
      accessor: "englishLevel",
      className: "d-flex justify-content-center",
      width: 150,
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-capitalize text-muted">
            {messages[`user.englishLevel.${props.value}`]}
          </p>
        </div>
      ),
    },
    {
      Header: "Role",
      accessor: "role",
      className: "d-flex justify-content-center",
      width: 200,
      Cell: ({ value }) => {
        return messages[`classes.role.${value}`];
      },
    },
  ];

  return (
    <Fragment>
      <Card>
        <CardBody>
          <CardTitle>
            <Label>
              <h3>{isEdit ? name : <IntlMessages id="classes.addNew" />}</h3>
            </Label>
            <div className="float-right">
              <Button
                className="mr-2"
                color="primary"
                onClick={() =>
                  isEdit
                    ? actions.editClasses({ data, history })
                    : actions.createNewClasses({ data, history })
                }
              >
                <IntlMessages id="button.save" />
              </Button>
              <Button
                className="mr-2"
                color="secondary"
                outline
                onClick={() => {
                  history.push(`/app/classes`);
                  actions.clear();
                }}
              >
                <IntlMessages id="button.cancel" />
              </Button>
              {/* {isEdit && (
                <Button
                  className="mr-2"
                  color="danger"
                  onClick={() => actions.deleteClasses({ data })}
                >
                  <IntlMessages id="button.delete" />
                </Button>
              )} */}
            </div>
          </CardTitle>
          <FormGroup>
            <Label>
              <IntlMessages id="classes.name" />
            </Label>
            <Input
              type="name"
              name="name"
              value={name}
              onChange={(e) => actions.handleInputChange(e)}
              invalid={isEmpty(name) ? true : false}
            />
            <FormFeedback>{messages[`classes.name`]}</FormFeedback>
          </FormGroup>
          <Nav tabs className="separator-tabs">
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "unClass" })}
                onClick={() => {
                  actions.changeTabs("unClass");
                }}
              >
                <i className="simple-icon-layers" /> <IntlMessages id="classes.unClass" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "member" })}
                onClick={() => {
                  actions.changeTabs("member");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="classes.member" />
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={tabs} className="chat-app-tab-content mt-3">
            <TabPane tabId="unClass" className="chat-app-tab-pane">
              <h3>
                <IntlMessages id="classes.unClass" />
              </h3>
              {isEdit && (
                <ReactTable
                  data={studentsOption}
                  columns={dataTableColumns}
                  TbodyComponent={CustomTbodyComponent}
                  minRows={0}
                  sortable={false}
                  filterable={false}
                  showPageJump={true}
                  showPageSizeOptions={true}
                  pageSize={studentsOption.length +1}
                  PaginationComponent={DataTablePagination}
                  className="-striped -highlight h-full overflow-hidden"
                />
              )}
            </TabPane>
            <TabPane tabId="member" className="chat-app-tab-pane">
              <h3>
                <IntlMessages id="classes.unClass" />
              </h3>
              {isEdit && (
                <ReactTable
                  data={classStudents}
                  columns={dataTableColumnsMember}
                  TbodyComponent={CustomTbodyComponent}
                  minRows={0}
                  sortable={false}
                  filterable={false}
                  showPageJump={true}
                  showPageSizeOptions={true}
                  pageSize={classStudents.length +1}
                  PaginationComponent={DataTablePagination}
                  className="-striped -highlight h-full overflow-hidden"
                  onPageChange={(pageIndex) => actions.fetchAllStudents({ data: { ...pagination, page: pageIndex + 1 } })}
                    onPageSizeChange={(pageSize, pageIndex) =>
                      actions.fetchAllStudents({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } })
                    }
                  getTrProps={(state, rowInfo, column) => {
                    return {
                      className: "cursor-pointer",
                      onClick: () => actions.handleToggleModal({ data: rowInfo.original }),
                    };
                  }}
                />
              )}
            </TabPane>
          </TabContent>
        </CardBody>
      </Card>
      <ModalControl {...props} />
    </Fragment>
  );
}

export default React.memo(injectIntl(ClassesDetail));
