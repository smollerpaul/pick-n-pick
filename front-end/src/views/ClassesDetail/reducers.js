/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";
import {get,filter} from "lodash";
export const name = "ClassesDetail";

const initialState = freeze({
  data: {
    name: " ",
  },
  modalAvatar: false,
  tabs: "unClass",
  message: "",
  classes: [],
  students: [],
  leaders: [],
  dataModal: [],
  studentsInClass: [],
  isOpen: false,
  isLoading: false,
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      usePagination: true,
      isExactly: false,
    },
  },
});

export default handleActions(
  {
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.fetchAllClasses]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        data: {
          ...state.data,
          [name]: value,
        },
      });
    },
    [actions.fetchAllClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        classes: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.fetchAllStudentsSuccess]: (state, action) => {
      return freeze({
        ...state,
        students: action.payload.data,
      });
    },
     [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
        dataModal: action.payload.data,
      });
    },
    [actions.createNewClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        data: initialState.data,
      });
    },
    [actions.createNewClassesDraftSuccess]: (state, action) => {
      return freeze({
        ...state,
        data: action.payload.data,
        isEdit: true,
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value
          }
        }
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: true,
        data: action.payload.data,
      });
    },
    [actions.editClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        classes: initialState.classes,
      });
    },
    [actions.editClassesDraftSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: true,
        isOpen:false,
        data: action.payload.data,
      });
    },
    [actions.deleteClassesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        classes: [...state.classes.filter(o => o._id !== action.payload.data)]
      });
    },
    [actions.addStudentInclassSuccess]: (state, action) => {
      return freeze({
        ...state,
        studentsInClass: [...state.studentsInClass, get(action, "payload.data.studentId", "")]
      });
    },
    [actions.removeStudentInclassSuccess]: (state, action) => {
      return freeze({
        ...state,
        studentsInClass: filter(state.studentsInClass, o => o !== action.payload.data._id)
      });
    },
    [actions.promveStudtentsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isOpen: false,
        leaders: [...state.leaders, get(action, "payload.data.studentId", "")],
        students: []
      });
    },
    [actions.clear]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        data: initialState.data,
      });
    },
    [actions.clearDataModal]: (state, action) => {
      const {name} = action.payload;
      return freeze({
        ...state,
        isOpen: false,
        [name]: [],
      });
    },
  },
  initialState
);
