/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("CLASSES_DETAIL/HANDLE_INPUT_CHANGE");

export const handleToggleModal = createAction("CLASSES_DETAIL/HANDLE_TOGGLE_MODAL");

export const fetchAllClasses = createAction("CLASSES_DETAIL/FETCH_ALL_CLASSES");
export const fetchAllClassesSuccess = createAction("CLASSES_DETAIL/FETCH_ALL_Classes_SUCCESS");

export const getInfo = createAction("CLASSES_DETAIL/GET_INFO_CLASSES");
export const getInfoSuccess = createAction("CLASSES_DETAIL/ GET_INFO_Classes_SUCCESS");

export const createNewClasses = createAction("CLASSES_DETAIL/CREATE_NEW_CLASSES");
export const createNewClassesSuccess = createAction("CLASSES_DETAIL/CREATE_NEW_Classes_SUCCESS");

export const editClasses = createAction("CLASSES_DETAIL/EDIT_CLASSES");
export const editClassesSuccess = createAction("CLASSES_DETAIL/EDIT_Classes_SUCCESS");

export const createNewClassesDraft = createAction("CLASSES_DETAIL/CREATE_NEW_CLASSES_DRAFT");
export const createNewClassesDraftSuccess = createAction("CLASSES_DETAIL/CREATE_NEW_Classes_DRAFT_SUCCESS");

export const editClassesDraft = createAction("CLASSES_DETAIL/EDIT_CLASSES_DRAFT");
export const editClassesDraftSuccess = createAction("CLASSES_DETAIL/EDIT_Classes_DRAFT_SUCCESS");

export const promveStudtents = createAction("CLASSES_DETAIL/PROMOVE_STUDENTS");
export const promveStudtentsSuccess = createAction("CLASSES_DETAIL/PROMOVE_STUDENTS_SUCCESS");

export const updateTask = createAction("CLASSES_DETAIL/UPDATE_TASK_CLASSES");
export const updateTaskSuccess = createAction("CLASSES_DETAIL/UPDATE_TASK_Classes_SUCCESS");

export const deleteClasses = createAction("CLASSES_DETAIL/DELETE_CLASSES");
export const deleteClassesSuccess = createAction("CLASSES_DETAIL/DELETE_Classes_SUCCESS");

export const fetchAllStudents = createAction("CLASSES_DETAIL/FETCH_ALL_STUDENTS");
export const fetchAllStudentsSuccess = createAction("CLASSES_DETAIL/FETCH_ALL_STUDENTS_SUCCESS");

export const addStudentInclass = createAction("CLASSES_DETAIL/ADD_STUDENT_IN_CLASS");
export const addStudentInclassSuccess = createAction("CLASSES_DETAIL/ADD_STUDENT_IN_CLASS_SUCCESS");

export const removeStudentInclass = createAction("CLASSES_DETAIL/REMOVE_STUDENT_IN_CLASS");
export const removeStudentInclassSuccess = createAction("CLASSES_DETAIL/REMOVE_STUDENT_IN_CLASS_SUCCESS");

export const clear = createAction("CLASSES_DETAIL/CLEAR");
export const clearDataModal = createAction("CLASSES_DETAIL/CLEAR_DATA_MODAL");
export const handleSearch = createAction("CLASSES_DETAIL/HANDLE_SEARCH");
export const changeTabs = createAction("CLASSES_DETAIL/CHANGE_TABS");
