import React, { Fragment, useState } from "react";
import { Row, Card, CardBody, Label, CardImg, Button, Modal } from "reactstrap";
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import { FormGroup, Input, FormFeedback, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";

import { Colxx } from "theme/components/common/CustomBootstrap";
import Breadcrumb from "theme/containers/navs/Breadcrumb";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import classnames from "classnames";
import Pagination from "theme/containers/pages/Pagination";

import TableClassSpeaking from "./TableCLassSpeadking"
import Topic from "./Topic"

import { get, isEmpty } from "lodash";

function Students(props) {
  const { students, studentsPendding, listProcessing, match, classSpeakings } = props;
  const { actions, isLoading, pagination, tabs, history, data } = props;

  const { page, limit, totalPages } = pagination;
  const { messages } = props.intl;

  const studentsOption = students.map((o, index) => ({ ...o, stt: index + 1 }))

  const [modalOpen, setModalOpen] = useState(false);
  const onPageChange = (pageIndex) => {
    actions.fetchAllStudents({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllStudents({ data: { ...pagination, page: pageIndex + 1, limit: pageSize } });
  };
  const dataTableColumns = [
    {
      Header: "STT",
      accessor: "stt",
      className: "justify-center",
      width: 40,
      Cell: (props) => <p className="text-capitalize text-muted">{props.value}</p>,
    },
    {
      Header: "Avatar",
      accessor: "avatar",
      className: "justify-center",
      width: 80,
      Cell: (props) =>
        <a to="route" target="_blank" href={`${props.value}`}><CardImg top src={isEmpty(get(props, "value", ""))
          ? "/assets/img/avatar.png"
          : get(props, "value", "")}
          alt="Avatar" /></a>
    },
    {
      Header: messages["user.name"],
      accessor: "name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">{props.value}</p>,
    },
    {
      Header: messages["user.faculty"],
      accessor: "faculty",
      width: 150,
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">{props.value}</p>,
    },
    {
      Header: messages["user.phone"],
      accessor: "phone",
      width: 200,
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize text-muted">{props.value}</p>,
    },
    {
      Header: messages["user.email"],
      accessor: "email",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p className="text-muted">{props.value ? props.value : 0}</p>
        </div>
      ),
    },
    {
      Header: 'Intro',
      accessor: "videoIntroduce",
      className: "d-flex justify-content-center",
      width: 150,
      Cell: (props) => (
        <a className="text-reset" href={`${props.value}`} style={{textDecoration: "underline"}} target="_blank" >View</a>
      ),
    },
  ];

  return (
    <Fragment>
      <Row>
        <Colxx xxs="12" className="header-top">
          {/* <Breadcrumb heading="menu.students" match={match} /> */}
        </Colxx>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs">
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "pendding" })}
                onClick={() => {
                  actions.fetchAllStudentsPendding({ data: {
                    ...pagination,
                    search: {
                      usePagination: false,
                      isExactly: true,
                    }
                  } });
                  actions.changeTabs("pendding");
                }}
              >
                <i className="simple-icon-layers" /> <IntlMessages id="students.studentsPedding" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "table" })}
                onClick={() => {
                  actions.fetchAllStudents({ data: pagination });
                  actions.changeTabs("table");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="classes.students" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "classSpeaking" })}
                onClick={() => {
                  actions.fetchAllClassSpeaking({ data: pagination });
                  actions.changeTabs("classSpeaking");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="students.classesSpeaking" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: tabs === "topic" })}
                onClick={() => {
                  actions.fetchAllTopic({ data: pagination });
                  actions.changeTabs("topic");
                }}
              >
                <i className="simple-icon-grid" /> <IntlMessages id="students.topic" />
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={tabs} className="chat-app-tab-content mt-3">
            <TabPane tabId="table" className="chat-app-tab-pane">
              <Card className="mb-4">
                <CardBody>
                  <ReactTable
                    data={studentsOption}
                    columns={dataTableColumns}
                    TbodyComponent={CustomTbodyComponent}
                    defaultPageSize={limit}
                    sortable={false}
                    filterable={false}
                    showPageJump={true}
                    PaginationComponent={DataTablePagination}
                    showPageSizeOptions={true}
                    pageSize={studentsOption.length +1}
                    page={page - 1}
                    loading={isLoading}
                    pages={totalPages}
                    manual
                    onPageChange={(pageIndex) => onPageChange(pageIndex)}
                    onPageSizeChange={(pageSize, pageIndex) =>
                      onPageSizeChange(pageSize, pageIndex)
                    }
                    className="-striped -highlight h-full overflow-hidden"
                  />
                </CardBody>
              </Card>
            </TabPane>
            <TabPane tabId="pendding" className="chat-app-tab-pane mt-3">
              <Fragment>
                <Row>
                  {studentsPendding.map((o) => {
                    let isHave = listProcessing.includes(o._id);
                    return (
                      <Colxx xxs="12" md="6" lg="4" className="my-2">
                        <Card body>
                          <CardImg
                            top
                            src={
                              isEmpty(get(o, "studentId.avatar", ""))
                                ? "/assets/img/avatar.png"
                                : get(o, "studentId.avatar", "")
                            }
                            alt="Avatar"
                          />
                          <Label className="mt-3 mb-1">
                            <IntlMessages id="user.name" />:{"  "}
                            <span className="text-bold">{get(o, "studentId.name", "")}</span>
                          </Label>
                          <Label className="mt-1 mb-1">
                            <IntlMessages id="user.faculty" />:{"  "}
                            <span className="text-bold">{get(o, "studentId.faculty", "")}</span>
                          </Label>
                          <Label className="mt-1 mb-1">
                            <IntlMessages id="user.phone" />:{"  "}
                            <span className="text-bold">{get(o, "studentId.phone", "")}</span>
                          </Label>
                          <Label className="mt-1 mb-1">
                            <IntlMessages id="user.reasonToJoin" />:{"  "}
                            <span className="text-bold">{get(o, "studentId.reasonToJoin", "")}</span>
                          </Label>
                          <Label className="mt-1 mb-1">
                            <IntlMessages id="user.englishLevel" />:{"  "}
                            <span className="text-bold">
                              {messages[`user.englishLevel.${get(o, "studentId.englishLevel", "")}`]}
                            </span>
                          </Label>
                          {isHave === false && (
                            <div className="d-flex justify-content-around">
                              <Button
                                color="primary"
                                className="default"
                                onClick={() =>
                                  actions.handleStudentsPendding({
                                    data: { interviewId: o._id, accept: true },
                                  })
                                }
                              >
                                <IntlMessages id="button.accept" />
                              </Button>{" "}
                              <Button
                                color="danger"
                                className="default"
                                outline
                                onClick={() => {
                                  actions.handleInputChange({
                                    target: { name: "interviewId", value: o._id },
                                  });
                                  setModalOpen(!modalOpen);
                                }}
                              >
                                <IntlMessages id="button.reject" />
                              </Button>
                            </div>
                          )}
                        </Card>
                      </Colxx>
                    );
                  })}
                </Row>
                <Pagination
                  currentPage={page}
                  totalPage={totalPages}
                  onChangePage={(i) =>
                    actions.fetchAllCurrentProductCategories({
                      data: { ...pagination, page: i },
                    })
                  }
                />
              </Fragment>
            </TabPane>
            <TabPane tabId="classSpeaking" className="chat-app-tab-pane">
              <TableClassSpeaking {...props} />
            </TabPane>
            <TabPane tabId="topic" className="chat-app-tab-pane">
              <Topic {...props} />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
      <Modal isOpen={modalOpen} size="md">
        <ModalBody>
          <ModalHeader className="text-center">
            <Label>
              <IntlMessages id="modalDelete.sure" />
            </Label>
          </ModalHeader>
          <FormGroup>
            <Label>
              <IntlMessages id="students.rejectReason" />
            </Label>
            <Input
              type="textarea"
              rows={5}
              name="rejectReason"
              value={get(data, "rejectReason", "")}
              onChange={(e) => actions.handleInputChange(e)}
              invalid={isEmpty(get(data, "rejectReason", "")) ? true : false}
            />
            <FormFeedback>{messages[`students.rejectReason`]}</FormFeedback>
          </FormGroup>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-around">
          <Button
            color="info"
            className="default"
            outline
            onClick={() => {
              setModalOpen(!modalOpen);
              actions.clear();
            }}
          >
            <IntlMessages id="button.cancel" />
          </Button>
          <Button
            color="danger"
            className="default"
            outline
            onClick={() => {
              actions.handleStudentsPendding({
                data: {
                  interviewId: get(data, "interviewId", ""),
                  accept: false,
                  rejectReason: get(data, "rejectReason", ""),
                },
              });
              setModalOpen(!modalOpen);
            }}
          >
            <IntlMessages id="button.reject" />
          </Button>
        </ModalFooter>
      </Modal>
    </Fragment>
  );
}

export default React.memo(injectIntl(Students));
