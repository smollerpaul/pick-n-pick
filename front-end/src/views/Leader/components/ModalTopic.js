import React, { Fragment, useState } from "react";
import { Card, CardBody, Label, CardImg, Button, Modal } from "reactstrap";
import { ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import moment from "moment";
import { get, isEmpty, orderBy } from "lodash";


function Students(props) {
    const { actions, isLoading, pagination, dataModalSpeaking, isModalSpeaking } = props;
    const { page, limit, totalPages } = pagination;
    const { messages } = props.intl;
    const { votings } = dataModalSpeaking

    const [isComleted, setisComleted] = useState(false)
    const [data, setData] = useState(Object)

    const onPageChange = (pageIndex) => {
        actions.fetchAllStudentClassSpeaking({ data: { ...pagination, page: pageIndex + 1 } });
    };
    const onPageSizeChange = (pageSize, pageIndex) => {
        actions.fetchAllStudentClassSpeaking({
            data: { ...pagination, page: pageIndex + 1, limit: pageSize },
        });
    };
    const dataTableColumns = [
        {
            Header: messages["user.avatar"],
            accessor: "studentId.avatar",
            className: "justify-center",
            width: 80,
            Cell: (props) => (
                <CardImg
                    top
                    src={
                        isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
                    }
                    alt="Avatar"
                />
            ),
        },
        {
            Header: "Name",
            accessor: "studentId.name",
            className: "justify-center",
            Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
        },
        {
            Header: "Date of Birth",
            accessor: "studentId.birthDate",
            className: "justify-center",
            width: 150,
            Cell: (props) => <p className="text-capitalize ">{moment(props.value).format("LL")}</p>,
        },
        {
            Header: 'Phone',
            accessor: "studentId.phone",
            className: "justify-center",
            width: 100,
            Cell: (props) => (
                <div className="d-flex justify-content-center">
                    <p>{props.value}</p>
                </div>
            ),
        },
        {
            Header: "Email",
            accessor: "studentId.email",
            className: "justify-center",
            Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
        },
        {
            Header: "Video",
            accessor: "video.linkVideo",
            className: "justify-center",
            width: 80,
            Cell: (props) => props.value ? <a className="text-reset" style={{ textDecoration: "underline" }} href={`${props.value}`} target="_blank"  >View</a> : <p className="text-capitalize" style={{color: "red", textDecoration: "underline"}}>NO</p>
        },
    ];
    return (
        <Fragment>
            <Modal
                isOpen={isModalSpeaking}
                size="xl"
                toggle={() => actions.closeModal()}
            >
                <ModalBody className="text-center">
                    <ReactTable
                        data={get(dataModalSpeaking, "votings", [])}
                        columns={dataTableColumns}
                        TbodyComponent={CustomTbodyComponent}
                        defaultPageSize={limit}
                        sortable={false}
                        filterable={false}
                        showPageJump={true}
                        PaginationComponent={DataTablePagination}
                        showPageSizeOptions={true}
                        pageSize={get(dataModalSpeaking, "votings", []).length + 1}
                        page={page - 1}
                        loading={isLoading}
                        pages={totalPages}
                        manual
                        onPageChange={(pageIndex) => onPageChange(pageIndex)}
                        onPageSizeChange={(pageSize, pageIndex) =>
                            onPageSizeChange(pageSize, pageIndex)
                        }
                        className="-striped -highlight h-full overflow-hidden"
                    />
                </ModalBody>
                <ModalFooter className="d-flex justify-content-center" >
                    <Button
                        outline
                        onClick={() => actions.closeModal()}
                    >
                        Ok
                    </Button>
                </ModalFooter>
            </Modal>
        </Fragment>
    );
}

export default React.memo(injectIntl(Students));
