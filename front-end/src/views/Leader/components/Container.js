import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Students from "./Students";
import { isEmpty } from "lodash";

class Container extends PureComponent {
  componentDidMount() {
    const {students,studentsPendding,classSpeakings, pagination, actions} = this.props
    if (isEmpty(studentsPendding)) {
      actions.fetchAllStudentsPendding({ data: {
        ...pagination,
        search: {
          usePagination: false,
          isExactly: true,
        }
      } });
    }
    if (isEmpty(students)) {
      actions.fetchAllStudents({ data: pagination });
    }
    if (isEmpty(classSpeakings)) {
      actions.fetchAllClassSpeaking({ data: pagination });
    }

  }

  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <div className="loading"></div>}
        <Students {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Container));
