import React, { Fragment, useState } from "react";
import { Card, CardBody, Label, CardImg, Button, Modal } from "reactstrap";
import { ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import moment from "moment";
import { get, isEmpty, orderBy } from "lodash";
import ModalTopic from "./ModalTopic"

function Students(props) {
    const { actions, isLoading, pagination, topics, } = props;
    const { page, limit, totalPages } = pagination;
    const { messages } = props.intl;

    const [isComleted, setisComleted] = useState(false)
    const [data, setData] = useState(Object)

    const onPageChange = (pageIndex) => {
        actions.fetchAllStudentClassSpeaking({ data: { ...pagination, page: pageIndex + 1 } });
    };
    const onPageSizeChange = (pageSize, pageIndex) => {
        actions.fetchAllStudentClassSpeaking({
            data: { ...pagination, page: pageIndex + 1, limit: pageSize },
        });
    };
    const dataTableColumns = [
        {
            Header: messages["user.avatar"],
            accessor: "speakerId.avatar",
            className: "justify-center",
            width: 80,
            Cell: (props) => (
                <CardImg
                    top
                    src={
                        isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
                    }
                    alt="Avatar"
                />
            ),
        },
        {
            Header: messages["speaking.name"],
            accessor: "speakerId.name",
            className: "justify-center",
            Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
        },
        {
            Header: 'Nationality',
            accessor: "speakerId.countryId.name",
            className: "justify-center",
            Cell: (props) => (
                <div className="d-flex justify-content-center">
                    <p>{props.value}</p>
                </div>
            ),
        },
        {
            Header: messages["speaking.topic"],
            accessor: "topic",
            className: "justify-center",
            Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
        },
        {
            Header: "Date & Time",
            accessor: "happenDate",
            className: "justify-center",
            width: 250,
            Cell: (props) => <p className="text-capitalize ">{moment(props.value).format("LLLL")}</p>,
        },
        {
            Header: messages["speaking.numVoting"],
            accessor: "numVotings",
            className: "justify-center",
            width: 80,
            Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
        },
        {
            Header: "Video",
            accessor: "videos",
            className: "justify-center",
            width: 80,
            Cell: (props) => (
                <div className="d-flex justify-content-center">
                    <p>{get(props, "value", "0")}</p>
                </div>
            ),
        },
        {
            Header: messages["user.isCompleted"],
            accessor: "isCompleted",
            className: "justify-center",
            Cell: ({original, value}) => (
                <div className="d-flex justify-content-center">
                    <Button
                    outline
                    color={value === true ? "success" : "info"}
                    onClick={(e) => {
                        e.stopPropagation()
                        setData({
                                ...original,
                                isCompleted : original.isCompleted ? false : true,
                        })
                        setisComleted(!isComleted)
                    }}>{messages[`user.isCompleted.${value}`]}</Button>
                </div>
            ),
        },
    ];
    return (
        <Fragment>
            <Card className="mb-4">
                <CardBody>
                    <ReactTable
                        data={orderBy(topics, "happenDate", "asc")}
                        columns={dataTableColumns}
                        TbodyComponent={CustomTbodyComponent}
                        defaultPageSize={limit}
                        sortable={false}
                        filterable={false}
                        showPageJump={true}
                        PaginationComponent={DataTablePagination}
                        showPageSizeOptions={true}
                        pageSize={topics.length + 1}
                        page={page - 1}
                        loading={isLoading}
                        pages={totalPages}
                        manual
                        onPageChange={(pageIndex) => onPageChange(pageIndex)}
                        onPageSizeChange={(pageSize, pageIndex) =>
                            onPageSizeChange(pageSize, pageIndex)
                        }
                        getTrProps={(state, rowInfo, column) => {
                            return {
                                className: "cursor-pointer",
                                onClick: () => actions.getInfo({data: rowInfo.original}),
                            };
                        }}
                        className="-striped -highlight h-full overflow-hidden"
                    />
                </CardBody>
            </Card>
            <Modal isOpen={isComleted}>
                <ModalBody className="text-center">
                    <Label className="h5" >
                    Are you sure to complete this topic ?
                    </Label>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-end" >
                    <Button
                        outline
                        onClick={() => setisComleted(!isComleted)}
                    >
                        <IntlMessages id="button.cancel" />
                    </Button>
                    <Button
                        color="danger"
                        onClick={() => {
                            console.log("data", data)
                            actions.handleComplete({ data: data  })
                            setisComleted(!isComleted)
                        }}
                    >
                        <IntlMessages id="button.confirm" />
                    </Button>
                </ModalFooter>
            </Modal>
            <ModalTopic {...props} />
        </Fragment>
    );
}

export default React.memo(injectIntl(Students));
