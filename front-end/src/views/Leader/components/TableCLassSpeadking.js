import React, { Fragment, useState } from "react";
import { Card, CardBody, Label, CardImg, Button, Modal } from "reactstrap";
import { ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import ReactTable from "react-table";
import { injectIntl } from "react-intl";
import IntlMessages from "helpers/IntlMessages";
import DataTablePagination from "theme/components/DatatablePagination";
import CustomTbodyComponent from "theme/components/CustomTbodyComponent";
import moment from "moment";
import { get, isEmpty, orderBy } from "lodash";
function getWeekDays(curr, firstDay = 1 /* 0=Sun, 1=Mon, ... */) {
  // var cd = curr.getDate() - curr.getDay();
  // var from = new Date(curr.setDate(cd + firstDay));
  // var to = new Date(curr.setDate(cd + 6 + firstDay));
  let start = new Date(moment(curr).startOf('week'))
  let end = new Date(moment(curr).endOf('week'))
  return {
    from: moment(start).add(1, 'days'),
    to: moment(end).add(1, 'days'),
  };
}


function Students(props) {
  const { actions, isLoading, pagination, classSpeakings, dataSpeaking } = props;
  const { votings } = dataSpeaking;
  const { page, limit, totalPages } = pagination;
  const { messages } = props.intl;

  const [modalCofirm, setModalCofirm] = useState(false);
  const [vote, setVote] = useState({ votings: [] });
  const [isConfirm, setIsConfirm] = useState(false);


  const onPageChange = (pageIndex) => {
    actions.fetchAllStudentClassSpeaking({ data: { ...pagination, page: pageIndex + 1 } });
  };
  const onPageSizeChange = (pageSize, pageIndex) => {
    actions.fetchAllStudentClassSpeaking({
      data: { ...pagination, page: pageIndex + 1, limit: pageSize },
    });
  };
  const dataTableColumns = [
    {
      Header: messages["user.avatar"],
      accessor: "speakerId.avatar",
      className: "justify-center",
      width: 80,
      Cell: (props) => (
        <CardImg
          top
          src={
            isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
          }
          alt="Avatar"
        />
      ),
    },
    {
      Header: messages["speaking.name"],
      accessor: "speakerId.name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: 'Nationality',
      accessor: "speakerId.countryId.name",
      className: "justify-center",
      Cell: (props) => (
        <div className="d-flex justify-content-center">
          <p>{props.value}</p>
        </div>
      ),
    },
    {
      Header: messages["speaking.topic"],
      accessor: "topic",
      className: "d-flex justify-content-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: messages["speaking.numVoting"],
      accessor: "numVotings",
      className: "d-flex justify-content-center",
      width: 150,
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: "Date & Time",
      accessor: "happenDate",
      className: "d-flex justify-content-center",
      width: 250,
      Cell: (props) => <p className="text-capitalize ">{moment(props.value).format("LLLL")}</p>,
    },
    {
      Header: messages["user.isCofirmed"],
      accessor: "isConfirmed",
      className: "d-flex justify-content-center",
      Cell: (props) => (
        <Button color={props.value === true ? "success" : "primary"} size="small" className="d-flex justify-content-center">
          <IntlMessages id={messages[`user.isConfirmed.${props.value}`]} />
        </Button>
      ),
    },
    // {
    //   Header: messages["user.isCompleted"],
    //   accessor: "isCompleted",
    //   className: "d-flex justify-content-center",
    //   Cell: (props) => (
    //     <Button color={props.value === true ? "success" : "info"} size="small" outline className="d-flex justify-content-center">
    //       <IntlMessages id={messages[`user.isCompleted.${props.value}`]} />
    //     </Button>
    //   ),
    // },
  ];

  const dataTableModalColumns = [
    {
      Header: "",
      accessor: "studentId.avatar",
      className: "justify-center",
      width: 80,
      Cell: (props) => (
        <CardImg
          top
          src={
            isEmpty(get(props, "value", "")) ? "/assets/img/avatar.png" : get(props, "value", "")
          }
          alt="Avatar"
        />
      ),
    },
    {
      Header: messages["speaking.student"],
      accessor: "studentId.name",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: messages["speaking.topic"],
      accessor: "studentId.email",
      className: "justify-center",
      Cell: (props) => <p className="text-capitalize ">{props.value}</p>,
    },
    {
      Header: messages["speaking.phone"],
      accessor: "studentId.englishLevel",
      className: "justify-center",
      Cell: (props) => (
        <p className="text-capitalize ">{messages[`user.englishLevel.${props.value}`]}</p>
      ),
    },
  ];

  // .setHours(0, 0, 0, 0)

  const today = new Date(new Date().setHours(0, 0, 0, 0));
  const weeks = [
    getWeekDays(new Date(today)),
    getWeekDays(new Date(today.setDate(today.getDate() + 7))),
    getWeekDays(new Date(today.setDate(today.getDate() + 7))),
    getWeekDays(new Date(today.setDate(today.getDate() + 7))),
  ];

  console.log("today", weeks)

  const dataModalOption = get(vote, "votings", []).filter(o => o.studentId !== null)
  return (
    <Fragment>
      {weeks.map((o) => {
        const option = orderBy(classSpeakings, "numVotings", "desc").filter((p) => {
          let date = new Date(p.happenDate);
          return date >= new Date(o.from) && date <= new Date(o.to);
        })
        const isHave = option.find(o => o.isConfirmed === true)
        return (
          <Fragment>
            <Card className="mb-4">
              <CardBody>
                <h5>
                  <IntlMessages id="speaking.from" /> {moment(o.from).format("DD/MM")}{" "}
                  <IntlMessages id="speaking.to" /> {moment(o.to).format("DD/MM")}
                </h5>
                <ReactTable
                  data={isHave ? [isHave] : option}
                  columns={dataTableColumns}
                  TbodyComponent={CustomTbodyComponent}
                  defaultPageSize={limit}
                  sortable={false}
                  filterable={false}
                  showPageJump={true}
                  PaginationComponent={DataTablePagination}
                  showPageSizeOptions={true}
                  pageSize={option.length + 1}
                  page={page - 1}
                  loading={isLoading}
                  pages={totalPages}
                  manual
                  onPageChange={(pageIndex) => onPageChange(pageIndex)}
                  onPageSizeChange={(pageSize, pageIndex) => onPageSizeChange(pageSize, pageIndex)}
                  getTrProps={(state, rowInfo, column) => {
                    return {
                      className: "cursor-pointer",
                      onClick: () => {
                        setVote(rowInfo.original);
                        setModalCofirm(!modalCofirm);
                      },
                    };
                  }}
                  className="-striped -highlight h-full overflow-hidden"
                />
              </CardBody>
            </Card>
          </Fragment>
        )
      })}
      <Modal isOpen={modalCofirm} size="xl">
        <ModalBody>
          <ModalHeader>
            <Label>
              <IntlMessages id="speaking.votings" />
            </Label>
          </ModalHeader>
          <ReactTable
            data={dataModalOption}
            columns={dataTableModalColumns}
            TbodyComponent={CustomTbodyComponent}
            defaultPageSize={limit}
            sortable={false}
            filterable={false}
            showPageJump={true}
            PaginationComponent={DataTablePagination}
            showPageSizeOptions={true}
            pageSize={dataModalOption.length + 1}
            className="-striped -highlight h-full overflow-hidden"
          />
        </ModalBody>
          {vote.isConfirmed ? 
          <ModalFooter className="d-flex justify-content-center">
          <Button
            color="info"
            outline
            onClick={() => {
              setModalCofirm(!modalCofirm);
              actions.clear();
            }}
          >
            Ok
          </Button>
          </ModalFooter> : (
              <ModalFooter className="d-flex justify-content-end">
                <Button
                  color="info"
                  outline
                  onClick={() => {
                    setModalCofirm(!modalCofirm);
                    actions.clear();
                  }}
                >
                  <IntlMessages id="button.cancel" />
                </Button>
                <Button
                  color="danger"
                  // disabled={vote.isConfirmed}
                  onClick={() => {
                    setIsConfirm(!isConfirm)
                    setModalCofirm(!modalCofirm);
                  }}
                >
                  <IntlMessages id="button.confirm" />
                </Button>
              </ModalFooter>
            )
          }
      </Modal>
      <Modal isOpen={isConfirm}>
        <ModalBody className="text-center">
          <Label className="h5" >
            Are you sure to confirm this class.
          </Label>
          <Label className="h5" >
          You can not undo.
          </Label>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-end" >
          <Button
            outline
            onClick={() => setIsConfirm(!isConfirm)}
          >
            <IntlMessages id="button.cancel" />
          </Button>
          <Button
            color="danger"
            onClick={() => {
              actions.confirmClassSpeaking({ data: vote })
              setIsConfirm(!isConfirm)
            }}
          >
            <IntlMessages id="button.confirm" />
          </Button>
        </ModalFooter>
      </Modal>
    </Fragment>
  );
}

export default React.memo(injectIntl(Students));
