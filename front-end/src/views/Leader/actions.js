/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const handleInputChange = createAction("LEADERS/HANDLE_INPUT_CHANGE");

export const handleToggleModal = createAction("LEADERS/HANDLE_TOGGLE_MODAL");

export const fetchAllStudents = createAction("LEADERS/FETCH_ALL_STUDENTS");
export const fetchAllStudentsSuccess = createAction("LEADERS/FETCH_ALL_STUDENTS_SUCCESS");

export const getInfo = createAction("LEADERS/GET_INFO");
export const getInfoSuccess = createAction("LEADERS/ GET_INFO_SUCCESS");

export const createNewStudents = createAction("LEADERS/CREATE_NEW_STUDENTS");
export const createNewStudentsSuccess = createAction("LEADERS/CREATE_NEW_STUDENTS_SUCCESS");

export const editStudents = createAction("LEADERS/EDIT_STUDENTS");
export const editStudentsSuccess = createAction("LEADERS/EDIT_STUDENTS_SUCCESS");

export const updateTask = createAction("LEADERS/UPDATE_TASK_STUDENTS");
export const updateTaskSuccess = createAction("LEADERS/UPDATE_TASK_STUDENTS_SUCCESS");

export const deleteStudents = createAction("LEADERS/DELETE_STUDENTS");
export const deleteStudentsSuccess = createAction("LEADERS/DELETE_STUDENTS_SUCCESS");

export const addStudentInclass = createAction("LEADERS/ADD_STUDENT_IN_CLASS");
export const addStudentInclassSuccess = createAction("LEADERS/ADD_STUDENT_IN_CLASS_SUCCESS");

export const removeStudentInclass = createAction("LEADERS/REMOVE_STUDENT_IN_CLASS");
export const removeStudentInclassSuccess = createAction("LEADERS/REMOVE_STUDENT_IN_CLASS_SUCCESS");

export const handleSearch = createAction("LEADERS/HANDLE_SEARCH");
export const changeTabs = createAction("LEADERS/CHANGE_TABS");

export const fetchAllStudentsPendding = createAction("LEADERS/STUDENTS_PEDDING");
export const fetchAllStudentsPenddingSuccess = createAction("LEADERS/STUDENTS_PEDDING_SUCCESS");

export const handleStudentsPendding = createAction("LEADERS/HANDLE_STUDENTS_PEDDING");
export const handleStudentsPenddingSuccess = createAction("LEADERS/HANDLE_STUDENTS_PEDDING_SUCCESS");

export const fetchAllClassSpeaking = createAction("LEADERS/FETCH_ALL_CLASS_SPEAKING");
export const fetchAllClassSpeakingSuccess = createAction("LEADERS/FETCH_ALL_CLASS_SPEAKING_SUCCESS");

export const fetchAllStudentClassSpeaking = createAction("LEADERS/FETCH_ALL_STUDENT_CLASS_SPEAKING");
export const fetchAllStudentClassSpeakingSuccess = createAction("LEADERS/FETCH_ALL_STUDENT_CLASS_SPEAKING_SUCCESS");

export const confirmClassSpeaking = createAction("LEADERS/COFIRM_CLASS_SPEAKING");
export const confirmClassSpeakingSuccess = createAction("LEADERS/COFIRM_CLASS_SPEAKING_SUCCESS");

export const fetchAllTopic = createAction("LEADERS/FETCH_ALL_TOPIC");
export const fetchAllTopicSuccess = createAction("LEADERS/FETCH_ALL_TOPIC_SUCCESS");

export const handleComplete = createAction("LEADERS/HANDLE_COMPLETE");
export const handleCompleteSuccess = createAction("LEADERS/HANDLE_COMPLETE_SUCCESS");

export const closeModal = createAction("LEADERS/CLOSE_MODAL");

export const clear = createAction("LEADERS/CLEAR");
