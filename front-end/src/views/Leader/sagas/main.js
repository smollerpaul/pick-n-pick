import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "apis/students";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "theme/components/common/react-notifications";

export function* handleFetchAllStudents(action) {
  try {
    const res = yield call(API.fetchAllStudents, action.payload.data);
    yield put(actions.fetchAllStudentsSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllStudentsPendding(action) {
  try {
    const res = yield call(API.fetchAllStudentsPendding, action.payload.data);
    yield put(actions.fetchAllStudentsPenddingSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleGetInfo(action) {
  try {
    const res = yield call(API.getInfo, action.payload.data);
    yield put(actions.getInfoSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlecreateNewStudents(action) {
  try {
    const res = yield call(API.createNewStudents, action.payload.data);
    yield put(actions.createNewStudentsSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleEditStudents(action) {
  try {
    const res = yield call(API.editStudents, action.payload.data);
    yield put(actions.editStudentsSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleDeleteStudents(action) {
  try {
    const res = yield call(API.deleteStudents, action.payload.data);
    yield put(actions.deleteStudentsSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlehandleStudentsPendding(action) {
  try {
    const res = yield call(API.handleStudentsPendding, action.payload.data);
    yield put(actions.handleStudentsPenddingSuccess(res));
    yield put(actions.fetchAllStudentsPendding({data: {
      search: {
        usePagination: false,
        isExactly: true,
      }
    }}));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleupdateTask(action) {
  try {
    const {data} = action.payload;
    for (let item of data) yield put(actions.editStudents({data: item}))
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllClassSpeaking(action) {
  try {
    const res = yield call(API.fetchAllClassSpeaking, action.payload.data);
    yield put(actions.fetchAllClassSpeakingSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllStudentClassSpeaking(action) {
  try {
    const res = yield call(API.fetchAllStudentClassSpeaking, action.payload.data);
    yield put(actions.fetchAllStudentClassSpeakingSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handleconfirmClassSpeaking(action) {
  try {
    const res = yield call(API.confirmClassSpeaking, action.payload.data);
    yield put(actions.confirmClassSpeakingSuccess(res));
    NotificationManager.success("Confirm success")
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlefetchAllTopic(action) {
  try {
    const res = yield call(API.fetchAllTopic, action.payload.data);
    yield put(actions.fetchAllTopicSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}

export function* handlehandleComplete(action) {
  try {
    const res = yield call(API.handleComplete, action.payload.data);
    yield put(actions.handleCompleteSuccess(res));
  } catch (err) {
    NotificationManager.error("Errors", err);
  }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function* fetchAllStudents() {
  yield takeAction(actions.fetchAllStudents, handleFetchAllStudents);
}

export function* fetchAllStudentsPendding() {
  yield takeAction(actions.fetchAllStudentsPendding, handlefetchAllStudentsPendding);
}

export function* getInfo() {
  yield takeAction(actions.getInfo, handleGetInfo);
}

export function* createNewStudents() {
  yield takeAction(actions.createNewStudents, handlecreateNewStudents);
}

export function* editStudents() {
  yield takeEvery(actions.editStudents, handleEditStudents);
}

export function* deleteStudents() {
  yield takeAction(actions.deleteStudents, handleDeleteStudents);
}

export function* handleStudentsPendding() {
  yield takeAction(actions.handleStudentsPendding, handlehandleStudentsPendding);
}

export function* updateTask() {
  yield takeAction(actions.updateTask, handleupdateTask);
}

export function* fetchAllClassSpeaking() {
  yield takeAction(actions.fetchAllClassSpeaking, handlefetchAllClassSpeaking);
}

export function* fetchAllStudentClassSpeaking() {
  yield takeAction(actions.fetchAllStudentClassSpeaking, handlefetchAllStudentClassSpeaking);
}

export function* confirmClassSpeaking() {
  yield takeAction(actions.confirmClassSpeaking, handleconfirmClassSpeaking);
}

export function* fetchAllTopic() {
  yield takeAction(actions.fetchAllTopic, handlefetchAllTopic);
}


export function* handleComplete() {
  yield takeAction(actions.handleComplete, handlehandleComplete);
}

export default [
  fetchAllStudents,
  fetchAllStudentsPendding,
  createNewStudents,
  editStudents,
  deleteStudents,
  getInfo,
  updateTask,
  handleStudentsPendding,
  fetchAllClassSpeaking,
  fetchAllStudentClassSpeaking,
  confirmClassSpeaking,
  fetchAllTopic,
  handleComplete
];
