/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "constants/defaultValues";

export const name = "leaders";

const initialState = freeze({
  data: {
    name: " ",
    rejectReason: " "
  },
  modalAvatar: false,
  tabs: "pendding",
  message: "",
  students: [],
  studentsPendding: [],
  studentsInClass: [],
  classSpeakings: [],
  topics: [],
  isOpen: false,
  isLoading: false,
  listProcessing: [],
  dataSpeaking: {},
  dataModalSpeaking: {},
  isModalSpeaking: false,
  viewSpeaking: false,
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageSize,
    search: {
      usePagination: true,
      isExactly: false,
    },
  },
});

export default handleActions(
  {
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value,
          },
        },
      });
    },
    [actions.fetchAllStudents]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        pagination: {
          ...state.pagination,
          ...action.payload.data,
        },
      });
    },
    [actions.handleInputChange]: (state, action) => {
      const event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      return freeze({
        ...state,
        message: initialState.message,
        data: {
          ...state.data,
          [name]: value,
        },
      });
    },
    [actions.fetchAllStudentsSuccess]: (state, action) => {
      return freeze({
        ...state,
        students: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
     [actions.handleToggleModal]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen,
      });
    },
    [actions.createNewStudentsSuccess]: (state, action) => {
      return freeze({
        ...state,
        students: [action.payload.data, ...state.students],
        data: [],
        isOpen: false,
      });
    },
    [actions.handleSearch]: (state, action) => {
      const { target } = action.payload;
      const { name, value } = target;
      return freeze({
        ...state,
        pagination: {
          ...state.pagination,
          search: {
            ...state.pagination.search,
            [name]: value
          }
        }
      });
    },
    [actions.changeTabs]: (state, action) => {
      return freeze({
        ...state,
        tabs: action.payload,
      });
    },
    [actions.getInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isModalSpeaking: true,
        dataModalSpeaking: action.payload.data,
      });
    },
    [actions.editStudentsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        students: [ ...state.students.map(item => {
          if (item._id === action.payload.data._id) {
            return action.payload.data;
          }
          return item;
        })]
      });
    },
    [actions.deleteStudentsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        data: initialState.data,
        students: [...state.students.filter(o => o._id !== action.payload.data)]
      });
    },
    [actions.addStudentInclassSuccess]: (state, action) => {
      return freeze({
        ...state,
        studentsInClass: [...state.studentsInClass, action.payload.data._id]
      });
    },
    [actions.fetchAllStudentsPenddingSuccess]: (state, action) => {
      return freeze({
        ...state,
        studentsPendding: action.payload.data,
        pagination: {
          ...state.pagination,
          ...action.payload.pagination,
        },
        isLoading: false,
      });
    },
    [actions.handleStudentsPenddingSuccess]: (state, action) => {
      return freeze({
        ...state,
        listProcessing: [...state.listProcessing,action.payload.data.interviewId],
        data: [],
      });
    },
    [actions.fetchAllClassSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        classSpeakings: action.payload.data,
        data: [],
      });
    },
    [actions.fetchAllStudentClassSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        dataSpeaking: action.payload.data,
        data: [],
      });
    },
    [actions.clear]: (state, action) => {
      return freeze({
        ...state,
        data: {},
      });
    },
    [actions.confirmClassSpeakingSuccess]: (state, action) => {
      return freeze({
        ...state,
        isEdit: false,
        isOpen:false,
        classSpeakings: [ ...state.classSpeakings.map(item => {
          if (item._id === action.payload.data._id) {
            return {...item, isConfirmed: action.payload.data.isConfirmed};
          }
          return item;
        })]
      });
    },
    [actions.fetchAllTopicSuccess]: (state, action) => {
      return freeze({
        ...state,
        topics: action.payload.data,
      });
    },
    [actions.closeModal]: (state, action) => {
      return freeze({
        ...state,
        isModalSpeaking: false,
      });
    },
    [actions.handleCompleteSuccess]: (state, action) => {
      return freeze({
        ...state,
        topics: state.topics.map(o => {
          if(o._id === action.payload.data._id) return {...o, isCompleted: action.payload.data.isCompleted}
          return o
        }),
      });
    },
  },
  initialState
);
