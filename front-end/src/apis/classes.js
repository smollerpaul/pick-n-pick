import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}/leaders/universities/classes`;

export function fetchAllClasses(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET", data);
}

export function createNewClasses(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function editClasses(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteClasses(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "DELETE");
}

export function getInfo(data) {
  const endpoint = `${baseEndpoint}/${data}`;
  return request(endpoint, "GET");
}

export function changeSearch(data) {
  const endpoint = `${config.apiBaseURL}/areas`;
  return request(endpoint, "GET", data);
}

export function searchInfo(data) {
  const endpoint = `${config.apiBaseURL}/${data.type}/all`;
  return request(endpoint, "GET", data);
}

export function fetchAllStudents(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/students`;
  return request(endpoint, "GET", data);
}

export function addStudentInclass(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/students`;
  return request(endpoint, "POST", data);
}

export function promveStudtents(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/students/specificRole`;
  return request(endpoint, "POST", data);
}

export function removeStudentInclass(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/students/waiting/${data}`;
  return request(endpoint, "DELETE");
}

export function changeUniversity(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/students/requestAdminAction/${data._id}`;
  return request(endpoint, "POST", data);
}

export function fetchClasses(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/speakings/${data._id}`;
  return request(endpoint, "GET");
}
