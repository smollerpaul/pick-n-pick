import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}/students/speakings`;

export function fetchAllClasses(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET", data);
}

export function fetchAllClassesLeader(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings`;
  return request(endpoint, "GET");
}

export function createNewClasses(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function editClasses(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteClasses(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "DELETE");
}

export function getInfo(data) {
  const endpoint = `${baseEndpoint}/${data}`;
  return request(endpoint, "GET");
}

export function changeSearch(data) {
  const endpoint = `${config.apiBaseURL}/areas`;
  return request(endpoint, "GET", data);
}

export function searchInfo(data) {
  const endpoint = `${config.apiBaseURL}/${data.type}/all`;
  return request(endpoint, "GET", data);
}

export function fetchAllStudents(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/students/`;
  return request(endpoint, "GET", data);
}

export function addStudentInclass(data) {
  const endpoint = `${config.apiBaseURL}/leaders/universities/students`;
  return request(endpoint, "POST", data);
}

export function joinClass(data) {
  const endpoint = `${config.apiBaseURL}/students/speakings/${data}`;
  return request(endpoint, "POST");
}

export function unJoinClass(data) {
  const endpoint = `${config.apiBaseURL}/students/speakings/${data._id}`;
  return request(endpoint, "DELETE");
}

export function confirmClass(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings/${data.speakingId}`;
  return request(endpoint, "POST");
}

export function fetchTopic(data) {
  const endpoint = `${config.apiBaseURL}/students/videos/speakings/${data._id}`;
  return request(endpoint, "GET");
}

export function fetchAllSpeakingConfirm(data) {
  const endpoint = `${config.apiBaseURL}/students/speakings/confirmed`;
  return request(endpoint, "GET", data);
}

export function uploadClip(data) {
  const endpoint = `${config.apiBaseURL}/students/videos/`;
  return request(endpoint, "POST", data);
}

export function checkValidate(data) {
  const endpoint = `${config.apiBaseURL}/students/me/verify`;
  return request(endpoint, "POST",data);
}

export function resendEmail(data) {
  const endpoint = `${config.apiBaseURL}/students/me/resend`;
  return request(endpoint, "POST");
}