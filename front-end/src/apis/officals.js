import config from "../config";
import { request } from "../services/api";

const baseEndpointAdmin = `${config.apiBaseURL}/admins/officials`;

export function fetchAllOfficals(data) {
  const endpoint = `${baseEndpointAdmin}`;
  return request(endpoint, "GET", data);
}

export function createOfficals(data) {
  const endpoint = `${baseEndpointAdmin}/`;
  return request(endpoint, "POST", data.data);
}

export function editOfficals(data) {
  const endpoint = `${baseEndpointAdmin}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteOfficals(data) {
  const endpoint = `${baseEndpointAdmin}/${data._id}`;
  return request(endpoint, "DELETE");
}

export function fetchOffical(data) {
  const endpoint = `${baseEndpointAdmin}/${data._id}`;
  return request(endpoint, "GET");
}
