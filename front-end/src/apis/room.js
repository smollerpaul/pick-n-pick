import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}`;

export function getInfo(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET", data);
}

export function speakerRoom(data) {
  const endPoint = `${baseEndpoint}/speakers/join/`;
  return request(endPoint, "GET", { token: data });
}

export function studentRoom(data) {
  const endPoint = `${baseEndpoint}/students/speakings/join/${data}`;
  return request(endPoint, "GET");
}