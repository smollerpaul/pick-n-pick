import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}/admins`;

export function fetchAllUniversity(data) {
  const endpoint = `${baseEndpoint}/universities`;
  return request(endpoint, "GET", data);
}

export function createNewUniversity(data) {
  const endpoint = `${baseEndpoint}/universities`;
  return request(endpoint, "POST", data);
}

export function editUniversity(data) {
  const endpoint = `${baseEndpoint}/universities/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteUniversity(data) {
  const endpoint = `${baseEndpoint}/universities/${data._id}`;
  return request(endpoint, "DELETE");
}

export function getInfo(data) {
  const endpoint = `${baseEndpoint}/universities/${data._id}`;
  return request(endpoint, "GET");
}

export function changeSearch(data) {
  const endpoint = `${config.apiBaseURL}/areas`;
  return request(endpoint, "GET", data);
}

export function searchInfo(data) {
  const endpoint = `${config.apiBaseURL}/${data.type}/all`;
  return request(endpoint, "GET", data)
}

export function fetchAllCountries(data) {
  const endPoint = `${config.apiBaseURL}/countries`;
  return request(endPoint, "GET");
}

export function fetchAllStudent(data) {
  const endpoint = `${baseEndpoint}/students/`;
  return request(endpoint, "GET", data);
}

export function createNewStudent(data) {
  const endpoint = `${baseEndpoint}/students/`;
  return request(endpoint, "POST", data);
}

export function editStudent(data) {
  const endpoint = `${baseEndpoint}/students/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteStudent(data) {
  const endpoint = `${baseEndpoint}/students/${data._id}`;
  return request(endpoint, "DELETE");
}

export function getInfoStudent(data) {
  const endpoint = `${baseEndpoint}/students/${data._id}`;
  return request(endpoint, "GET");
}

export function assignLeader(data) {
  const endpoint = `${baseEndpoint}/students/specificRole`;
  return request(endpoint, "POST", data);
}

export function fetchAllStudentChangeUniversity(data) {
  const endpoint = `${baseEndpoint}/students`;
  return request(endpoint, "GET", data);
}

export function changeUniversity(data) {
  const endpoint = `${baseEndpoint}/students/changeUniversity/${data._id}`;
  return request(endpoint, "POST", data);
}