import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}/leaders/classes/students`;

export function fetchAllStudents(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET", data);
}

export function createNewStudents(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function editStudents(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteStudents(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "DELETE");
}

export function changeSearch(data) {
  const endpoint = `${config.apiBaseURL}/areas`;
  return request(endpoint, "GET", data);
}

export function searchInfo(data) {
  const endpoint = `${config.apiBaseURL}/${data.type}/all`;
  return request(endpoint, "GET", data)
}

export function fetchAllStudentsPendding(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/students/waitingForInterview`;
  return request(endpoint, "GET", data)
}

export function handleStudentsPendding(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/students/interviewResult`;
  return request(endpoint, "POST", data)
}

export function fetchAllClassSpeaking(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings`;
  return request(endpoint, "GET", data)
}

export function fetchAllStudentClassSpeaking(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings`;
  return request(endpoint, "GET", data)
}

export function confirmClassSpeaking(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings/${data._id}`;
  return request(endpoint, "POST", data)
}

export function fetchAllTopic(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings/teamSpeakings`;
  return request(endpoint, "GET")
}

export function handleComplete(data) {
  console.log("usser",data)
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings/teamSpeakings/${data._id}`;
  return request(endpoint, "PUT", data)
}

export function getInfo(data) {
  const endpoint = `${config.apiBaseURL}/leaders/classes/speakings/${data._id}`;
  return request(endpoint, "GET");
}