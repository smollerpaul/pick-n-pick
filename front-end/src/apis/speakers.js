import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}/admins/speakers`;

export function fetchAllSpeakers(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET", data);
}

export function createNewSpeakers(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function editSpeakers(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteSpeakers(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "DELETE");
}

export function getInfo(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "GET");
}

export function changeSearch(data) {
  const endpoint = `${config.apiBaseURL}/areas`;
  return request(endpoint, "GET", data);
}

export function searchInfo(data) {
  const endpoint = `${config.apiBaseURL}/${data.type}/all`;
  return request(endpoint, "GET", data)
}

export function fetchAllCountries(data) {
  const endPoint = `${config.apiBaseURL}/countries`;
  return request(endPoint, "GET");
}

export function fetchAllSRMs(data) {
  const endPoint = `${config.apiBaseURL}/admins/srms`;
  return request(endPoint, "GET");
}

export function createNewCountries(data) {
  const endPoint = `${config.apiBaseURL}/admins/countries`;
  return request(endPoint, "POST", data);
}