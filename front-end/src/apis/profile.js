import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/users/role`;

export function fetchAllCountries(data) {
  const endPoint = `${baseEndpoint}/countries`;
  return request(endPoint, "GET");
}

export function getInfo(data) {
  const endPoint = `${baseEndpoint}/`;
  return request(endPoint, "GET");
}

export function updateUser(data) {
  const endPoint = `${config.apiBaseURL}/users`;
  return request(endPoint, "PUT", data);
}
