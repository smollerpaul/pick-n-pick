import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}`;
const baseEndpointNew = `${config.apiBaseURL}/todoLists/new`;

export function getFolder(data) {
  const endPoint = `${baseEndpoint}/todoLists`;
  return request(endPoint, "GET", data);
}

export function createFolder(data) {
  const endPoint = `${baseEndpoint}/todoLists`;
  return request(endPoint, "POST", data);
}

export function updateFolder(data) {
  const endPoint = `${baseEndpoint}/todoLists/${data.id}`;
  return request(endPoint, "PUT", data.data);
}

export function delFolder(data) {
  const endPoint = `${baseEndpoint}/todoLists/${data}`;
  return request(endPoint, "DELETE");
}

export function getTask(data) {
  const endPoint = `${baseEndpoint}/todoLists/${data}/tasks`;
  return request(endPoint, "GET");
}

export function createTask(data) {
  const endPoint = `${baseEndpoint}/todoLists/${data.id}/tasks`;
  let newTask = data.data;
  return request(endPoint, "POST", newTask);
}

export function updateTask(data) {
  const endPoint = `${baseEndpoint}/todoLists/${data.listId}/tasks/${data.taskId}`;
  return request(endPoint, "PUT", data.data);
}

export function updateStatusTask(data) {
  const endPoint = `${baseEndpoint}/todoLists/${data.listId}/tasks/${data.taskId}/status`;
  return request(endPoint, "PUT", data.data);
}

export function delTask(data) {
  const endPoint = `${baseEndpoint}/todoLists/${data.listId}/tasks/${data.taskId}`;
  return request(endPoint, "DELETE");
}

export const getCalendar = data => {
  let { month, year } = data;
  const endPoint = `${baseEndpoint}/todoLists/calendars?month=${month}&year=${year}`;
  return request(endPoint, "GET");
};


export const fetchAllTodoList = data => {
  const endPoint = `${baseEndpointNew}`;
  return request(endPoint, "GET", data);
}

export const fetchTodoListDetail = data => {
  const endPoint = `${baseEndpointNew}/${data.id}`;
  return request(endPoint, "GET");
}

export const getTaskDetail = data => {
  const endPoint = `${baseEndpointNew}/${data.listId}/${data.id}`;
  return request(endPoint, "GET");
}


export function createChecklist(data) {
  const endPoint = `${baseEndpointNew}/${data.projectId}/${data.id}/checklist`;
  return request(endPoint, "POST", data);
}

export function editChecklist(data) {
  const endPoint = `${baseEndpointNew}/${data.projectId}/${data.id}/checklist/${data.checkListId}`;
  return request(endPoint, "PUT", data);
}

export function deleteChecklist(data) {
  const endPoint = `${baseEndpointNew}/${data.projectId}/${data.id}/checklist/${data.checkListId}`;
  return request(endPoint, "DELETE");
}

export function createComment(data) {
  const endPoint = `${baseEndpointNew}/${data.projectId}/${data.id}/comments`;
  return request(endPoint, "POST", data);
}

export function editComment(data) {
  const endPoint = `${baseEndpointNew}/${data.projectId}/${data.id}/comments/${data.commentId}`;
  return request(endPoint, "PUT", data);
}

export function deleteComment(data) {
  const endPoint = `${baseEndpointNew}/${data.projectId}/${data.id}/comments/${data.commentId}`;
  return request(endPoint, "DELETE");
}


export function createTasks(data) {
  const endPoint = `${config.apiBaseURL}/todoLists/${data.projectId}/tasks`;
  return request(endPoint, "POST", data);
}

export function updateTasks(data) {
  const endPoint = `${config.apiBaseURL}/todoLists/${data.projectId}/tasks/${data.id}`;
  return request(endPoint, "PUT", data);
}

export function deleteTasks(data) {
  const endPoint = `${config.apiBaseURL}/todoLists/${data.listId}/tasks/${data.id}`;
  return request(endPoint, "DELETE");
}
