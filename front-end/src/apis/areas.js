import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}/areas`;
const baseEndpointAdmin = `${config.apiBaseURL}/admins/areas`;

export function fetchAllAreas(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET", data);
}

export function createAreas(data) {
  const endpoint = `${baseEndpointAdmin}/`;
  return request(endpoint, "POST", data.data);
}

export function editAreas(data) {
  const endpoint = `${baseEndpointAdmin}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteAreas(data) {
  const endpoint = `${baseEndpointAdmin}/${data._id}`;
  return request(endpoint, "DELETE");
}

export function fetchArea(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "GET");
}
