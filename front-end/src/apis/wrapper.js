import config from "../config";
import { request} from "services/api";

const baseEndpoint = `${config.apiBaseURL}/officials`;

export function getUserInfo() {
  const endPoint = `${config.apiBaseURL}/users/role`;
  return request(endPoint, "GET");
}

export function updateUserInfo(data) {
  const endPoint = `${baseEndpoint}`;
  return request(endPoint, "PUT", data);
}
