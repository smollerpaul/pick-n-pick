import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}/srms`;

export function fetchAllSpeaker(data) {
  const endpoint = `${baseEndpoint}/speakers`;
  return request(endpoint, "GET", data);
}

export function createNewSpeaker(data) {
  const endpoint = `${baseEndpoint}/speakers`;
  return request(endpoint, "POST", data);
}

export function editSpeaker(data) {
  const endpoint = `${baseEndpoint}/speakers/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteSpeaker(data) {
  const endpoint = `${baseEndpoint}/speakers/${data._id}`;
  return request(endpoint, "DELETE");
}

export function getInfo(data) {
  const endpoint = `${baseEndpoint}/speakers/${data._id}`;
  return request(endpoint, "GET");
}

export function changeSearch(data) {
  const endpoint = `${config.apiBaseURL}/areas`;
  return request(endpoint, "GET", data);
}

export function searchInfo(data) {
  const endpoint = `${config.apiBaseURL}/${data.type}/all`;
  return request(endpoint, "GET", data)
}

export function fetchAllCountries(data) {
  const endPoint = `${config.apiBaseURL}/countries`;
  return request(endPoint, "GET");
}

export function fetchAllSpeaking(data) {
  const endpoint = `${baseEndpoint}/speakings/`;
  return request(endpoint, "GET", data);
}

export function createNewSpeaking(data) {
  const endpoint = `${baseEndpoint}/speakings/`;
  return request(endpoint, "POST", data);
}

export function editSpeaking(data) {
  const endpoint = `${baseEndpoint}/speakings/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function deleteSpeaking(data) {
  const endpoint = `${baseEndpoint}/speakings/${data._id}`;
  return request(endpoint, "DELETE");
}

export function getInfoSpeaking(data) {
  const endpoint = `${baseEndpoint}/speakings/${data._id}`;
  return request(endpoint, "GET");
}