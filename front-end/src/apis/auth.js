import config from "../config";
import axios from "axios";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}`;

export function facebookLogin(data) {
  const endPoint = `${baseEndpoint}/users/login/facebook`;
  return axios({ method: "POST", url: endPoint, data });
}

export function googleLogin(data) {
  const endPoint = `${baseEndpoint}/users/login/google`;
  return axios({ method: "POST", url: endPoint, data });
}

export function adminLogin(data) {
  const endPoint = `${baseEndpoint}/admin/users/login`;
  return axios({ method: "POST", url: endPoint, data });
}

export function adminNLPLogin(data) {
  const endPoint = `${config.apiNLPBaseURL}/users/login`;
  return axios({ method: "POST", url: endPoint, data });
}

export function fetchAllCountries(data) {
  const endPoint = `${baseEndpoint}/countries`;
  return request(endPoint, "GET");
}

export function fetchAllUniversities(data) {
  const endPoint = `${baseEndpoint}/universities`;
  return request(endPoint, "GET");
}

export function createUser(data) {
  const endPoint = `${baseEndpoint}/students/register`;
  return request(endPoint, "POST",data);
}

export function userLogin(data) {
  const endPoint = `${baseEndpoint}/auth/login`;
  return request(endPoint, "POST",data);
}
