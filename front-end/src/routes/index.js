import React, { Component, Suspense, lazy } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import Wrapper from "views/Wrapper/components/WrapperContainer";
import AppLayout from "theme/layout/WelcomeLayout";

const Classes = lazy(() => import("./classes"));
class App extends Component {
  render() {
    return (
      <Wrapper>
        <AppLayout {...this.props}>
          <Suspense fallback={<div className="loading" />}>
            <Switch>
              <Route path={`/`} component={Classes} />
              <Redirect to="/error" />
            </Switch>
          </Suspense>
        </AppLayout>
      </Wrapper>
    );
  }
}

export default withRouter(App);
