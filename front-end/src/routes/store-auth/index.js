import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import UserLayout from "theme/layout/UserLayout";
import LoginStores from "views/LoginStores/components/LoginUserContainer";
import Register from "views/Register/components/Container";

const User = ({ match }) => {
  return (
    <UserLayout>
      <Switch>
        <Redirect exact from={`${match.url}/`} to={`${match.url}/login`} />
        <Route path={`${match.url}/login`} component={LoginStores} />
        <Route path={`${match.url}/register`} component={Register} />
        <Redirect to="/error" />
      </Switch>
    </UserLayout>
  );
};

export default User;
