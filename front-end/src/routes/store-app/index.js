import React, { Component, Suspense, lazy } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import Wrapper from "views/Wrapper/components/WrapperContainer";
import AppLayout from "theme/layout/AppLayout";

const Leaders = lazy(() => import("./leaders"));
const Classes = lazy(() => import("./classes"));
const SRMs = lazy(() => import("./srms"))
const Students = lazy(() => import("./students"))
const Profile = lazy(() => import("./profile"));

class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <Wrapper>
        <AppLayout {...this.props}>
          <Suspense fallback={<div className="loading" />}>
            <Switch>
              <Redirect exact from={`${match.url}/`} to={`${match.url}/students`} />
              <Route path={`${match.url}/srms`} component={SRMs} />
              <Route path={`${match.url}/leaders`} component={Leaders} />
              <Route path={`${match.url}/classes`} component={Classes} />
              <Route path={`${match.url}/students`} component={Students} />
              <Route path={`${match.url}/profile`} component={Profile} />
              <Redirect to="/error" />
            </Switch>
          </Suspense>
        </AppLayout>
      </Wrapper>
    );
  }
}

export default withRouter(App);
