import React, { Component, Suspense, lazy } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import Wrapper from "views/Wrapper/components/WrapperContainer";

const Student = lazy(() => import("views/Student/components/Container"));
class Router extends Component {
    render() {
        const { match } = this.props;

        return (
            <Wrapper>
                <Suspense fallback={<div className="loading" />}>
                    <div className="dashboard-wrapper">
                        <Switch>
                            <Route path={`${match.url}/`} component={Student} />
                            <Redirect to="/error" />
                        </Switch>
                    </div>
                </Suspense>
            </Wrapper>
        );
    }
}

export default withRouter(Router);
