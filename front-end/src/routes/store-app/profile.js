import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import {Container} from "reactstrap"

import Profile from "views/Profile/components/Container";

const Infomation = ({ match }) => (
  <Container>
    <Switch>
      <Route path={`${match.url}/`} component={Profile} />
      <Redirect to="/error" />
    </Switch>
  </Container>
);
export default Infomation;
