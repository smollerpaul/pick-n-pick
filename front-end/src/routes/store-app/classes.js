import React, { Component, Suspense, lazy } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import Wrapper from "views/Wrapper/components/WrapperContainer";

const Classes = lazy(() => import("views/Classes/components/Container"));
const ClassesDetail = lazy(() => import("views/ClassesDetail/components/Container"));
class Router extends Component {
    render() {
        const { match } = this.props;

        return (
            <Wrapper>
                <Suspense fallback={<div className="loading" />}>
                <div className="dashboard-wrapper">
                    <Switch>
                        <Route path={`${match.url}/:id`} component={ClassesDetail} />
                        <Route path={`${match.url}/`} component={Classes} />
                        <Redirect to="/error" />
                    </Switch>
                </div>
                </Suspense>
            </Wrapper>
        );
    }
}

export default withRouter(Router);
