import React, { Component, Suspense, lazy } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import Wrapper from "views/Wrapper/components/WrapperContainer";
import AppLayout from "theme/layout/AppLayout";

const SRM = lazy(() => import("views/AdminSRM/components/Container"));
const Speakers = lazy(() => import("views/AdminSpeakers/components/Container"));
const University = lazy(() => import("views/AdminUniversity/components/Container"));
class Router extends Component {
    render() {
        const { match } = this.props;

        return (
            <Wrapper>
                <AppLayout {...this.props}>
                    <Suspense fallback={<div className="loading" />}>
                        <div className="dashboard-wrapper">
                            <Switch>
                                <Redirect exact from={`${match.url}/`} to={`${match.url}/srms`} />
                                <Route path={`${match.url}/srms`} component={SRM} />
                                <Route path={`${match.url}/speakers`} component={Speakers} />
                                <Route path={`${match.url}/university`} component={University} />
                                <Redirect to="/error" />
                            </Switch>
                        </div>
                    </Suspense>
                </AppLayout>
            </Wrapper>
        );
    }
}

export default withRouter(Router);
