import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import {Container} from "reactstrap"

import Classes from "views/Classes/components/Container";

const Infomation = ({ match }) => (
  <Container>
    <Switch>
      <Route path={`${match.url}/`} component={Classes} />
      <Redirect to="/error" />
    </Switch>
  </Container>
);
export default Infomation;
