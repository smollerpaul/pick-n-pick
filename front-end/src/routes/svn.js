import React, { Component, Suspense, lazy } from "react";
import { BrowserRouter, Route, withRouter, Switch, Redirect } from "react-router-dom";

const SVN = lazy(() => import("views/RoomLive/components/Container"));
class App extends Component {
    render() {
        const { match } = this.props;
        return (
            <Suspense fallback={<div className="loading" />}>
                <BrowserRouter>
                    <Switch>
                        <Redirect exact from={`${match.url}/`} to={`${match.url}`} />
                        <Route path={`${match.url}/speaking/`} component={Speaking} />
                        <Route path={`${match.url}/speaker/`} component={Speaker} />
                        <Route path={`/`} component={SVN} />
                        <Redirect to="/error" />
                    </Switch>
                </BrowserRouter>
            </Suspense>
        );
    }
}

class Speaker extends Component {
    render() {
        const { match } = this.props;
        return (
            <Suspense fallback={<div className="loading" />}>
                    <Switch>
                        <Route path={`${match.url}/:id`} component={SVN} />
                        <Redirect to="/error" />
                    </Switch>
            </Suspense>
        );
    }
}

class Speaking extends Component {
    render() {
        const { match } = this.props;
        return (
            <Suspense fallback={<div className="loading" />}>
                    <Switch>
                        <Route path={`${match.url}/:id`} component={SVN} />
                        <Redirect to="/error" />
                    </Switch>
            </Suspense>
        );
    }
}

export default withRouter(App);
