import { put, delay } from "redux-saga/effects";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";

//Closing Submenu
function* handleClosingSubmenu(action) {
  yield put(actions.hideSubMenu());
  yield delay(300);
  yield put(actions.toggleSubMenu());
}

function* closingSubMenu() {
  yield takeAction(actions.closingSubMenu, handleClosingSubmenu);
}
export default [closingSubMenu];
