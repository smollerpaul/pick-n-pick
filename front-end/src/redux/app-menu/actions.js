import { createAction } from "redux-actions";

export const toggleAppMenu = createAction("TOGGLE_APPMENU");
export const toggleSubMenu = createAction("TOGGLE_SUB_MENU");
export const hideSubMenu = createAction("HIDE_SUB_MENU");
export const closingSubMenu = createAction("CLOSING_SUB_MENU");
