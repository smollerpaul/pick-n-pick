import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

const initialState = freeze({
  isOpen: false,
  isClosing: false,
  isSubMenuOpen: false
});

export default handleActions(
  {
    [actions.toggleAppMenu]: (state, action) => {
      return freeze({
        ...state,
        isOpen: !state.isOpen
      });
    },
    [actions.hideSubMenu]: state => {
      return freeze({
        ...state,
        isClosing: true
      });
    },
    [actions.toggleSubMenu]: state => {
      return freeze({
        ...state,
        isClosing: false,
        isSubMenuOpen: !state.isSubMenuOpen
      });
    }
  },
  initialState
);
