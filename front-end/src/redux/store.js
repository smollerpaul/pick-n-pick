import { createStore, applyMiddleware, compose } from "redux";
import { createLogger } from "redux-logger";
import { routerMiddleware, connectRouter } from "connected-react-router";
import { createBrowserHistory, createMemoryHistory } from "history";

import createSagaMiddleware from "redux-saga";
import makeRootReducer from "./reducers";
import sagas from "./sagas";

// A nice helper to tell us if we're on the server
const isServer = !(
  typeof window !== "undefined" &&
  window.document &&
  window.document.createElement
);

const history = isServer
  ? createMemoryHistory({
      initialEntries: ["/"],
    })
  : createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();
const middleware = routerMiddleware(history);
const loggerMiddleware = createLogger({
  predicate: () => process.env.NODE_ENV === "development",
});
const middlewares = [sagaMiddleware, middleware, loggerMiddleware];

export function configureStore(initialState) {
  const store = createStore(
    connectRouter(history)(makeRootReducer(history)),
    initialState,
    compose(applyMiddleware(...middlewares))
  );

  sagaMiddleware.run(sagas);

  if (module.hot) {
    module.hot.accept("./reducers", () => {
      const nextRootReducer = require("./reducers");
      store.replaceReducer(nextRootReducer);
    });
  }

  return {
    store,
    history,
  };
}
