import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

///////////////////////////////////////////////////////////////////////////////////////////////
import settings from "./settings/reducer";
import menu from "./menu/reducer";
import appmenu from "./app-menu/reducer";

import LoginStores, { name as nameOfLoginStores } from "views/LoginStores";
import Register, { name as nameOfRegister } from "views/Register";
import Classes, { name as nameOfClasses } from "views/Classes";
import ClassesDetail, { name as nameOfClassesDetail } from "views/ClassesDetail";
import Leader, { name as nameOfLeader } from "views/Leader";
import SRM, { name as nameOfSRM } from "views/SRM";
import Student, { name as nameOfStudent } from "views/Student";
import AdminSRM, { name as nameOfAdminSRM } from "views/AdminSRM";
import AdminSpeakers, { name as nameOfAdminSpeakers } from "views/AdminSpeakers";
import AdminUniversity, { name as nameOfAdminUniversity } from "views/AdminUniversity";
import Wrapper, { name as nameOfWrapper } from "views/Wrapper";
import Profile, { name as nameOfProfile } from "views/Profile";
import Room, { name as nameOfRoom } from "views/RoomLive";

export const staticReducers = {
  appmenu,
  menu,
  settings,
  [nameOfLoginStores]: LoginStores,
  [nameOfRegister]: Register,
  [nameOfClasses]: Classes,
  [nameOfClassesDetail]: ClassesDetail,
  [nameOfLeader]: Leader,
  [nameOfSRM]: SRM,
  [nameOfStudent]: Student,
  [nameOfAdminSRM]: AdminSRM,
  [nameOfAdminSpeakers]: AdminSpeakers,
  [nameOfAdminUniversity]: AdminUniversity,
  [nameOfWrapper]: Wrapper,
  [nameOfProfile]: Profile,
  [nameOfRoom]: Room
};

export default (history, asyncReducers) =>
  combineReducers({ ...staticReducers, ...asyncReducers, router: connectRouter(history) });
