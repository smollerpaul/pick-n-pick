/**
 * @file init sagas
 */

import { all } from "redux-saga/effects";
import AppMenu from "./app-menu/sagas";
////////////////////////////////////////////////////////////////////////////////
// import { sagas as LoginUser } from "views/LoginUser";

import { sagas as Wrapper } from "views/Wrapper";

import { sagas as LoginStores } from "views/LoginStores";
import { sagas as Register } from "views/Register";
import { sagas as Classes } from "views/Classes";
import { sagas as ClassesDetail } from "views/ClassesDetail";
import { sagas as Leader } from "views/Leader";
import { sagas as SRM } from "views/SRM";
import { sagas as Student } from "views/Student";
import { sagas as AdminSRM } from "views/AdminSRM";
import { sagas as AdminSpeakers } from "views/AdminSpeakers";
import { sagas as AdminUniversity } from "views/AdminUniversity";
import { sagas as Profile } from "views/Profile";
import { sagas as RoomLive } from "views/RoomLive";

export const defaultSagaLists = [Wrapper, AppMenu, LoginStores];

export default function* rootSaga() {
  yield all([
    Wrapper(), 
    AppMenu(), 
    LoginStores(), 
    Register(),
    Classes(),
    ClassesDetail(),
    Leader(),
    SRM(),
    Student(),
    AdminSRM(),
    AdminSpeakers(),
    AdminUniversity(),
    Profile(),
    RoomLive()
  ]);
}
