/* Gogo Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/

module.exports = {
  /* 01.General */
  "general.copyright": "Gogo React © Todos los derechos reservados.",

  /* 02.Inicio de sesión de usuario, cierre de sesión, registro */
  "user.login-title": "Iniciar sesión",
  "user.register": "Registro",
  "user.forgot-password": "Se te olvidó tu contraseña",
  "user.email": "Email",
  "user.password": "Contraseña",
  "user.forgot-password-question": "¿Contraseña olvidada?",
  "user.fullname": "Nombre completo",
  "user.login-button": "INICIAR SESIÓN",
  "user.register-button": "REGISTRO",
  "user.reset-password-button": "REINICIAR",
  "user.buy": "COMPRAR",
  "user.username": "Nombre de Usuario",

  /* 03.Menú */
  "menu.app-user": "Inicio",
  "menu.listitem": "Lista de Productos",
  "menu.info": "User",
  "menu.personal": "Personal Infomation",
  "menu.application": "Application",
  "menu.dashboards": "Tableros",
  "menu.gogo": "Gogo",
  "menu.start": "Comienzo",
  "menu.device": "Device",
  "menu.albums": "Albums",
  "menu.yours": "Your photo",
  "menu.todolist": "To-do List",
  "menu.googleCalendar": "Google Calendar",
  "menu.ui": "IU",
  "menu.charts": "Gráficos",
  "menu.chat": "Chatea",
  "menu.survey": "Encuesta",
  "menu.search": "Búsqueda",
  "menu.docs": "Docs",
  "menu.dashboard": "Dashboard",
  "menu.blank-page": "Blank Page",

  /* 04.Error  */
  "pages.error-title": "Vaya, parece que ha ocurrido un error!",
  "pages.error-code": "Código de error",
  "pages.go-back-home": "REGRESAR A INICIO",

  /* 05.Form components*/
  "form-components.primary": "Primary",

  /* 06.Todo */
  "todo.add-new": "ADD NEW",
  "todo.add-new-title": "Add New Task",
  "todo.update-title": "Update Task",
  "todo.title": "Title",
  "todo.detail": "Detail",
  "todo.add-more": "Add more",
  "todo.type": "Type",
  "todo.label": "Label",
  "todo.status": "Status",
  "todo.cancel": "Cancel",
  "todo.submit": "Submit",
  "todo.update-task": "Update",
  "todo.del-task": "Delete",
  "todo.change-name": "Rename",
  "todo.del-folder": "Delete",
  "todo.display-options": "Display Options",
  "todo.orderby": "Order By : ",
  "todo.all-tasks": "All Tasks",
  "todo.pending-tasks": "Pending Tasks",
  "todo.completed-tasks": "Completed Tasks",
  "todo.task-list-empty": "Tasks list is empty",
  "todo.task-list-empty-sub": "Create the new one below to organize your work",
  "todo.categories": "Categories",
  "todo.labels": "Labels",
  "todo.tasklist": "Tasks List",
  "todo.new-folder": "New folder",
  "todo.folder-placeholder": "Folder's Name",
  "todo.start-time": "Start Time",
  "todo.end-time": "End Time",
  "todo.location": "Location",

  /* 07. Photo */
  "photo.upload": "Upload",
  "photo.selected-photo": "selected",
  "photo.deviceId": "Device ID",
  "photo.add-photos": "Add photos",
  "photo.add-to": "Add to",

  /* 08. Albums */
  "albums.create": "Create album",
  "albums.update": "Update album",
  "albums.title": "Title album",
  "albums.device": "Device Id",

  "homepage.weather": "Weather",
  "homepage.tasksName": "Task Name",
  "homepage.calendar": "My Calendars",
  "homepage.todayTasks": "Today Tasks",
  "homepage.isDone": "Done",
  "homepage.isNotDone": "Processing",
};
