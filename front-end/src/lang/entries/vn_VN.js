import appLocaleData from 'react-intl/locale-data/vi';
import vnMessages from '../locales/vn_VN';

const vnLang = {
    messages: {
        ...vnMessages
    },
    locale: 'vi',
    data: appLocaleData
};
export default vnLang;