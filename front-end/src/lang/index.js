import { addLocaleData } from 'react-intl';
import enLang from './entries/en-US';
import esLang from './entries/es-ES';
import enRtlLang from './entries/en-US-rtl';
import vnLang from "./entries/vn_VN";

const AppLocale = {
    en: enLang,
    es: esLang,
    vi: vnLang,
    enrtl:enRtlLang
};
addLocaleData(AppLocale.en.data);
addLocaleData(AppLocale.es.data);
addLocaleData(AppLocale.vi.data);
addLocaleData(AppLocale.enrtl.data);

export default AppLocale;
