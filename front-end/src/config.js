import { get } from "lodash";
const env = {
  development: {
    apiBaseURL: "https://igen.funitech.vn/apis/v1",
    apiNLPBaseURL: "https://igen.funitech.vn/nlp/v1",
    apiURL: "https://igen.funitech.vn",
    redirectUrl: "http://localhost:3000",
    redirectUrlZalo: "https://staging.mistants.com/app-user/info/thirdParty",
    facebookKey: "2405503222996843",
    googleKey: "350106747877-61upqvkvr0nei7en959qqdqioq0pait6.apps.googleusercontent.com",
    trelloKey: "ebb94cfd8d436146aa50db7b0e540684",
    spotifyKey: "49edc987ec2a4d4685d7ffd8679d3110",
    zaloId: "1777210115141317295",
    googleAnalyticCode: "UA-155882211-1",
    fbPageId: "110775490423316"
  },
  stage: {
    apiBaseURL: "https://igen.funitech.vn/apis/v1",
    apiNLPBaseURL: "https://igen.funitech.vn/nlp/v1",
    apiURL: "https://igen.funitech.vn",
    redirectUrl: "http://localhost:3000",
    redirectUrlZalo: "https://staging.mistants.com/app-user/info/thirdParty",
    facebookKey: "2405503222996843",
    googleKey: "350106747877-61upqvkvr0nei7en959qqdqioq0pait6.apps.googleusercontent.com",
    trelloKey: "ebb94cfd8d436146aa50db7b0e540684",
    spotifyKey: "49edc987ec2a4d4685d7ffd8679d3110",
    zaloId: "1777210115141317295",
    googleAnalyticCode: "UA-155882211-1",
    googleSiteKeyCaptcha: "6Lf2is4UAAAAAM9L349ihD4Hk4EPtfjzdB08iIhg",
    fbPageId: "110775490423316"
  },
  production: {
    apiBaseURL: "https://thanhdoan-api.funitech.vn/apis/v1",
    apiURL: "https://production-apis.funipos.net",
    apiNLPBaseURL: "https://production-apis.funipos.net/nlp/v1",
    redirectUrl: "https://production-apis.funipos.net",
    facebookKey: "778689459284014",
    googleKey: "73166716191-g4akv5ggo7jgppv96n66b5oh65vnh3tj.apps.googleusercontent.com",
    trelloKey: "ebb94cfd8d436146aa50db7b0e540684",
    spotifyKey: "49edc987ec2a4d4685d7ffd8679d3110",
    fbPageId: "110775490423316"
  },
  onLocal: {
    apiBaseURL: "http://localhost:1426/apis/v1",
    apiNLPBaseURL: "http://localhost:1426/nlp/v1",
    apiURL: "http://localhost:1426",
    redirectUrl: "http://localhost:3000",
    facebookKey: "2405503222996843",
    googleKey: "350106747877-61upqvkvr0nei7en959qqdqioq0pait6.apps.googleusercontent.com",
    trelloKey: "ebb94cfd8d436146aa50db7b0e540684",
    spotifyKey: "49edc987ec2a4d4685d7ffd8679d3110",
    zaloId: "1777210115141317295",
    googleAnalyticCode: "UA-155882211-1",
    fbPageId: "110775490423316"
  }
};

const envConfig = get(process.env, "REACT_APP_NODE_ENV", "development");

const configsExport = { ...env[envConfig] };
const isDebug = configsExport.logging === "DEBUG" ? true : false;

export default {
  ...configsExport,
  isDebug
};
