import React, { Component, Suspense, lazy } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { IntlProvider } from "react-intl";
import AppLocale from "lang";
import NotificationContainer from "theme/components/common/react-notifications/NotificationContainer";
import ValidateDataContainer from "theme/components/common/validateData/Container";
import { getDirection } from "helpers/Utils";

// const main = lazy(() => import("./routes"));
const error = lazy(() => import("./routes/error"));
const homepage = lazy(() => import("./routes/index"));
const svn = lazy(() => import("./routes/svn"));

export let current;

// Admin Import

// Store Impage
const storeModule = lazy(() => import("./routes/store-auth"));
const appStore = lazy(() => import("./routes/store-app"));
const adminPage = lazy(() => import("./routes/admin-app"));

// Welcome Import

class App extends Component {
  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidUpdate() {
    const direction = getDirection();
    if (direction.isRtl) {
      document.body.classList.add("rtl");
      document.body.classList.remove("ltr");
    } else {
      document.body.classList.add("ltr");
      document.body.classList.remove("rtl");
    }
  }

  render() {
    const { locale } = this.props;
    const currentAppLocale = AppLocale[locale];

    return (
      <div className="h-100">
        <IntlProvider locale={currentAppLocale.locale} messages={currentAppLocale.messages}>
          <React.Fragment>
            <NotificationContainer />
            <ValidateDataContainer />
            <Router>
              <Suspense fallback={<div className="loading" />}>
                <Switch>
                  <Route path="/app" component={appStore} />
                  <Route path="/admin" component={adminPage} />
                  <Route path="/user" component={storeModule} />
                  <Route path={`/vsn`} component={svn} />
                  <Route path="/error" exact component={error} />
                  <Redirect to="/app" />
                </Switch>
              </Suspense>
            </Router>
          </React.Fragment>
        </IntlProvider>
      </div>
    );
  }
}

const mapStateToProps = ({ settings }) => {
  const { locale } = settings;
  return { locale };
};
const mapActionsToProps = {};

export default connect(mapStateToProps, mapActionsToProps)(App);
