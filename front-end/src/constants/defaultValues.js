/* 
Menu Types:
"menu-default", "menu-sub-hidden", "menu-hidden"
*/
export const defaultMenuType = "menu-sub-hidden";
export const defaultTitle = {
  vi: "Nền tảng quản lý & bán hàng Tốt nhất Việt Nam | FuniPos Application",
  en: "The best sales & management platform in Vietnam | FuniPos Application"
};
export const appName = "| FuniPos Application";
export const subHiddenBreakpoint = 1440;
export const menuHiddenBreakpoint = 768;
export const defaultLocale = "en";
export const localeOptions = [
  { id: "en", name: "English", direction: "ltr" },
  { id: "vi", name: "Vietnamese", direction: "ltr" }
];

export const firebaseConfig = {
  apiKey: "AIzaSyBBksq-Asxq2M4Ot-75X19IyrEYJqNBPcg",
  authDomain: "gogo-react-login.firebaseapp.com",
  databaseURL: "https://gogo-react-login.firebaseio.com",
  projectId: "gogo-react-login",
  storageBucket: "gogo-react-login.appspot.com",
  messagingSenderId: "216495999563"
};

export const defaultPageSize = 10;
export const defaultPageItemSize = 12;

export const searchPath = "/app/pages/search";
export const servicePath = "https://api.coloredstrategies.com";

/* 
Color Options:
"light.purple", "light.blue", "light.green", "light.orange", "light.red", "dark.purple", "dark.blue", "dark.green", "dark.orange", "dark.red"
*/
export const isMultiColorActive = false;
export const defaultColor = "light.blue";
export const defaultDirection = "ltr";
export const isDarkSwitchActive = false;
export const themeColorStorageKey = "__theme_color";

export const userManagerRoles = [
  "superAdmin",
  "manager",
  "sales",
  "hr",
  "employees",
  "partTime",
  "fullTime"
];
export const timeTableTypes = [
  "fullday",
  "shiftOne",
  "shiftTwo",
  "shiftThree",
  "shiftFour",
  "shiftFive"
];

export const menuBars = ["stores", "employees", "analytics"];
