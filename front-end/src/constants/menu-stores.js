export const stores = [
  {
    id: "students",
    icon: "iconsminds-students",
    label: "menu.students",
    to: "/app/students",
    roles: ["uniLeader", "leader", "student"],
  },
  {
    id: "Leaders",
    icon: "iconsminds-students",
    label: "menu.leaders",
    to: "/app/leaders",
    roles: ["leader"],
  },
  {
    id: "unitLeader",
    icon: "iconsminds-dashboard",
    label: "menu.unitLeader",
    to: "/app/classes",
    roles: ["uniLeader"],
  },
  {
    id: "SRM",
    icon: "iconsminds-conference",
    label: "menu.srms",
    to: "/app/srms",
    roles: ["srm"],
  },
  {
    id: "AdminSRM",
    label: "menu.adminsrm",
    icon: "iconsminds-conference",
    roles: ["admin"],
    to: "/admin/srms"
  },
  {
    id: "speakers",
    label: "menu.speakers",
    icon: "iconsminds-students",
    roles: ["admin"],
    to: "/admin/speakers"
  },
  {
    id: "university",
    label: "menu.university",
    icon: "iconsminds-dashboard",
    roles: ["admin"],
    to: "/admin/university"
  },

];

export default stores;
