const data = [
  {
    id: "dashboard",
    icon: "iconsminds-shop-4",
    label: "menu.dashboard",
    to: "/app-user/info"
  },
  {
    id: "info",
    icon: "simple-icon-user",
    label: "menu.info",
    to: "/app-user/info",
    subs: [
      {
        icon: "simple-icon-user",
        label: "menu.personal",
        to: "/app-user/info/personal"
      },
      {
        icon: "iconsminds-check",
        label: "menu.application",
        to: "/app-user/info/devices"
      },
      {
        icon: "simple-icon-layers",
        label: "menu.thirdParty",
        to: "/app-user/info/thirdParty"
      }
    ]
  },
  {
    id: "device",
    icon: "iconsminds-check",
    label: "menu.todolist",
    to: "/app-user/device/todolist"
  },
  {
    id: "photos",
    icon: "simple-icon-picture",
    label: "menu.photos",
    to: "/app-user/photos/albums",
  }
];
export default data;
