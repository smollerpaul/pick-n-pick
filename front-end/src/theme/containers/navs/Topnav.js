import React, { Component, Fragment } from "react";
import { injectIntl } from "react-intl";
import { UncontrolledDropdown, DropdownItem, DropdownToggle, DropdownMenu } from "reactstrap";
import { NavLink, Link } from "react-router-dom";
import { connect } from "react-redux";
import { setContainerClassnames, clickOnMobileMenu } from "redux/actions";
import { menuHiddenBreakpoint, searchPath } from "constants/defaultValues";
import { MobileMenuIcon, MenuIcon } from "theme/components/svg";
import { name as nameOfWrapper } from "views/Wrapper";
import { clearEverything } from "services/localStoredService";
import { get } from "lodash";
import { withRouter } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";

class TopNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isInFullScreen: false,
      searchKeyword: "",
    };
  }

  isInFullScreen = () => {
    return (
      (document.fullscreenElement && document.fullscreenElement !== null) ||
      (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
      (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
      (document.msFullscreenElement && document.msFullscreenElement !== null)
    );
  };
  handleSearchIconClick = (e) => {
    if (window.innerWidth < menuHiddenBreakpoint) {
      let elem = e.target;
      if (!e.target.classList.contains("search")) {
        if (e.target.parentElement.classList.contains("search")) {
          elem = e.target.parentElement;
        } else if (e.target.parentElement.parentElement.classList.contains("search")) {
          elem = e.target.parentElement.parentElement;
        }
      }

      if (elem.classList.contains("mobile-view")) {
        this.search();
        elem.classList.remove("mobile-view");
        this.removeEventsSearch();
      } else {
        elem.classList.add("mobile-view");
        this.addEventsSearch();
      }
    } else {
      this.search();
    }
  };
  addEventsSearch = () => {
    document.addEventListener("click", this.handleDocumentClickSearch, true);
  };
  removeEventsSearch = () => {
    document.removeEventListener("click", this.handleDocumentClickSearch, true);
  };
  handleDocumentClickSearch = (e) => {
    let isSearchClick = false;
    if (
      e.target &&
      e.target.classList &&
      (e.target.classList.contains("navbar") ||
        e.target.classList.contains("simple-icon-magnifier"))
    ) {
      isSearchClick = true;
      if (e.target.classList.contains("simple-icon-magnifier")) {
        this.search();
      }
    } else if (
      e.target.parentElement &&
      e.target.parentElement.classList &&
      e.target.parentElement.classList.contains("search")
    ) {
      isSearchClick = true;
    }

    if (!isSearchClick) {
      const input = document.querySelector(".mobile-view");
      if (input && input.classList) input.classList.remove("mobile-view");
      this.removeEventsSearch();
      this.setState({
        searchKeyword: "",
      });
    }
  };
  handleSearchInputChange = (e) => {
    this.setState({
      searchKeyword: e.target.value,
    });
  };
  handleSearchInputKeyPress = (e) => {
    if (e.key === "Enter") {
      this.search();
    }
  };
  search = () => {
    this.props.history.push(searchPath + "/" + this.state.searchKeyword);
    this.setState({
      searchKeyword: "",
    });
  };
  toggleFullScreen = () => {
    const isInFullScreen = this.isInFullScreen();

    var docElm = document.documentElement;
    if (!isInFullScreen) {
      if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
      } else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
      } else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
      } else if (docElm.msRequestFullscreen) {
        docElm.msRequestFullscreen();
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
    this.setState({
      isInFullScreen: !isInFullScreen,
    });
  };
  handleLogout = () => {
    clearEverything();
  };
  menuButtonClick = (e, menuClickCount, containerClassnames) => {
    e.preventDefault();
    setTimeout(() => {
      var event = document.createEvent("HTMLEvents");
      event.initEvent("resize", false, false);
      window.dispatchEvent(event);
    }, 350);
    this.props.setContainerClassnames(
      ++menuClickCount,
      containerClassnames,
      this.props.selectedMenuHasSubItems
    );
  };
  mobileMenuButtonClick = (e, containerClassnames) => {
    e.preventDefault();
    this.props.clickOnMobileMenu(containerClassnames);
  };
  render() {
    const { containerClassnames, menuClickCount, userInfo, history, location } = this.props;
    const { messages } = this.props.intl;
    return (
      <nav className="navbar navbar-pos fixed-top">
        <div className="d-flex align-items-center navbar-left">
          {get(location, "pathname", "/") === "/" ? (
            <Fragment>
              <NavLink to="/app" className="menu-button d-none d-md-block">
                <i style={{ color: "white" }} className="iconsminds-home" />
              </NavLink>
              <NavLink to="/app" className="menu-button-mobile d-xs-block d-sm-block d-md-none">
                <i style={{ color: "white" }} className="iconsminds-home" />
              </NavLink>
            </Fragment>
          ) : (
              <Fragment>
                <NavLink
                  to="#"
                  className="menu-button d-none d-md-block"
                  style={{ color: "white", fontSize: "3em" }}
                  onClick={(e) => this.menuButtonClick(e, menuClickCount, containerClassnames)}
                >
                  {/* <MenuIcon /> */}
                  <div className="h4" style={{color: "#8636AC"}}>
                    Roles
                  </div>
                </NavLink>
                <NavLink
                  to="#"
                  style={{ color: "white" }}
                  className="menu-button-mobile d-xs-block d-sm-block d-md-none"
                  onClick={(e) => this.mobileMenuButtonClick(e, containerClassnames)}
                >
                  <div className="h4" style={{color: "#8636AC"}}>
                    Roles
                  </div>
                </NavLink>
              </Fragment>
            )}
        </div>
        <Link className="navbar-logo" to="/app">
          <span className="logo d-none d-xs-block" />
          <span className="logo-mobile d-block d-xs-none" />
        </Link>
        <div className="navbar-right">
          {/* <div className="header-icons d-inline-block align-middle">
            <button
              className="header-icon btn btn-empty d-none d-sm-inline-block"
              type="button"
              id="fullScreenButton"
              onClick={this.toggleFullScreen}
            >
              {this.state.isInFullScreen ? (
                <i className="simple-icon-size-actual d-block" />
              ) : (
                <i className="simple-icon-size-fullscreen d-block" />
              )}
            </button>
          </div> */}
          <div className="user d-inline-block">
            <UncontrolledDropdown className="dropdown-menu-right d-flex flex-column ">
              <DropdownToggle className="p-0" color="empty">
                <span>
                  <div
                    style={{
                      backgroundImage: `url("${get(userInfo, "avatar", "/assets/img/avatar.png")}")`,
                      backgroundSize: "cover",
                      height: "3em",
                      backgroundPosition: "center",
                      borderRadius: "100%",
                      width: "3em",
                    }}
                  >
                  </div>
                </span>
              </DropdownToggle>
              <DropdownMenu className="mt-3">
                <DropdownItem>
                  <div className="text-center">
                    <div className="d-flex justify-content-center mb-2">
                      <div
                        style={{
                          backgroundImage: `url("${get(userInfo, "avatar", "/assets/img/avatar.png")}")`,
                          backgroundSize: "cover",
                          height: "10em",
                          backgroundPosition: "center",
                          borderRadius: "100%",
                          width: "10em",
                        }}
                      >
                      </div>
                    </div>
                    <p className="name text-capitalize">{get(userInfo, "name", "User Name")}</p>
                    <p className="name ">{get(userInfo, "universityId.name", "")}</p>
                    {userInfo.classId &&
                    <p className="name">Team: {get(userInfo, "classId.name", "")}</p>
                    }
                    <p className="name">Role: {messages[`user.role.${get(userInfo, "role", "role")}`]}</p>
                  </div>
                </DropdownItem>
                <DropdownItem className="text-center" onClick={() => history.push(`/app/profile`)}>Profile</DropdownItem>
                <DropdownItem className="text-center" onClick={() => this.handleLogout()}>Log out</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = (state) => {
  const { containerClassnames, menuClickCount, selectedMenuHasSubItems } = state.menu;
  return {
    containerClassnames,
    menuClickCount,
    selectedMenuHasSubItems,
    router: state.router,
    ...state[nameOfWrapper],
  };
};
export default injectIntl(
  withRouter(connect(mapStateToProps, { setContainerClassnames, clickOnMobileMenu })(TopNav))
);
