import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { UncontrolledDropdown, DropdownItem, DropdownToggle, DropdownMenu } from "reactstrap";
import { NavLink, Link } from "react-router-dom";
import { connect } from "react-redux";
import { setContainerClassnames, clickOnMobileMenu } from "redux/actions";
import { menuHiddenBreakpoint, searchPath } from "constants/defaultValues";
import { isDarkSwitchActive } from "constants/defaultValues";
import { MobileMenuIcon, MenuIcon } from "theme/components/svg";
import TopnavDarkSwitch from "./Topnav.DarkSwitch";
import { name as nameOfWrapper } from "views/Stores/Wrapper";
import { clearEverything } from "services/localStoredService";
import { get } from "lodash";
import IntlMessages from "helpers/IntlMessages";
import SelectLanguages from "theme/components/common/SelectLanguages";

class TopNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isInFullScreen: false
    };
  }

  isInFullScreen = () => {
    return (
      (document.fullscreenElement && document.fullscreenElement !== null) ||
      (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
      (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
      (document.msFullscreenElement && document.msFullscreenElement !== null)
    );
  };
  handleSearchIconClick = e => {
    if (window.innerWidth < menuHiddenBreakpoint) {
      let elem = e.target;
      if (!e.target.classList.contains("search")) {
        if (e.target.parentElement.classList.contains("search")) {
          elem = e.target.parentElement;
        } else if (e.target.parentElement.parentElement.classList.contains("search")) {
          elem = e.target.parentElement.parentElement;
        }
      }

      if (elem.classList.contains("mobile-view")) {
        this.search();
        elem.classList.remove("mobile-view");
        this.removeEventsSearch();
      } else {
        elem.classList.add("mobile-view");
        this.addEventsSearch();
      }
    } else {
      this.search();
    }
  };
  addEventsSearch = () => {
    document.addEventListener("click", this.handleDocumentClickSearch, true);
  };
  removeEventsSearch = () => {
    document.removeEventListener("click", this.handleDocumentClickSearch, true);
  };
  handleDocumentClickSearch = e => {
    let isSearchClick = false;
    if (
      e.target &&
      e.target.classList &&
      (e.target.classList.contains("navbar-pos") ||
        e.target.classList.contains("simple-icon-magnifier"))
    ) {
      isSearchClick = true;
      if (e.target.classList.contains("simple-icon-magnifier")) {
        this.search();
      }
    } else if (
      e.target.parentElement &&
      e.target.parentElement.classList &&
      e.target.parentElement.classList.contains("search")
    ) {
      isSearchClick = true;
    }

    if (!isSearchClick) {
      const input = document.querySelector(".mobile-view");
      if (input && input.classList) input.classList.remove("mobile-view");
      this.removeEventsSearch();
      this.setState({
        searchKeyword: ""
      });
    }
  };
  handleSearchInputChange = e => {
    this.setState({
      searchKeyword: e.target.value
    });
  };
  handleSearchInputKeyPress = e => {
    if (e.key === "Enter") {
      this.search();
    }
  };
  search = () => {
    this.props.history.push(searchPath + "/" + this.state.searchKeyword);
    this.setState({
      searchKeyword: ""
    });
  };
  toggleFullScreen = () => {
    const isInFullScreen = this.isInFullScreen();

    var docElm = document.documentElement;
    if (!isInFullScreen) {
      if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
      } else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
      } else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
      } else if (docElm.msRequestFullscreen) {
        docElm.msRequestFullscreen();
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
    this.setState({
      isInFullScreen: !isInFullScreen
    });
  };
  handleLogout = () => {
    clearEverything();
    this.props.history.push("/user");
  };
  menuButtonClick = (e, menuClickCount, containerClassnames) => {
    e.preventDefault();
    setTimeout(() => {
      var event = document.createEvent("HTMLEvents");
      event.initEvent("resize", false, false);
      window.dispatchEvent(event);
    }, 350);
    this.props.setContainerClassnames(
      ++menuClickCount,
      containerClassnames,
      this.props.selectedMenuHasSubItems
    );
  };
  mobileMenuButtonClick = (e, containerClassnames) => {
    e.preventDefault();
    this.props.clickOnMobileMenu(containerClassnames);
  };

  render() {
    const {
      containerClassnames,
      menuClickCount,
      actions,
      selectStores,
      userInfo,
      history
    } = this.props;

    return (
      <nav className="navbar navbar-pos fixed-top">
        <div className="d-flex align-items-center navbar-pos-left">
          <NavLink
            to="#"
            className="menu-button d-none d-md-block"
            onClick={e => this.menuButtonClick(e, menuClickCount, containerClassnames)}
          >
            <MenuIcon />
          </NavLink>
          <NavLink
            to="#"
            className="menu-button-mobile d-xs-block d-sm-block d-md-none"
            onClick={e => this.mobileMenuButtonClick(e, containerClassnames)}
          >
            <MobileMenuIcon />
          </NavLink>
          {selectStores && selectStores.brandsAndStores && (
            <div className="search">
              <UncontrolledDropdown className="ml-2">
                <DropdownToggle caret color="light" size="sm" className="language-button">
                  <span className="name">
                    <i className="iconsminds-shop-2" style={{ paddingRight: "10px" }} />
                    {selectStores.currentStore.name} - {selectStores.currentStore.brandName}
                  </span>
                </DropdownToggle>
                <DropdownMenu className="mt-3">
                  {selectStores.brandsAndStores.map(
                    l =>
                      l.stores &&
                      l.stores.map(i => (
                        <DropdownItem
                          onClick={() => {
                            actions.selectStores({ storeId: i._id, brandId: l._id });
                            window.location.reload();
                          }}
                          key={l._id}
                        >
                          {i.name} - {l.name}
                        </DropdownItem>
                      ))
                  )}
                </DropdownMenu>
              </UncontrolledDropdown>
            </div>
          )}
          <SelectLanguages />
          <div className="position-relative d-none d-none d-lg-inline-block">
            <NavLink className="btn btn-outline-primary btn-sm ml-2" to="/pos/select-stores">
              <i className="iconsminds-shop-2" />{" "}
              <IntlMessages id="selectBrands.selectBrands" />
            </NavLink>
          </div>
        </div>
        <Link className="navbar-pos-logo" to="/">
          <span className="logo d-none d-xs-block" />
          <span className="logo-mobile d-block d-xs-none" />
        </Link>
        <div className="navbar-pos-right">
          {isDarkSwitchActive && <TopnavDarkSwitch />}

          <div className="header-icons d-inline-block align-middle">
            {/* <TopnavEasyAccess />
            <TopnavNotifications notifications={this.props.notifications} /> */}
            <button
              className="header-icon btn btn-empty d-none d-sm-inline-block"
              type="button"
              id="fullScreenButton"
              onClick={this.toggleFullScreen}
            >
              {this.state.isInFullScreen ? (
                <i className="simple-icon-size-actual d-block" />
              ) : (
                <i className="simple-icon-size-fullscreen d-block" />
              )}
            </button>
          </div>
          <div className="user d-inline-block">
            <UncontrolledDropdown className="dropdown-menu-right">
              <DropdownToggle className="p-0" color="empty">
                <span className="name mr-1">{get(userInfo, "fullname", "User Name")}</span>
                <span>
                  <img
                    alt="Profile"
                    src={get(userInfo, "avatar", "/assets/img/profile-pic-l.jpg")}
                  />
                </span>
              </DropdownToggle>
              <DropdownMenu className="mt-3" right>
                <DropdownItem onClick={() => history.push("/app/stores/info/personal")}>
                  <IntlMessages id="topnav.account" />
                </DropdownItem>
                <DropdownItem onClick={() => this.handleLogout()}>
                  <IntlMessages id="topnav.signOut" />
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => {
  const { containerClassnames, menuClickCount, selectedMenuHasSubItems } = state.menu;

  return {
    containerClassnames,
    menuClickCount,
    selectedMenuHasSubItems,

    ...state[nameOfWrapper]
  };
};
export default injectIntl(
  connect(mapStateToProps, { setContainerClassnames, clickOnMobileMenu })(TopNav)
);
