import React, { PureComponent } from "react";
import { injectIntl } from "react-intl";
import { Row, Label, FormGroup } from "reactstrap";
import { Colxx } from "theme/components/common/CustomBootstrap";
import Select from "react-select";
import IntlMessages from "helpers/IntlMessages";
import CustomSelectInput from "theme/components/common/CustomSelectInput";
import config from "../../config";
import { request } from "services/api";
import { isEmpty, find, get } from "lodash";
import RequireMessages from "theme/components/common/RequireMessages";

class DeliveryAddress extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      districts: [],
      wards: [],
      city: "",
      district: "",
      ward: "",
    };
  }
  componentDidMount() {
    this.searchInfo({ type: "cities" });
  }

  componentDidUpdate() {
    const { city, ward, district } = this.state;
    const { cities, districts, wards } = this.state;
    if (!isEmpty(this.props.city)) {
      if (isEmpty(city) && !isEmpty(cities)) {
        const myCity = find(cities, (o) => o.label === this.props.city);
        this.setState({ city: get(myCity, "value") });
        this.searchInfo({ type: "district", district: get(myCity, "value") });
      }
    }
    if (!isEmpty(this.props.district)) {
      if (isEmpty(district) && !isEmpty(districts)) {
        const myDistrict = find(districts, (o) => o.label === this.props.district);
        this.setState({ district: get(myDistrict, "value") });
        this.searchInfo({ type: "ward", ward: get(myDistrict, "value"), district: city });
      }
    }
    if (!isEmpty(this.props.ward)) {
      if (isEmpty(ward) && !isEmpty(wards)) {
        const myWard = find(wards, (o) => o.label === this.props.ward);
        this.setState({ ward: get(myWard, "value") });
      }
    }
  }

  componentWillUnmount() {
    this.setState({ cities: [], districts: [], wards: [], city: "", district: "", ward: "" });
  }
  searchInfo = async (props) => {
    const { type, district, ward } = props;
    if (type === "cities") {
      const { data } = await request(`${config.apiBaseURL}/delivery-code/cities`);
      this.setState({ cities: data.map((o) => ({ label: o.nameWithType, value: o.code })) });
    }
    if (type === "district") {
      const { data } = await request(
        `${config.apiBaseURL}/delivery-code/cities/${district}/district`
      );
      this.setState({ districts: data.map((o) => ({ label: o.nameWithType, value: o.code })) });
    }
    if (type === "ward") {
      const { data } = await request(
        `${config.apiBaseURL}/delivery-code/cities/${district}/district/${ward}/ward`
      );
      this.setState({ wards: data.map((o) => ({ label: o.nameWithType, value: o.code })) });
    }
  };
  render() {
    const { isEdit, lg = "4", required = false, error = {} } = this.props;
    const { onChangeCities, onChangeDistrict, onChangeWard } = this.props;
    return (
      <Row>
        <Colxx xxs="12" lg={lg}>
          <FormGroup>
            <Label>
              <IntlMessages id="deliveryAddress.city" />
              {required && <span className="text-red">*</span>}
            </Label>
            <Select
              classNamePrefix="react-select"
              className="react-select"
              isDisabled={isEdit}
              components={{ Input: CustomSelectInput }}
              options={this.state.cities}
              value={find(this.state.cities, (o) => o.value === this.state.city)}
              onChange={(e) => {
                onChangeCities(e);
                this.setState({ district: e.value });
                this.setState({ city: e.value });
                this.searchInfo({ type: "district", district: e.value });
              }}
            />
            {required && error.province && <RequireMessages heading="deliveryAddress.city" />}
          </FormGroup>
        </Colxx>
        <Colxx xxs="12" lg={lg}>
          <FormGroup>
            <Label>
              <IntlMessages id="deliveryAddress.district" />
              {required && <span className="text-red">*</span>}
            </Label>
            <Select
              classNamePrefix="react-select"
              className="react-select"
              isDisabled={isEdit}
              components={{ Input: CustomSelectInput }}
              options={this.state.districts}
              value={find(this.state.districts, (o) => o.value === this.state.district)}
              onChange={(e) => {
                onChangeDistrict(e);
                this.setState({ ward: e.value });
                this.searchInfo({ type: "ward", district: this.state.district, ward: e.value });
              }}
            />
            {required && error.district && <RequireMessages heading="deliveryAddress.city" />}
          </FormGroup>
        </Colxx>
        <Colxx xxs="12" lg={lg}>
          <FormGroup>
            <Label>
              <IntlMessages id="deliveryAddress.ward" />
              {required && <span className="text-red">*</span>}
            </Label>
            <Select
              classNamePrefix="react-select"
              className="react-select"
              isDisabled={isEdit}
              components={{ Input: CustomSelectInput }}
              options={this.state.wards}
              value={find(this.state.wards, (o) => o.value === this.state.ward)}
              onChange={(e) => {
                onChangeWard(e);
                this.setState({ ward: e.value });
              }}
            />
            {required && error.ward && <RequireMessages heading="deliveryAddress.city" />}
          </FormGroup>
        </Colxx>
      </Row>
    );
  }
}

export default injectIntl(DeliveryAddress);
