import React from "react";
import "react-dropzone-uploader/dist/styles.css";
import Dropzone from "react-dropzone-uploader";
import config from "../../config";
import { get } from "lodash";
import { NotificationManager } from "theme/components/common/react-notifications";

export default function DropzoneExample(props) {
  const getUploadParams = async ({ file, meta: { name } }) => {
    try {
      const body = new FormData();
      body.append("file", file);
      const endpoint = config.apiURL + "/files";
      return { url: endpoint, body };
    } catch (err) {
      NotificationManager.error("Errors", err);
    }
  };
  const handleChangeStatus = (input, status) => {
    const { xhr } = input;

    if (status === "done") {
      const data = JSON.parse(xhr.response);
      props.onUpload(get(data, "data.url"));
    }
    if (status === "removed") {
      const data = JSON.parse(xhr.response);
      props.onUpload(get(data, "data.url"));
    }
  };

  return (
    <Dropzone
      getUploadParams={getUploadParams}
      onChangeStatus={handleChangeStatus}
      submitButtonDisabled={true}
      accept="image/*"
    />
  );
}
