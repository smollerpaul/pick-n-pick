import React, { Fragment } from "react";
import { UncontrolledTooltip } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";

const TooltipsHelper = props => {
  const id = `tooltip-${Math.random().toString(36).substr(2, 9)}`;
  return (
    <Fragment>
      {" "}
      <i className="simple-icon-question text-muted" id={id} style={{ fontSize: "1rem"}}>
        <UncontrolledTooltip placement="auto" target={id}>
          <IntlMessages id={props.id} />
        </UncontrolledTooltip>
      </i>
    </Fragment>
  );
};

export default TooltipsHelper;
