import React, { useState } from "react";
import { injectIntl } from "react-intl";
import { InputGroup, Input, InputGroupAddon, Button } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import Animate from "theme/components/Animate";

function SearchButton(props) {
  const { onSearch, onToggle, search, onChange, name } = props;
  const [isOpen, setOpen] = useState(false);

  return (
    <div className="text-zero top-right-button-container">
      <InputGroup className="mb-3 top-right-button">
        {isOpen && (
          <Animate animation="transition.slideRightIn" duration={300} delay={400}>
            <Input
              name="all"
              outline
              value={search}
              onKeyPress={e => {
                if (e.key === "Enter") onSearch();
              }}
              onChange={e => onChange(e)}
            />
          </Animate>
        )}
        <InputGroupAddon addonType="prepend">
          <Button color="primary" onClick={() => setOpen(!isOpen)}>
            <i className="iconsminds-magnifi-glass" />
          </Button>
        </InputGroupAddon>
        <InputGroupAddon addonType="append">
          <Button color="primary" size="lg" className="top-right-button" onClick={() => onToggle()}>
            <IntlMessages id={name} />
          </Button>
        </InputGroupAddon>
      </InputGroup>
    </div>
  );
}

export default injectIntl(SearchButton);
