import { EventEmitter } from "events";
import { isEmpty } from "lodash"

const createUUID = () => {
  const pattern = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
  return pattern.replace(/[xy]/g, c => {
    const r = (Math.random() * 16) | 0;
    const v = c === "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

const Constants = {
  CHANGE: "change",
  PRIMARY: "primary",
  SECONDARY: "secondary",
  INFO: "info",
  SUCCESS: "success",
  WARNING: "warning",
  ERROR: "error"
};

class NotificationManager extends EventEmitter {
  constructor() {
    super();
    this.listNotify = [];
  }

  create(notify) {
    const defaultNotify = {
      id: createUUID(),
      type: Constants.WARNING,
      title: null,
      missingFields: [],
      language: "",
      timeOut: 5000
    };
    this.listNotify.push(Object.assign(defaultNotify, notify));
    this.emitChange();
  }

  validate(data, requiredFields, language) {
    const inputFields = Object.keys(data).filter(o => !isEmpty(o))
    const missingFields = requiredFields.filter(o => !inputFields.includes(o));
    const title = `validate.${Math.floor(Math.random() * Math.floor(10))}`;
    if (missingFields.length !== 0) {
      this.create({ missingFields, language, title });
    }
    return missingFields.length === 0;
  }

  remove(notification) {
    this.listNotify = this.listNotify.filter(n => notification.id !== n.id);
    this.emitChange();
  }

  emitChange() {
    this.emit(Constants.CHANGE, this.listNotify);
  }

  addChangeListener(callback) {
    this.addListener(Constants.CHANGE, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(Constants.CHANGE, callback);
  }
}

export default new NotificationManager();
