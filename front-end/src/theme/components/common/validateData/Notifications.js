import React from "react";
import PropTypes from "prop-types";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import Modal from "./Modal";

class Notifications extends React.Component {
  static propTypes = {
    notifications: PropTypes.array.isRequired,
    onRequestHide: PropTypes.func,
    enterTimeout: PropTypes.number,
    leaveTimeout: PropTypes.number
  };

  static defaultProps = {
    notifications: [],
    onRequestHide: () => {},
    enterTimeout: 400,
    leaveTimeout: 400
  };

  handleRequestHide = notification => () => {
    const { onRequestHide } = this.props;
    if (onRequestHide) {
      onRequestHide(notification);
    }
  };

  render() {
    const { notifications, enterTimeout, leaveTimeout } = this.props;

    return (
      <div>
        <TransitionGroup>
          {notifications.map(notification => {
            const key = notification.id || new Date().getTime();
            return (
              <CSSTransition
                timeout={{ exit: leaveTimeout, enter: enterTimeout }}
                key={key}
                classNames="notification"
              >
                <Modal
                  key={key}
                  onRequestHide={this.handleRequestHide(notification)}
                  {...notification}
                />
              </CSSTransition>
            );
          })}
        </TransitionGroup>
      </div>
    );
  }
}

export default Notifications;
