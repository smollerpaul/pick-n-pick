import React from "react";
import PropTypes from "prop-types";
import Manager from "./Manager";
import Notifications from "./Notifications";

class Container extends React.Component {
  static propTypes = {
    enterTimeout: PropTypes.number,
    leaveTimeout: PropTypes.number
  };

  static defaultProps = {
    enterTimeout: 400,
    leaveTimeout: 400
  };

  state = {
    notifications: []
  };

  componentWillMount = () => {
    Manager.addChangeListener(this.handleStoreChange);
  };

  componentWillUnmount = () => {
    Manager.removeChangeListener(this.handleStoreChange);
  };

  handleStoreChange = notifications => {
    this.setState({ notifications });
  };

  handleRequestHide = notification => {
    Manager.remove(notification);
  };

  render() {
    const { notifications } = this.state;
    const { enterTimeout, leaveTimeout } = this.props;
    return (
      <Notifications
        enterTimeout={enterTimeout}
        leaveTimeout={leaveTimeout}
        notifications={notifications}
        onRequestHide={this.handleRequestHide}
      />
    );
  }
}

export default Container;
