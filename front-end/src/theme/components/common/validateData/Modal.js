import React, { Fragment } from "react";
import IntlMessages from "helpers/IntlMessages";
import { Modal, ModalBody, Jumbotron, ModalHeader } from "reactstrap";

class Notification extends React.Component {
  componentDidMount = () => {
    const { timeOut } = this.props;
    if (timeOut !== 0) {
      this.timer = setTimeout(this.requestHide, timeOut);
    }
  };

  componentWillUnmount = () => {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  };

  handleClick = () => {
    const { onClick } = this.props;
    if (onClick) onClick();
    this.requestHide();
  };

  requestHide = () => {
    const { onRequestHide } = this.props;
    if (onRequestHide) onRequestHide();
  };

  render() {
    const { title, missingFields, language } = this.props;
    return (
      <Modal isOpen={true} toggle={this.handleClick}>
        <ModalHeader toggle={this.handleClick}>
          <IntlMessages id="validate.title" />
        </ModalHeader>
        <ModalBody>
          <Jumbotron className="text-center p-2">
            <Fragment>
              <img
                src="/assets/img/no-data-found.jpg"
                style={{ width: "70%" }}
                alt="No Data Found"
              />
              <p>
                <IntlMessages id="validate.missing" />
              </p>
              <p>
                <h4>
                  {missingFields &&
                    missingFields.map((o, i) => (
                      <Fragment key={i}>
                        <IntlMessages id={`${language}.${o}`} />
                        {missingFields.length - i === 1 ? null : ", "}
                      </Fragment>
                    ))}
                </h4>
              </p>
              <hr className="my-4" />
              <p className="lead mb-0 quote-container">
                <i className="simple-icon-bulb quote-icon" />
                <em>
                  <IntlMessages id={title} />
                </em>
              </p>
              <p className="lead mb-0"></p>
            </Fragment>
          </Jumbotron>
        </ModalBody>
      </Modal>
    );
  }
}

export default Notification;
