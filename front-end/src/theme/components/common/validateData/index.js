import Notifications from './Notifications';
import Container from './Container';
import ValidateData from './Manager';

export { Notifications, Container, ValidateData };
export default Notifications;
