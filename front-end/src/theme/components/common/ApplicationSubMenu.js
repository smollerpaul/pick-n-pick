import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as action from "redux/app-menu/actions";
class ApplicationSubMenu extends React.Component {
  constructor(...params) {
    super(...params);
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.toggle = this.toggle.bind(this);

    this.state = {
      isInit: true
    };
  }

  handleDocumentClick(e) {
    const container = ReactDOM.findDOMNode(this);
    if (container.contains(e.target) || container === e.target) {
      return;
    }

    this.toggle(e);
  }

  toggle(e) {
    e.preventDefault();
    const isOpen = this.props.isOpen;

    if (!isOpen) {
      this.addEvents();
    } else {
      this.removeEvents();
    }
    this.props.actions.toggleAppMenu();
  }

  componentWillUnmount() {
    this.removeEvents();
  }
  componentDidMount() {
    this.setState({
      isInit: false
    });
  }
  addEvents() {
    ["click", "touchstart"].forEach(event =>
      document.addEventListener(event, this.handleDocumentClick, true)
    );
  }

  removeEvents() {
    ["click", "touchstart"].forEach(event =>
      document.removeEventListener(event, this.handleDocumentClick, true)
    );
  }

  render() {
    return (
      <div
        className={`app-menu ${this.props.isOpen ? "shown" : ""}${
          this.state.isInit ? "closed " : ""
        }${this.props.isClosing ? "closed" : ""}`}
        // className="app-menu slideInRight slideOutLeft"
      >
        {this.props.children}
        <NavLink className="app-menu-button d-inline-block d-xl-none" onClick={this.toggle}>
          <i className="simple-icon-refresh" />
        </NavLink>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    ...state["appmenu"]
  };
}

function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationSubMenu);
