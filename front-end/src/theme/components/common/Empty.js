import React from "react";
import { Card, CardBody, Button } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";

export default function Empty(props) {
  return (
    <Card className="text-center w-30 mx-auto my-auto">
      <CardBody>
        <img
          src="/assets/img/cat-empty.gif"
          alt="WOW U DONE EVERY THING, GET YOURSELF A BREAK"
          style={{ width: "90%" }}
          className="mt-0"
        />
        <hr className="my-4" />
        <h3>
          <IntlMessages id="empty.title" />{" "}
        </h3>
        {props.isCreate &&
          <Button size="xs" outline onClick={() => props.onClick()}>
            <p className="mb-0 quote-container">
              <i className="simple-icon-bulb quote-icon" />
              <em>
                <IntlMessages id="empty.footer" />{" "}
              </em>
            </p>
          </Button>
        }
      </CardBody>
    </Card>
  );
}
