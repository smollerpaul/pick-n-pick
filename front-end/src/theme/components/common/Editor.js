import React, { Component } from "react";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import config from "../../../config";
import { request } from "services/api";
import { get } from "lodash";
import ReactQuill from "react-quill";
import axios from "axios";
import { NotificationManager } from "theme/components/common/react-notifications";

const getFileExtension = filename => {
  var ext = /^.+\.([^.]+)$/.exec(filename);
  return ext == null ? "" : ext[1];
};
export default class Editor extends Component {
  handleUpload = async file => {
    try {
      const endpoint = config.apiBaseURL + "/uploads";
      let { data } = await request(endpoint, "POST", {
        fileExtension: getFileExtension(file.name)
      });
      await axios.put(data.url, file, { headers: { "Content-Type": data.contentType } });
      return data.getUrl;
    } catch (err) {
      NotificationManager.error("Errors", err);
    }
  };
  imageHandler = () => {
    const quill = this.quill;
    const input = document.createElement("input");

    input.setAttribute("type", "file");
    input.setAttribute("accept", "image/*");
    input.click();

    input.onchange = async () => {
      // Save current cursor state
      const range = quill.getEditorSelection(true);
      const res = await this.handleUpload(input.files[0]);
      quill.getEditor().insertEmbed(range.index, "image", `${res}`);
      quill.getEditor().setSelection(range.index + 1);
    };
  };
  render() {
    const quillFormats = [
      "header",
      "bold",
      "italic",
      "underline",
      "strike",
      "blockquote",
      "list",
      "bullet",
      "indent",
      "link",
      "image"
    ];
    return (
      <ReactQuill
        theme="snow"
        ref={el => (this.quill = el)}
        value={get(this.props, "value", "")}
        onChange={e => this.props.onChange(e)}
        formats={quillFormats}
        modules={{
          // ImageResize: {},
          // // imageResize: {
          // //   displaySize: true
          // // },
          // imageResize: true,
          toolbar: {
            container: [
              [{ header: "1" }, { header: "2" }, { header: [3, 4, 5, 6] }, { font: [] }],
              [{ size: [] }],
              ["bold", "italic", "underline", "strike", "blockquote"],
              [{ list: "ordered" }, { list: "bullet" }],
              ["link", "image", "video"],
              ["clean"],
              ["code-block"]
            ],
            handlers: {
              image: this.imageHandler
            }
          }
        }}
      />
    );
  }
}
