import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from "reactstrap";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as action from "redux/app-menu/actions";
class ApplicationMenu extends React.Component {
  constructor(...params) {
    super(...params);
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false
    };
  }

  handleDocumentClick(e) {
    const container = ReactDOM.findDOMNode(this);
    if (container.contains(e.target) || container === e.target) {
      return;
    }

    if (!this.props.isSubMenuOpen) this.toggle(e);
  }

  toggle(e) {
    e.preventDefault();
    const isOpen = this.props.isOpen;
    if (!isOpen) {
      this.addEvents();
    } else {
      this.removeEvents();
    }
    this.props.actions.toggleAppMenu();
  }

  componentWillUnmount() {
    this.removeEvents();
  }

  addEvents() {
    ["click", "touchstart"].forEach(event =>
      document.addEventListener(event, this.handleDocumentClick, true)
    );
  }

  removeEvents() {
    ["click", "touchstart"].forEach(event =>
      document.removeEventListener(event, this.handleDocumentClick, true)
    );
  }

  render() {
    const { classType = "app-menu-pos"} = this.props
    return (
      <div className={`${classType} ${this.props.isOpen ? "shown" : ""}`}>
        {this.props.children}
        <NavLink className="app-menu-button d-inline-block d-xl-none" onClick={this.toggle}>
          <i className="simple-icon-refresh" />
        </NavLink>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    ...state["appmenu"]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationMenu);
