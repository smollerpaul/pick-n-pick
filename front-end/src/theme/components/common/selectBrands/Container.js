import React, { Fragment } from "react";
import { save, get as getStore } from "services/localStoredService";
import { isEmpty, isUndefined, get } from "lodash";
import config from "../../../../config";
import { request } from "services/api";
import { Modal, ModalHeader, Card, ModalBody, Jumbotron, Button } from "reactstrap";
import AnimateGroup from "theme/components/AnimateGroup";
import IntlMessages from "helpers/IntlMessages";
import { withRouter } from "react-router-dom";
import { history } from "AppRenderer";
import { Separator } from "theme/components/common/CustomBootstrap";
import { NavLink } from "react-router-dom";
class Container extends React.Component {
  constructor(props) {
    super(props);
    this.fetchBrands();
    this.state = {
      firstRun: true,
      brands: [],
      brandId: getStore("brandid")
    };
  }

  fetchBrands = async () => {
    const { data } = await request(`${config.apiBaseURL}/brands`, "GET");
    if (isEmpty(data)) {
      if (
        get(this.props, "location.pathname") !== "/app/stores/brands/create" &&
        get(this.props, "location.pathname") !== "/setup-brands"
      ) {
        history.push("/setup-brands");
        window.location.reload();
      }
    } else {
      this.setState({ brands: data, firstRun: false });
    }
  };

  render() {
    if (get(this.props, "location.pathname") === "/app/stores/brands/create") {
      return this.props.children;
    }
    if (get(this.props, "location.pathname") === "/setup-brands") {
      return this.props.children;
    }

    const { brands, brandId, firstRun } = this.state;
    if (firstRun) return this.props.children;
    if (isEmpty(brands) || isUndefined(brandId)) {
      return (
        <Modal isOpen={true} size="lg">
          <ModalHeader>
            <IntlMessages id="selectBrands.headers" />
          </ModalHeader>
          <ModalBody>
            <AnimateGroup enter={{ animation: "transition.expandIn" }}>
              {isEmpty(brands) ? (
                <Jumbotron className="text-center p-2">
                  <Fragment>
                    <img
                      src="/assets/img/no-data-found.jpg"
                      style={{ width: "50%" }}
                      alt="No Data Found"
                    />
                    <p className="lead">
                      <IntlMessages id="selectBrands.noBrandsTitle" />
                    </p>
                    <Button
                      onClick={() => {
                        history.push("/setup-brands");
                        window.location.reload();
                      }}
                    >
                      <p className="lead mb-0 quote-container">
                        <i className="simple-icon-bulb quote-icon" />
                        <em>
                          <IntlMessages id="selectBrands.createNewBrands" />
                        </em>
                      </p>
                    </Button>
                    <p className="lead mb-0"></p>
                  </Fragment>
                </Jumbotron>
              ) : (
                <Fragment>
                  {brands.map(o => {
                    const { name, avatar, _id } = o;
                    return (
                      <Fragment>
                        <NavLink to="#" location={{}}>
                          <Card
                            className="d-flex flex-row mb-0 shadow-none effect-hover"
                            onClick={() => {
                              this.setState({ brandId: _id });
                              save("brandid", _id);
                            }}
                          >
                            <img
                              alt={name}
                              src={avatar}
                              className="list-thumbnail responsive border-0 card-img-left"
                            />
                            <div className="pl-2 d-flex flex-grow-1 min-width-zero">
                              <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                                <p className="list-item-heading mb-1 truncate">{name}</p>
                              </div>
                              <div className="custom-control custom-checkbox pl-1 go-corner">
                                <div className="go-arrow">
                                  <i className="simple-icon-arrow-right " />
                                </div>
                              </div>
                            </div>
                          </Card>
                        </NavLink>
                        <Separator className="mb-3 mt-3" />
                      </Fragment>
                    );
                  })}
                  <NavLink to="#" location={{}}>
                    <Card
                      className="d-flex flex-row mb-0 shadow-none effect-hover"
                      onClick={() => {
                        history.push("/app/stores/brands/create");
                        window.location.reload();
                      }}
                    >
                      <img
                        alt="createNewBrands"
                        src="/assets/img/plus.jpg"
                        className="list-thumbnail responsive border-0 card-img-left"
                      />
                      <div className="pl-2 d-flex flex-grow-1 min-width-zero">
                        <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                          <p className="list-item-heading mb-1 truncate">
                            <IntlMessages id="selectBrands.createNewBrands" />
                          </p>
                        </div>
                        <div className="custom-control custom-checkbox pl-1 go-corner">
                          <div className="go-arrow">
                            <i className="simple-icon-arrow-right " />
                          </div>
                        </div>
                      </div>
                    </Card>
                  </NavLink>
                </Fragment>
              )}
            </AnimateGroup>
          </ModalBody>
        </Modal>
      );
    }

    return this.props.children;
  }
}

export default withRouter(Container);
