import React from "react";
import { InputGroup, InputGroupAddon, Input, Button } from "reactstrap";

export default function QuantityButton(props) {
  const { quantity, onChange, size = "md" } = props;
  return (
    <InputGroup size={size}>
      <InputGroupAddon addonType="prepend">
        <Button outline onClick={() => onChange(quantity - 1)} className="default">
          -
        </Button>
      </InputGroupAddon>
      <Input
        value={quantity}
        type="number"
        className="text-center"
        onChange={e => onChange(parseInt(e.target.value))}
      />
      <InputGroupAddon addonType="append">
        <Button onClick={() => onChange(quantity + 1)} className="default">
          +
        </Button>
      </InputGroupAddon>
    </InputGroup>
  );
}
