import React from "react";
import { Button, ModalBody, CardTitle, Modal } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";
import { iconsmind, simplelineicons } from "theme/components/common/icons";

export default function ModalSelectIcons(props) {
  const { isOpen, toggle, onSelect } = props;

  const IconGroup = ({ iconSet, setIndex }) => {
    return (
      <div className="mb-5">
        <h6 className="mb-4">{iconSet.title}</h6>
        {iconSet.icons.map((icon, index) => {
          return (
            <div className="glyph" key={setIndex + index} onClick={() => onSelect(icon)}>
              <div className={"glyph-icon " + icon} />
              <div className="class-name">{icon}</div>
            </div>
          );
        })}
        <div className="clearfix"></div>
      </div>
    );
  };

  return (
    <Modal isOpen={isOpen} toggle={() => toggle()} size="lg">
      <ModalBody>
        <div className="mt-2 mr-2 d-flex justify-content-between">
          <CardTitle className="mt-2 ml-2">
            <IntlMessages id="wikis.selectIcon" />
          </CardTitle>
          <Button outline onClick={() => toggle()}>
            <IntlMessages id="button.close" />
          </Button>
        </div>
        <div className="p-2">
          <div className="simple-line-icons">
            {simplelineicons.map((icon, index) => {
              return (
                <div className="glyph mb-2 mr-2" key={index} onClick={() => onSelect(icon)}>
                  <div className={"glyph-icon " + icon} />
                  <div className="class-name">{icon}</div>
                </div>
              );
            })}
          </div>
        </div>
      </ModalBody>
      <ModalBody>
        <div className="p-2">
          <div className="mind-icons">
            {iconsmind.map((iconSet, setIndex) => {
              return <IconGroup iconSet={iconSet} setIndex={setIndex} key={setIndex} />;
            })}
          </div>
        </div>
      </ModalBody>
    </Modal>
  );
}
