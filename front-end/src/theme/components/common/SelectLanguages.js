import React from "react";
import { UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from "reactstrap";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { changeLocale } from "redux/actions";
import { localeOptions } from "constants/defaultValues";
import { getDirection, setDirection } from "helpers/Utils";

class SelectLanguages extends React.Component {
  handleChangeLocale = (locale, direction) => {
    this.props.changeLocale(locale);

    const currentDirection = getDirection().direction;
    if (direction !== currentDirection) {
      setDirection(direction);
      setTimeout(() => {
        window.location.reload();
      }, 500);
    }
  };
  render() {
    const { locale } = this.props;
    return (
      <div className="d-inline-block">
        <UncontrolledDropdown className="ml-2">
          <DropdownToggle caret color="light" size="sm" className="language-button">
            <span className="name">{locale.toUpperCase()}</span>
          </DropdownToggle>
          <DropdownMenu className="mt-3" direction={"left"}>
            {localeOptions.map(l => (
              <DropdownItem onClick={() => this.handleChangeLocale(l.id, l.direction)} key={l.id}>
                {l.name}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </UncontrolledDropdown>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { locale } = state.settings;
  return {
    locale
  };
};
export default injectIntl(connect(mapStateToProps, { changeLocale })(SelectLanguages));
