import React from "react";
import { ModalBody, Modal, Button, ModalHeader, ModalFooter } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";

export default function ModalDelete(props) {
  const { isDelete, toggle, onDelete } = props;
  return (
    <Modal isOpen={isDelete} toggle={() => toggle()} centered style={{ boxShadow: "none" }}>
      <ModalHeader toggle={() => toggle()}>
        <IntlMessages id="modalDelete.header" />
      </ModalHeader>
      <ModalBody>
        <IntlMessages id="modalDelete.body" />
      </ModalBody>
      <ModalFooter>
        <Button
          color="danger"
          onClick={() => {
            onDelete();
            toggle();
          }}
        >
          <IntlMessages id="button.delete" />
        </Button>{" "}
        <Button color="primary" onClick={() => toggle()}>
          <IntlMessages id="button.cancel" />
        </Button>
      </ModalFooter>
    </Modal>
  );
}
