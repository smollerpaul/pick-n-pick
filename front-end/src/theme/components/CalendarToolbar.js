import React from "react";
import moment from "moment";
import { Button } from "reactstrap";
import { ButtonGroup, ButtonToolbar } from "reactstrap";
import IntlMessages from "helpers/IntlMessages";

export const CalendarToolbar = toolbar => {
  const goToBack = () => {
    toolbar.onNavigate("PREV");
  };
  const goToNext = () => {
    toolbar.onNavigate("NEXT");
  };
  const goToCurrent = () => {
    toolbar.onNavigate("TODAY");
  };

  const label = () => {
    const date = moment(toolbar.date);
    return (
      <span>
        <span>{date.format("MMMM")} </span>
        <span> {date.format("YYYY")}</span>
      </span>
    );
  };

  return (
    <div className="big-calendar-header d-flex">
      <div className="float-left">
        <Button className="btn btn-primary calendar-today-btn mr-2" onClick={goToCurrent}>
          Today
        </Button>
        <Button className="btn calendar-prev-btn mr-1" onClick={goToBack}>
          <span className="simple-icon-arrow-left" />
        </Button>
        <Button className="btn calendar-next-btn" onClick={goToNext}>
          <span className="simple-icon-arrow-right" />
        </Button>
      </div>

      <div style={{ flexGrow: 1, padding: "0 10px", textAlign: "center" }} className="mb-0">
        <h4>{label()}</h4>
      </div>

      <div className="float-right">
        <div>
          <ButtonToolbar>
            <ButtonGroup>
              <Button
                onClick={() => toolbar.onView("day")}
                size="sm"
                color={toolbar.view === "day" ? "primary" : "secondary"}
              >
                <IntlMessages id="toolbar.day" />
              </Button>
              <Button
                onClick={() => toolbar.onView("week")}
                size="sm"
                color={toolbar.view === "week" ? "primary" : "secondary"}
              >
                <IntlMessages id="toolbar.week" />
              </Button>
              <Button
                onClick={() => toolbar.onView("month")}
                size="sm"
                color={toolbar.view === "month" ? "primary" : "secondary"}
              >
                <IntlMessages id="toolbar.month" />
              </Button>
              <Button
                onClick={() => toolbar.onView("agenda")}
                size="sm"
                color={toolbar.view === "agenda" ? "primary" : "secondary"}
              >
                <IntlMessages id="toolbar.agenda" />
              </Button>
            </ButtonGroup>
          </ButtonToolbar>
        </div>
      </div>
    </div>
  );
};
