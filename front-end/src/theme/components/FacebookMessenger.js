import React from "react";
import config from "config";
import MessengerCustomerChat from "react-messenger-customer-chat";

function CustomerChat(props) {
  return <MessengerCustomerChat pageId={config.fbPageId} appId={config.facebookKey} />;
}

export default CustomerChat;
