import React from "react";
import AnimateGroup from "theme/components/AnimateGroup";
import classnames from "classnames";

export default function CustomTbodyComponent(props) {
  return (
    <AnimateGroup
      enter={{ animation: "transition.expandIn" }}
      leave={{ animation: "transition.expandOut" }}
    >
      <div className={classnames("rt-tbody", props.className || [])}>{props.children}</div>
    </AnimateGroup>
  );
}
