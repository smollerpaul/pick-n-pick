import React, { Fragment } from "react";
import moment from "moment";

export const CalendarHeaders = props => {
  return (
    <Fragment>
      <h3 className="mb-0">{props.label}</h3>
      <h7>{moment(props.date).format("DD/MM/YYYY")}</h7>
    </Fragment>
  );
};
