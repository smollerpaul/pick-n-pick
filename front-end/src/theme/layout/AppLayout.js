import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import TopNav from "theme/containers/navs/Topnav";
import SidebarStore from "theme/containers/navs/SidebarStore";
import { name } from "views/Wrapper/reducers";

class AppLayout extends Component {
  render() {
    const { containerClassnames, userInfo } = this.props;
    return (
      <div id="app-container" className={containerClassnames}>
        <TopNav
          history={this.props.history}
          userInfo={userInfo}
          isUser={this.props.location.pathname.includes("/app/")}
        />

        {(this.props.match.url === "/admin" || this.props.match.url === "/app") ? <SidebarStore /> : null}
        <main className="main-pos">
          <div className="container-fluid">{this.props.children}</div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = state => {
  const { containerClassnames } = state.menu;
  return { containerClassnames, ...state[name] };
};
const mapActionToProps = {};

export default withRouter(connect(mapStateToProps, mapActionToProps)(AppLayout));
