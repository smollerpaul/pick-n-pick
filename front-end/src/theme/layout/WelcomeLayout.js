import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import TopNav from "theme/containers/navs/Topnav";
import { name } from "views/Wrapper/reducers";

class AppLayout extends Component {
  render() {
    const { userInfo } = this.props;
    return (
      <Fragment>
        <div className="fixed-background" />
        <TopNav
          history={this.props.history}
          userInfo={userInfo}
          isUser={this.props.location.pathname.includes("/app/")}
        />
        {this.props.children}
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  const { containerClassnames } = state.menu;
  return { containerClassnames, ...state[name] };
};
const mapActionToProps = {};

export default withRouter(connect(mapStateToProps, mapActionToProps)(AppLayout));
