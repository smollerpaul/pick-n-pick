import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "views/Stores/Wrapper/reducers";

class AppLayout extends Component {
  render() {
    const { containerClassnames } = this.props;
    return (
      <div id="app-container" className={containerClassnames}>
        {this.props.children}
      </div>
    );
  }
}
const mapStateToProps = state => {
  const { containerClassnames } = state.menu;
  return { containerClassnames, ...state[name] };
};
const mapActionToProps = {};

export default withRouter(connect(mapStateToProps, mapActionToProps)(AppLayout));
